<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMisiHimaCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('misi_hima_candidates', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('detail');
            $table->integer('id_hima_candidate')->unsigned();
            $table->timestamps();
        });

        Schema::table('misi_hima_candidates', function (Blueprint $table) {
            $table->foreign('id_hima_candidate', 'id_hima_candidate_misi_hima_candidates_fk_01')->references('id')->on('hima_candidates')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('misi_hima_candidates');
    }
}
