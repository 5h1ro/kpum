<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMisiDpmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('misi_dpms', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('detail');
            $table->integer('id_dpm')->unsigned();
            $table->timestamps();
        });

        Schema::table('misi_dpms', function (Blueprint $table) {
            $table->foreign('id_dpm', 'id_dpm_misi_dpms_fk_01')->references('id')->on('dpms')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('misi_dpms');
    }
}
