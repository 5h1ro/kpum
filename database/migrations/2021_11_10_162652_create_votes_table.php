<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('votes', function (Blueprint $table) {
            $table->increments('id');
            $table->text('id_bem')->nullable();
            $table->text('id_dpm')->nullable();
            $table->text('id_hmj')->nullable();
            $table->text('id_hima')->nullable();
            $table->integer('id_user')->unsigned()->nullable();
            $table->timestamps();
        });

        Schema::table('votes', function (Blueprint $table) {
            $table->foreign('id_user', 'id_user_votes_fk_0')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('votes');
    }
}
