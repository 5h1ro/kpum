<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBemCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bem_candidates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fullname1');
            $table->string('name1');
            $table->integer('npm1')->unique();
            $table->string('birthplace1');
            $table->string('birthday1');
            $table->string('gender1');
            $table->string('email1');
            $table->integer('id_jurusan1')->unsigned();
            $table->integer('id_prodi1')->unsigned();
            $table->string('class1');
            $table->string('fullname2');
            $table->string('name2');
            $table->integer('npm2')->unique();
            $table->string('birthplace2');
            $table->string('birthday2');
            $table->string('gender2');
            $table->string('email2');
            $table->integer('id_jurusan2')->unsigned();
            $table->integer('id_prodi2')->unsigned();
            $table->string('class2');
            $table->text('visi');
            $table->string('formulir');
            $table->string('persyaratan');
            $table->string('photo1');
            $table->string('photo2');
            $table->timestamps();
        });

        Schema::table('bem_candidates', function (Blueprint $table) {
            $table->foreign('id_jurusan1', 'id_jurusan1_bem_candidates_fk_01')->references('id')->on('jurusans')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_prodi1', 'id_prodi1_bem_candidates_fk_02')->references('id')->on('prodis')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_jurusan2', 'id_jurusan2_bem_candidates_fk_03')->references('id')->on('jurusans')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_prodi2', 'id_prodi2_bem_candidates_fk_04')->references('id')->on('prodis')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bem_candidates');
    }
}
