<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bems', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name1');
            $table->string('name2');
            $table->string('nickname1');
            $table->string('nickname2');
            $table->integer('id_prodi1')->unsigned();
            $table->integer('id_prodi2')->unsigned();
            $table->string('img')->default(asset('assets/images/user-card/3.jpg'));
            $table->string('img2')->default(asset('assets/images/ecommerce/01.jpg'));
            $table->longText('visi');
            $table->timestamps();
        });

        Schema::table('bems', function (Blueprint $table) {
            $table->foreign('id_prodi1', 'id_prodi1_fk_01')->references('id')->on('prodis')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_prodi2', 'id_prodi2_fk_01')->references('id')->on('prodis')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bems');
    }
}
