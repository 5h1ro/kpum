<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMisiHmjCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('misi_hmj_candidates', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('detail');
            $table->integer('id_hmj_candidate')->unsigned();
            $table->timestamps();
        });

        Schema::table('misi_hmj_candidates', function (Blueprint $table) {
            $table->foreign('id_hmj_candidate', 'id_hmj_candidate_misi_hmj_candidates_fk_01')->references('id')->on('hmj_candidates')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('misi_hmj_candidates');
    }
}
