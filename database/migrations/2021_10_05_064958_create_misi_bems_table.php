<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMisiBemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('misi_bems', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('detail');
            $table->integer('id_bem')->unsigned();
            $table->timestamps();
        });

        Schema::table('misi_bems', function (Blueprint $table) {
            $table->foreign('id_bem', 'id_bem_fk_01')->references('id')->on('bems')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('misi_bems');
    }
}
