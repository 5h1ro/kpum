<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVotersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('fullname')->nullable();
            $table->string('number');
            $table->string('birthday')->nullable();
            $table->string('city')->nullable();
            $table->string('gender')->nullable();
            $table->string('religion')->nullable();
            $table->longText('address')->nullable();
            $table->integer('id_jurusan')->unsigned();
            $table->integer('id_prodi')->unsigned();
            $table->integer('id_user')->unsigned();
            $table->integer('bem')->default(0);
            $table->integer('dpm')->default(0);
            $table->integer('hmj')->default(0);
            $table->integer('hima')->default(0);
            $table->string('image')->default(asset('image/profile/profile.jpg'));
            $table->timestamps();
        });

        Schema::table('voters', function (Blueprint $table) {
            $table->foreign('id_jurusan', 'id_jurusan_fk_02')->references('id')->on('jurusans')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_prodi', 'id_prodi_fk_01')->references('id')->on('prodis')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_user', 'id_user_fk_01')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voters');
    }
}
