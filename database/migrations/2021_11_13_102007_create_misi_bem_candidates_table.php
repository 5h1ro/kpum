<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMisiBemCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('misi_bem_candidates', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('detail');
            $table->integer('id_bem_candidate')->unsigned();
            $table->timestamps();
        });

        Schema::table('misi_bem_candidates', function (Blueprint $table) {
            $table->foreign('id_bem_candidate', 'id_bem_candidate_misi_bem_candidates_fk_01')->references('id')->on('bem_candidates')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('misi_bem_candidates');
    }
}
