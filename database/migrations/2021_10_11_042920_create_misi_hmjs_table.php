<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMisiHmjsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('misi_hmjs', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('detail');
            $table->integer('id_hmj')->unsigned();
            $table->timestamps();
        });

        Schema::table('misi_hmjs', function (Blueprint $table) {
            $table->foreign('id_hmj', 'id_hmj_misi_hmjs_fk_01')->references('id')->on('hmjs')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('misi_hmjs');
    }
}
