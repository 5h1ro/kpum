<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('start');
            $table->string('end');
            $table->integer('id_jurusan')->unsigned();
            $table->integer('id_prodi')->unsigned();
            $table->string('category');
            $table->timestamps();
        });

        Schema::table('schedules', function (Blueprint $table) {
            $table->foreign('id_jurusan', 'id_prodi_schedules_fk_01')->references('id')->on('jurusans')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_prodi', 'id_prodi_schedules_fk_02')->references('id')->on('prodis')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}
