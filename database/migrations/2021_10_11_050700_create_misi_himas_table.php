<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMisiHimasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('misi_himas', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('detail');
            $table->integer('id_hima')->unsigned();
            $table->timestamps();
        });

        Schema::table('misi_himas', function (Blueprint $table) {
            $table->foreign('id_hima', 'id_hima_misi_himas_fk_01')->references('id')->on('himas')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('misi_himas');
    }
}
