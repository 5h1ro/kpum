<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMisiDpmCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('misi_dpm_candidates', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('detail');
            $table->integer('id_dpm_candidate')->unsigned();
            $table->timestamps();
        });

        Schema::table('misi_dpm_candidates', function (Blueprint $table) {
            $table->foreign('id_dpm_candidate', 'id_dpm_candidate_misi_dpm_candidates_fk_01')->references('id')->on('dpm_candidates')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('misi_dpm_candidates');
    }
}
