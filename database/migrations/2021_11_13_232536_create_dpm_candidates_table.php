<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDpmCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dpm_candidates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fullname');
            $table->string('name');
            $table->integer('npm')->unique();
            $table->string('birthplace');
            $table->string('birthday');
            $table->string('gender');
            $table->string('email');
            $table->integer('id_jurusan')->unsigned();
            $table->integer('id_prodi')->unsigned();
            $table->string('class');
            $table->text('visi');
            $table->string('formulir');
            $table->string('persyaratan');
            $table->string('photo');
            $table->timestamps();
        });

        Schema::table('dpm_candidates', function (Blueprint $table) {
            $table->foreign('id_jurusan', 'id_jurusan_dpm_candidates_fk_01')->references('id')->on('jurusans')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_prodi', 'id_prodi_dpm_candidates_fk_02')->references('id')->on('prodis')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dpm_candidates');
    }
}
