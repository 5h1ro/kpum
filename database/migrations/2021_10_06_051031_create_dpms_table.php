<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDpmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dpms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('nickname');
            $table->integer('id_prodi')->unsigned();
            $table->string('img')->default(asset('assets/images/user-card/3.jpg'));
            $table->string('img2')->default(asset('assets/images/ecommerce/01.jpg'));
            $table->longText('visi');
            $table->timestamps();
        });

        Schema::table('dpms', function (Blueprint $table) {
            $table->foreign('id_prodi', 'id_prodi_dpms_fk_01')->references('id')->on('prodis')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dpms');
    }
}
