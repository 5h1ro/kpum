<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $name = [
            'Super Admin',
            'Admin',
            'Voter',
            'Viewer',
        ];

        $permission = [
            ["role"],
            ["candidate", "schedule", "pendaftar", "voter", "report"],
            ["choice"],
            null
        ];

        for ($i = 0; $i < count($name); $i++) {
            $user = Role::create([
                'name'             => $name[$i],
                'permission'       => $permission[$i],
            ]);
            $user->save();
        };
    }
}
