<?php

namespace Database\Seeders;

use App\Models\Jurusan;
use Illuminate\Database\Seeder;

class JurusanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $name = [
            'Teknik',
            'Administrasi Bisnis',
            'Komputer Akuntansi',
        ];

        for ($i = 0; $i < count($name); $i++) {
            $user = Jurusan::create([
                'name'             => $name[$i],
            ]);
            $user->save();
        };
    }
}
