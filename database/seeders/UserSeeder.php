<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $email = [
            'sa@sa.com',
            'admin@admin.com',
        ];

        $role = [
            '1',
            '2',
        ];

        $npm = [
            '1',
            '2',
        ];

        $os = [
            'Linux',
            'Linux',
        ];

        $browser = [
            'Firefox',
            'Firefox',
        ];

        for ($i = 0; $i < count($email); $i++) {
            $user = User::create([
                'email'             => $email[$i],
                'npm'               => $npm[$i],
                'os'                => $os[$i],
                'browser'           => $browser[$i],
                'password'          => Hash::make('password'),
                'id_role'           => $role[$i]
            ]);
            $user->save();
        };
    }
}
