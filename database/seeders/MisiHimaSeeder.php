<?php

namespace Database\Seeders;

use App\Models\MisiHima;
use Illuminate\Database\Seeder;

class MisiHimaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $detail = [
            'Mewujudkan sumber daya manusia yang aktif dan produktif dalam bidang akademik maupun non akademik',
            'Mengajak mahasiswa Pogram Studi Tekonologi Informasi untuk berkembang bersama guna mengoptimalkan sarana prasarana yang tersedia',
            'Meningkatkan solidaritas dan kekeluargaan antar sesama guna menggapai satu tujuan',
            'Mengoptimalkan fungsi HIMATIF sebagai wadah pengembangan ide dan kreatifitas HIMATIF',
            'Mempererat Tali Kekeluargaan anggota HIMATIH',
            'Meningkatkan kerjasama HIATIF dengan Organisasi Mahasiswa di dalam dan diluar kamus secara sinergis',
            'Pengembangan sinergitas antara himpunan lembaga internal maupun eksternal',
            'Meningkatkan kinerja himatif dengan sikap kebersamaan tanpa terjadi kesenjangan diantara pengurus himatif',
            'Menjadikan Himpunan sebagai sarana untuk meningkatkan kualitas pribadi yang memiliki jiwa tanggung jawab dalam menjalankan amanah',
            'Mempererat hubungan antar sesama pengurus dan anggota HIMATEKOM pada umumnya',
            'Mengoptimalkan fungsi-fungsional setiap Divisi dan Menyeleggarakan kegiatan yang mendukung tercapainya mahasiswa yang aktif',
            'Membangun koordinasi dan berkesinambungan bersama dengan seluruh elemen yang meliputi didalam maupun diluar lingkup HIMATEKOM',
            'Berkontribusi sebagai wadah dalam menampung dan menyalurkan aspirasi untuk seluruh anggota HIMATEKOM',
            'Mampu meningkatkan solidaritas dan kebersamaan antar mahasiswa yang bisa menjembatani perbedaan mahasiswa tanpa kesenjangan sosial',
            'Menjalankan program kerja yang kreatif, inovatif dan produktif untuk mengembangkan prestasi bersama',
            'Menjalin dan membangun relasi secara luas baik internal maupun eksternal kampus guna meningkatkan Hardskill dan Softskill mahasiswa',
            'Meningkatkan kepengurusan HIMATEKOM yang supportif, inovatif, dan profesional guna menyelaraskan HIMATEKOM yang terkoordinir dan kekeluargaan',
            'Mengemban rasa persaudaraan dan kebersamaan antar mahasiswa Teknik Komputer Kontrol',
            'Menampung dan menindaklanjuti aspirasi seluruh mahasiswa Teknik Komputer Kontrol',
            'Menyelenggarakan dan mengembangkan kegiatan untuk menunjang potensi akademik maupun non-akademik mahasiswa Teknik Komputer Kontrol',
            'Mampu meningkatkan kualitas dan pribadi mahasiswa yang lebih unggul di bidang akademik atau non akademik',
            'Mampu menjadi wadah dan memfasilitasi bakat serta minat mahasiswa yang berprestasi',
            'Mampu meningkatkan solidaritas dan kebersamaan antar mahasiswa',
            'Mampu Melaksanakan kegiatan mahasiswa yang aktif, inovatif dan produktif dalam rangka mengembangkan prestasi bersama',
            'Menjadi wadah kaderisasi Mahasiswa Prodi Mesin Otomotif dengan menjunjung tinggi rasa solidaritas dan toleransi',
            'Mengembangkan potensi dan kreativitas Mahasiswa Prodi Mesin Otomotif',
            'Mengintegritaskan sistem organisasi HIMAMETO PNM yang adaptif, efektif dan relevan sesuai perkembangan zaman',
            'Menguatkan pondasi internal dan kinerja kepengurusan HIMAMETO dalam menampung aspirasi anggota',
            'Menjalin hubungan harmonis dan komunikasi yang ideal dalam mewujudkan kemajuan bersama',
            'Mewadahi minat, bakat dan kreativitas anggota HIMAMETO guna terciptanya kader Mesin Otomotif yang menjunjung tinggi solidaritas dan loyalitas dalam berkarya, berinovasi serta berprestasi',
            'Meningkatkan kerjasama antar pengurus dan anggota dalam mejalankan program kerja yang mampu berkontribusi dalam perkembangan Riset dan IPTEK baik di Internal maupun Eksternal HIMAMETO',
            'Menciptakan suasana kekeluargaan di dalam setiap kegiatan kemahasiswaan di Himameto',
            'Melaksanakan kegiatan mahasiswa yang aktif, inovatif ,dan produktif untuk meningkatkan prestasi bersama',
            'Memegang teguh nilai-nilai solidaritas',
            'Menanamkan jiwa sikap profesionalisme sebagai pengabdian kepada Mahasiswa Teknik Perkeretaapian PNM',
            'Menjalin kerjasama dengan organisasi internal maupun eksternal PNM yang menghasilkan kontribusi positif bagi Mahasiswa Teknik Perkeretaapian PNM. ',
            'Menjadi wadah aspirasi Mahasiswa Teknik Perkeretaapian dan berusaha merealisasikannya',
            'Mendukung dan berperan aktif dalam kegiatan pengembangan solidaritas Mahasiswa Teknik Perkeretaapian baik akademik maupun non akademik',
            'Meningkatkan keimanan dan ketaqwaan kepada tuhan yang maha esa serta menjunjung nilai pancasila',
            'Menciptakan internal HIMA-TKA yang adaptif dan profesional berlandasan kekeluargaan',
            'Mengembangkan potensi yang dimiliki mahasiswa Teknik Perkeretaapian guna tercapainnya aktualisasi diri dan prestasi mahasiswa',
            'Meningkatkan cita prodi TKA baik internal maupun eksternal PNM',
            'Menciptakan harmonisasi dan kolaborasi dengan keluarga mahasiswa teknik perkeretaapian dan juga alumni teknik perkeretaapian',
            'Meningkatkan kebersamaan dan kekeluargaan antar mahasiswa Teknik Listrik',
            'Mengoptimalkan organisasi sebagai wadah dalam menampung aspirasi mahasiswa.',
            'Menanamkan sikap dan jiwa profesionalisme dalam mahasiswa Teknik Listrik',
            'Mengajak mahasiswa khususnya mahasiswa teknik listrik aktif dalam program kerja yang berlangsung',
            'Mengadakan kegiatan - kegiatan yang bertujuan untuk mempererat kekeluargaan sesama anggota himateklis',
            'Menanamkan nilai kekompakan dan solidaritas yang tinggi sesama  mahasiswa terutama anggota himateklis',
            'Membangun himateklis yang berintegritas, apresiatif dan professional.',
            'Menjadikan himateklis sebagai wadah mahasiswa untuk berkreasi dan berekspresi dalam bidang akademik maupun non akademik.',
            'Meningkatkan relasi positif antar mahasiswa, interen dan exteren Politeknik Negeri Madiun.',
            'Menumbuhkan rasa kekeluargaan antar Mahasiswa Teknik Listrik',
            'Mengembangkan proker yang sudah ada, dengan menyesuaikan kondisi yang di alami kedepan.',
            'mewujudkan HIMATEKLIS yang berisi pribadi rukun,kompak,kerjasama yang baik,jujur,amanah,kerja keras serta Bisa membangun dan mengkoordinasi seluruh elemen HIMATEKLIS',
            'Mampu meningkatkan solidaritas dan kebersamaan antar mahasiswa',
            'Mengoptimalkan Himka sebagai wadah dalam menampung aspirasi mahasiswa Prodi',
            'Melaksanakan kegiatan mahasiswa yang aktif, inovatif dan produktif',
            'Mampu menjadi wadah dan memfasilitasi bakat serta minat mahasiswa',
            'Berkontribusi sebagai wadah penampung dan penyalur aspirasi serta menjadi pusat informasi dan kegiatan yang dilakukan oleh mahasiswa prodi Komputerisasi Akuntansi',
            'Turut berperan dalam meningkatkan keharmonisan dan sinergitas mahasiswa Prodi Komputerisasi Akuntansi',
            'Meningkatkan kualitas mahasiswa Prodi Komputerisasi Akuntansi dengan mendorong setiap mahasiswa untuk berpikir kritis, kreatif dan inovatif dalam berbagai aspek',
            'menjadi wadah kegiatan, penyalur aspirasi, minat dan bakat untuk seluruh mahasiswa kompak dengan asas kekeluargaan',
            'menjadi penyalur untuk mengembangkan minat dan bakat mahasiswa kompak sehingga menjadikan mahasiwa kompak yang maju dan berkembang',
            'menjalin hubungan baik dengan alumni, serta ormawa lainnya yang ada di PNM',
            'Mewadahi aspirasi serta minat dan bakat mahasiswa Prodi Akuntansi',
            'Meningkatkan kekeluargaan antar mahasiswa Prodi Akuntansi',
            'Menjadikan HIMAKSI sebagai sarana untuk meningkatkan kualitas dan prestasi mahasiswa Prodi Akuntansi dibidang akademik maupun nonakademik',
            'Menjadikan HIMA yang berisi pribadi amanah dan tanggung jawab.',
            'Mampu membuat dan melaksanakan program HIMAKSI yang memiliki manfaat secara luas.',
            'Melaksanakan kegiatan mahasiswa yang inovatif dan produktif untuk mengembangkan prestasi.',
            'Mengembangkan sistem kerja yang berkualitas dan berkuantitas dengan mengoptimalkan kinerja HIMAPRODI ADBIS',
            'Menjadikan HIMAPRODI ADBIS sebagai organisasi yg harmonis untuk mencitpakan lingkungan yang kondusif dalam menuangkan ide & bakat',
            'Menjadikan HIMAPRODI ADBIS sebagai sarana untuk meningkatkan kualitas pribadi yang memiliki jiwa tanggung jawab dalam menjalankan amanah',
            'Memberdayakan kualitas SDM Program Studi Administrasi Bisnis.',
            'Menjalin relasi baik dengan pihak internal dan eksternal Program Studi Administrasi Bisnis.',
            'Menjalankan program kerja dan agenda dengan semaksimal mungkin dengan tujuan menghasilkan output positif dan bermanfaat.',
            'Menjadi penghubung yang aktif dan responsif antara mahasiswa dengan program studi maupun Politeknik Negeri Madiun',
            'Menjadikan Himaprodi Adbis sebagai wadah dalam mengembangkan bakat & minat, serta mediator untuk menyalurkan aspirasi.',
            'Mengambil kesempatan yang bermanfaat guna mengembangkan Himaprodi Adbis dan mahasiswa program studi Administrasi Bisnis.',
            'Mengedepankan asas prioritas dalam pelaksanaan kegiatan, baik program kerja maupun agenda, agar handling problem dan time management dapat optimal.',
            'Menciptakan suasana organisasi yang menyenangkan dan mempunyai kekeluargaan yang tinggi',
        ];

        $id_hima = [
            '1',
            '1',
            '1',
            '2',
            '2',
            '2',
            '3',
            '3',
            '3',
            '4',
            '4',
            '4',
            '4',
            '5',
            '5',
            '5',
            '6',
            '6',
            '6',
            '6',
            '7',
            '7',
            '7',
            '7',
            '8',
            '8',
            '8',
            '9',
            '9',
            '9',
            '9',
            '10',
            '10',
            '10',
            '11',
            '11',
            '11',
            '11',
            '12',
            '12',
            '12',
            '12',
            '12',
            '13',
            '13',
            '13',
            '14',
            '14',
            '14',
            '15',
            '15',
            '15',
            '15',
            '15',
            '16',
            '17',
            '17',
            '17',
            '17',
            '18',
            '18',
            '18',
            '19',
            '19',
            '19',
            '20',
            '20',
            '20',
            '21',
            '21',
            '21',
            '22',
            '22',
            '22',
            '23',
            '23',
            '24',
            '24',
            '24',
            '24',
            '24',
            '24',
        ];

        for ($i = 0; $i < count($detail); $i++) {
            $user = MisiHima::create([
                'detail'            => $detail[$i],
                'id_hima'            => $id_hima[$i]
            ]);
            $user->save();
        };
    }
}
