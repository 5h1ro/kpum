<?php

namespace Database\Seeders;

use App\Models\Voter;
use Illuminate\Database\Seeder;

class VoterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $name = [
            'Dany',
        ];

        $fullname = [
            'Nurhakiki Romadhony Ikhwandany',
        ];

        $number = [
            '0856781923',
        ];

        $id_jurusan = [
            '1',
        ];

        $id_prodi = [
            '1',
        ];

        $id_user = [
            '3',
        ];

        for ($i = 0; $i < count($name); $i++) {
            $user = Voter::create([
                'name'             => $name[$i],
                'fullname'         => $fullname[$i],
                'number'           => $number[$i],
                'id_jurusan'       => $id_jurusan[$i],
                'id_prodi'         => $id_prodi[$i],
                'id_user'          => $id_user[$i],
            ]);
            $user->save();
        };
    }
}
