<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Voter;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class MetoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $email = [
            'alfinrk1@gmail.com',
            'ammarallam15@gmail.com',
            'bimosatrio639@gmail.com',
            'bregaswahyumulia77@gmail.com',
            'cocoleeoktazio@gmail.com',
            'dimasoky1303@gmail.com',
            'elgaadisaputra.ea@gmail.com',
            'fahmiahmana1@gmail.com',
            'naroskyjose8@gmail.com',
            'fondadityaputra371@gmail.com',
            'ridwanudinhabib1@gmail.com',
            'bangmenos@gmail.com',
            'irfanmbek19@gmail.com',
            'jokowardoyo1430@gmail.com',
            'arzakhalid.ak@gmail.com',
            'bayusangen32@gmail.com',
            'massfakhri@gmail.com',
            'chooirulboyzz@gmail.com',
            'delta171200@gmail.com',
            'satrioprayogi88@gmail.com',
            'yoppyrizki1605@gmail.com',
            'yusufsiregar11@gmail.com',
            'agungkurniawan.kun@gmail.com',
            'fendik285@gmail.com',
            'alvenuszuhud@gmail.com',
            'al.a.wijaya23@gmail.com',
            'andhikaae1945@gmail.com',
            'ardiekop0@gmail.com',
            'ghanaarmy27@gmail.com',
            'primbonbagus@gmail.com',
            'bismadanendra264@gmail.com',
            'fauzankurniawan996@gmail.com',
            'khairun1017@gmail.com',
            'mochamadchabib73@gmail.com',
            'royanmustofa460@gmail.com',
            'afifrevanza44@gmail.com',
            'dhenmasridho@gmail.com',
            'okta.risqy@gmail.com',
            'radasaktia@gmail.com',
            'malinoanas@gmail.com',
            'reyhanyusuf04@gmail.com',
            'rydhoptrprdna@gmail.com',
            'taufik@hidayah.com',
            'wahyu16juni2002@gmail.com',
            'irfanwinanda2@gmail.com',
            'abdulrahman24404@gmail.com',
            'adib.rossifumi46@gmail.com',
            'adityariski5445@gmail.com',
            'ahnaf.ibni.hanafi@gmail.com',
            'aldyprasetyowibowo@gmail.com',
            'andygalang319@gmail.com',
            'bimarandi485@gmail.com',
            'danutristanto5@gmail.com',
            'donikiclek1@gmail.com',
            'faisalwincoko08@gmail.com',
            'gerilrisky12@gmail.com',
            'ikhtinasahmad@gmail.com',
            'kelvindwiwardana@gmail.com',
            'faridfauzi224@gmail.com',
            'izza.rohman05@gmail.com',
            'muhamadanugrah.rzky@gmail.com',
            'udient2003@gmail.com',
            'rangga.aw04@gmail.com',
            'rizaldiseptyan8@gmail.com',
            'rezakurniawan1819@gmail.com',
            'vikybagus135@gmail.com',
            'wildancahae@gmail.com',
            'ahmadnurrochim1404@gmail.com',
            'aditya75091@gmail.com',
            'adjibn35@gmail.con',
            'ahnafanwar09@gmail.com',
            'argadwiprastya272@gmail.com',
            'bintangari041@gmail.com',
            'dwikyramadhani02@gmail.com',
            'ricoeric999@gmail.com',
            'candra11juni@gmail.com',
            'faizalrafidhia@gmail.com',
            'farhanikhsanuddin@gmail.com',
            'fidafbp@gmail.com',
            'danaaqsa123@gmail.com',
            'wahyugerhana61@gmail.com',
            'ilhamrfdrmwn@gmail.com',
            'marchelinoagusto@gmail.com',
            'Fauzanalzaki18@gmail.com',
            'baharrudinzulki@gmail.com',
            'muklisdwi10@gmail.com',
            'pw261199@gmail.com',
            'restufitrah24@gmail.com',
            'suryacandrapratama0@gmail.com',
            'wahyugilangpamungkas@gmail.com',
            'kantongb45@gmail.com',
            'adharofi098@gmail.com',
            'adityalaksana.01.9c@gmail',
            'agungpambudi@gmail.com',
            'azrilgundam@gmail.com',
            'cahyamadya497@gmail.com',
            'ditosyahrial@gmail.com',
            'donihanif2506@gmail.com',
            'eigir7222@gmail.com',
            'fajrifauzanazhim@gmail.com',
            'nepal.fmg@gmail.com',
            'hendrykandreas@gmail.com',
            'ikhfannaa123@gmail.com',
            'khazinasror625@gmail.com',
            'kuncoroarif1922@gmail.com',
            'antokalibago123@gmail.com',
            'ajik2887@gmail.com',
            'rifdhoni143@gmail.com',
            'adisoesanto86@gmail.com',
            'rizkyzanuar43@gmail.com',
            'muelgeovanny@gmail.com',
            'syamil.s003@gmail.com',
            'alyubiwildan@gmail.com',
            'adeilham7b@gmail.com',
            'alifiannr23@gmail.com',
            'bimomangkujiwo@gmail.com',
            'candracahya820@gmail.com',
            'renataboy263@gmail.com',
            'cahyos466@gmail.com',
            'endrap93@gmail.com',
            'fandonatakrisna@yahoo.com',
            'fernandaeka.prastya2000@gmail.com',
            'firda12032001@gmail.com',
            'frendyukil00@gmail.com',
            'hajriamh.09@gmail.com',
            'hidayathidayat832@gmail.com',
            'nursetoramadhan@gmail.com',
            'josaherlanasaputra@gmail.com',
            'shikalutaki2230@gmail.com',
            'mohirfani12345@gmail.com',
            'yohanhan8@gmail.com',
            'muhammadfarhanardhi19@gmail.com',
            'rizkyfebrilianto1@gmail.com',
            'uztadzitama@gmail.com',
            'yogippmh@gmail.com',
            'pancabaktiyudhistira@gmail.com',
            'putraraditya404@gmail.com',
            'ahmadriyanos02@gmail.com',
            'Akromfikri321@gmail.com',
            'audinanda7@gmail.com',
            'bimarada123@gmail.com',
            'ezrazhadisaputro@gmail.com',
            'giovanhidayat@gmail.com',
            'idriskris516171@gmail.com',
            'Fauziiqbal866@gmail.com',
            'towiimam3@gmail.com',
            'dandydyaz77@gmail.com',
            'rafiabdillah1207@gmail.com',
            'ainiya.khalidah@gmail.com',
            'muhammadaziz712@gmail.com',
            'wwwprioutomo@gmail.com',
            'rahmatalfiansyah24@gmail.com',
            'Sumiaji.id@gmail.com',
            'tegardwipambudi123@gmail.com',
            'titanianurc@gmail.com',
            'yalzafaham234@gmail.com',
            'yupiekaindriana@gmail.com',
        ];
        $password = [
            '1Zp33dBR',
            '5Fb60vCW',
            '0Iu29kUA',
            '7Mx34kXW',
            '7Eo20bYP',
            '0Dk84zRJ',
            '5Xi87jQN',
            '1Ov72kOE',
            '3Hd36mHU',
            '1Ry42tSL',
            '6Zc14yHR',
            '6Lj23zRH',
            '7Ed23kEK',
            '1Bo31mSH',
            '1Hj11yYW',
            '6Jp98zYZ',
            '0Jz43nWK',
            '1Cg36zPG',
            '0Bf81bYN',
            '9Aq14hFW',
            '9Cs20vXY',
            '1Hj11yYW',
            '7Ic92dVB',
            '1Cp99cVX',
            '5Ac23xZH',
            '0Ax74sLZ',
            '1Zb01bAE',
            '6Lj23zRH',
            '9Lb46xTT',
            '7Ed23kEK',
            '9Bk52hVM',
            '2Od39iZZ',
            '2To58sGP',
            '3Hr69cHC',
            '8Jc61pCM',
            '0Hw84zSP',
            '1Ry42tSL',
            '8Do46eFC',
            '6Xi93kJX',
            '7Nw75vAW',
            '8Yw52uUF',
            '6Gn96hAU',
            '1Hs68yOJ',
            '9Rg98uRA',
            '9Rf17qMK',
            '1Rp32gDR',
            '8Di20dPR',
            '4Jr12aEQ',
            '8Tg43vNO',
            '0Na01jGO',
            '2Lr96fVQ',
            '1Cv71bGU',
            '8Wz96iCH',
            '0Ws78uBZ',
            '7Xu53tKQ',
            '3Fw08sWL',
            '7Uu63jZJ',
            '4Wx83zPO',
            '2Th74nRM',
            '2Cw62vNZ',
            '1Jy29dEN',
            '4El74cPV',
            '2Od39iZZ',
            '3Kn07jKS',
            '6Ms03qXR',
            '2To58sGP',
            '9Cs20vXY',
            '4Tf20vRK',
            '3Lc31uUV',
            '5Ec10xZD',
            '7Fl29pDN',
            '4Pj47bFD',
            '1Se08hPP',
            '3Bx26mIQ',
            '7Cm03tSR',
            '3Ab79zBM',
            '8Nc26vRK',
            '2Lk43bZG',
            '8Wb01pPN',
            '8Yw52uUF',
            '1Pc88sAX',
            '3Ym46oTA',
            '4Bg82fRO',
            '5Sq12qUH',
            '2Th74nRM',
            '1Er52bTR',
            '9Uz96tAT',
            '3So22wBT',
            '2Ur01tLR',
            '8Mp15bGJ',
            '6Ig55xGV',
            '1Ov72kOE',
            '3Fk55zXV',
            '7Ed23kEK',
            '1Ql96oXH',
            '9Zx39uYT',
            '2Xm83cFS',
            '9Cs20vXY',
            '2It07cUQ',
            '1Ol17qSB',
            '8Mp15bGJ',
            '4Wg35eIV',
            '2Uk25dUI',
            '7Fl29pDN',
            '9Bk52hVM',
            '7Nw75vAW',
            '2Ym29gCR',
            '0Bf81bYN',
            '3Dk41eSA',
            '9Rx51rEE',
            '1Ov72kOE',
            '6Ti29pCN',
            '3Jy62gND',
            '3Df19cKK',
            '0Or97sJB',
            '4Bt57lOQ',
            '5Dp33aGB',
            '4Nl00gDK',
            '3Fk91qHE',
            '2Tq51nQT',
            '8Iy22qOA',
            '0Kb03gLS',
            '3Sw55bVT',
            '1Es51uOE',
            '5Me25vZW',
            '1Wc05oRJ',
            '4Bt57lOQ',
            '1Tw57iYM',
            '4Yx30cFZ',
            '1Vx73tNV',
            '5Oe52xTY',
            '8Ae69cTA',
            '8On99wCD',
            '2Zr91lFL',
            '4Ds85uJC',
            '2Ys94tAP',
            '7Ha03yRV',
            '3Yj76iCV',
            '7Aw93dRZ',
            '2Yp86cPD',
            '7Hj37oZG',
            '6Zz01fUG',
            '7Zb67eUE',
            '2Tz12sUD',
            '2Vj39sQP',
            '0Jn06pKM',
            '6Er14uOI',
            '1Cy00eEF',
            '8Dh40eEL',
            '5Br23pDH',
            '7Iy89fQK',
            '9Yb00gFC',
            '2Tz12sUD',
            '5Fs33sRP',
            '2Yp86cPD',
            '3Fk91qHE',
            '2Zr91lFL',
        ];

        $name = [
            'Alvin Refinda Kusuma',
            'Ammar Allam Asysyafiq',
            'Bimo Satrio Nugroho',
            'Bregas Wahyu Mulia',
            'Cocolee Oktazio Sulaiman',
            'Dimas Oky Nugroho',
            'Elga Aldi Saputra',
            'Fahmi Ahmana Ichromiarto',
            'Febryan Wahyu Hidayat',
            'Fonda Aditya Saputra',
            'Habib Ridwanudin',
            'Hendik Vega Vermanda',
            'Irfan Fachrezi',
            'Joko Wardoyo',
            'Khalid Muhamad Arza',
            'Moh. Bayu Santoso',
            'Muhamad Fikhri',
            'Muhammad Choirul Sholikhin',
            'Pringgo Dista Fery Arianto',
            'Satrio Prayogi Wibowo',
            'Yoppy Rizky Fredianto',
            'Yusuf Yosse Siregar',
            'Agung Kurniawan',
            'Akhmad Efendi Khoiri',
            'Al Venus Zuhud Febriansyah',
            'Alfiq Adi Mahsu',
            'Andhika Octa Setiawan',
            'Ardi Eko Prasetyo',
            'Arminto Ghana Purbaya',
            'Bagus Priambodo',
            'Bisma Arya Danendra',
            'Fauzan kurniawan',
            'Khairun Nur',
            'Mochamad Chabib Nuramin',
            'Mochamad Royanul Mustofa',
            'Mokhammad Afif Revanza',
            'Muhammad Ridho',
            'Okta Dilla Risqy',
            'Rada Sakti Anggara',
            'Rahmad Annas Malino',
            'Reyhan Yusuf Kalyana',
            'Rydho Putra Pradana',
            'Taufik Hidayah',
            'Wahyu Andika Putra',
            'Irfan Winanda Putra',
            'Abdul Rahman',
            'Adib Rajab Sirajuddin',
            'Aditya Riski Prabowo',
            'Ahnaf Ibni Hanafi',
            'Aldy Prasetya Wibowo',
            'Andy Galang Prakoso',
            'Bima Randi Wardiasaputra',
            'danu tristanto',
            'Doni Setyo Pamungkas',
            'Faisal Bachtiar Wincoko',
            'Geril risky yuanto',
            'Ikhtinas Ahmad',
            'Kelvin Dwi Wardana',
            'M.Farid Fauzi',
            'Moch.Rizki Prayogo',
            'Muhamad Anugerah.R',
            'Muhammad Saifuddin',
            'Rangga Adhyatmika Wardhana',
            'Rizaldi Septyan Rahmadhani',
            'Septian Reza Kurniawan',
            'Viky bagus hardianto',
            'Wildan Dwi Prasetya',
            'Achmad Nur Rochim',
            'Aditya Christian Armando',
            'Adji Bhekti Nugroho',
            'Ahnaf Syaiful Anwar',
            'Arga Dwi Prastya',
            'Bintang Arisendy',
            'Dwiky Ramadhani',
            'Eric rico ardiansyah',
            'Fadilla Candra Ardhika',
            'Faisal Rafidhia Danu Setiawan',
            'Farhan ikhsanuddin',
            'Fida’ Fadhullah Bahartiyan Putra',
            'Hearlanggeng Timur Widya Perdana',
            'I Wayan Wahyu Gerhana Putra Bajara',
            'Ilham Arif Dermawan',
            'Marchelino Agusto',
            'Mohammad Fauzan Alzaki',
            'Muhammad Baharrudin Zulkifatara',
            'Muklis Dwi Ervanda',
            'Norman hakim Febrian Muntholib',
            'Restu Fitrah Pramudya',
            'Surya Candra Pratama',
            'Wahyu Gilang Pamungkas',
            'Wulung  Samodro Aloko',
            'Adha Rofi Putra Harianto',
            'Aditya Laksana Putra',
            'Agung Pambudi',
            'Azril Fauzi',
            'Cahya Madya Pratama',
            'Dito Oky Syahrial',
            'Doni Hanif Pratama',
            'Eigi Ramadhani',
            'Fajri Fauzan Azhim',
            'Galih Satrio',
            'Hendryx Andreas Bintang Susilo',
            'Ikhfan Noor Aslam',
            'Khazinul Asror',
            'Kuncoro Arif Hidayat',
            'Mariyanto',
            'Muhamad Aji Mukti Wibowo',
            'Muhammad Rifdhoni',
            'Riyan Adi Susanto',
            'Rizky Zanuar Ilhami',
            'Samuel Geovanny Valentino',
            'Syamil Syabilillah',
            'Wildan Al-Ayubi',
            'Ade Ilham Gunaputra',
            'Alifian Nur Rahman',
            'Bimo Wicaksono',
            'Candra Cahya Saputra',
            'Dandhi Eka Inggar Renata',
            'Eko Novianto Cahyo Saputro',
            'Endra Putra Pamungkas ',
            'Fandonata Krisna Valiano',
            'Fernanda Eka Prastya',
            'Firda Rizky Cahya Putra',
            'Frendy Gilang Ramadhan',
            'Hajri Alif Magrubhi Herwanto',
            'Hidayat',
            'Jasho Nurseto Ramadhan',
            'Josa Herlana Saputra',
            'M. Rafli Haqqul Syach Putra',
            'Mohamad Irfani',
            'Muhammad Alwi Yohansyah',
            'Muhammad Farhan Ardhiansyah',
            'Rizky Febrilianto',
            'Ustadzitama',
            'Yogi Setiyawan',
            'Yudhistira Pancabakti',
            'Aditya Muhammad Saputra',
            'Ahmad Riyan O.S',
            'Akrom Fikri Susanto',
            'Audi Ananda',
            'Bimantoro Wahyu R',
            'Ezra Zachary',
            'Giovan Aldi Y.H',
            'Idris Kris Shandy',
            'Moh Iqbal Fauzi',
            'Muhamad Imam Bastowi',
            'Muhammad Dandy Dyaz Ahmadi',
            'Muhammad Rafi Abdillah',
            'Nana Ainiya Khalidah',
            'Nur Muhamad Hadi Al Aziz',
            'Prio Utomo',
            'Rahmat Alfiansyah',
            'Sumiaji',
            'Tegar Dwi Pambudi',
            'Titania Nur Cahyani',
            'Yalza Fahamiya Lisko Sinarma',
            'Yupi Eka Indriana',
        ];
        $number = [
            '6287860837046',
            '6281946396596',
            '6287833586454',
            '6285773464239',
            '6281915977652',
            '6281216736362',
            '6285233950216',
            '6281556446320',
            '62895367251843',
            '6285546186661',
            '6285604503172',
            '6282330246186',
            '6289649995981',
            '6285231862446',
            '62085731951870',
            '62895621146536',
            '6289696421606',
            '6285232950087',
            '6283848075447',
            '6282330219916',
            '628819148150',
            '62895327738974',
            '6283834826223',
            '6283856133874',
            '6281999967347',
            '6285708300730',
            '62881026160001',
            '6282131663878',
            '6282296444811',
            '6285895516901',
            '6285791377119',
            '6282228830784',
            '6289688733043',
            '6289603035939',
            '62895395095497',
            '6287862669041',
            '6283845417465',
            '62895342793164',
            '6281252703149',
            '6282143385293',
            '6287836291139',
            '6282338547636',
            '6285655623143',
            '6289515711874',
            '6285280451297',
            '6285749844289',
            '6281335161937',
            '62881026178537',
            '6285609971615',
            '6289502103610',
            '6289617472936',
            '62895341160149',
            '6281553449401',
            '6285646831294',
            '6282142410655',
            '6281335853231',
            '6283137168866',
            '628977941302',
            '6281248025601',
            '6285106030101',
            '6281230072365',
            '6285708203892',
            '6289503878868',
            '6285607323957',
            '6281615516922',
            '6285335475941',
            '6285704283761',
            '6285806176022',
            '6285894089629',
            '6282233831293',
            '62895411338151',
            '62895366405412',
            '628993980935',
            '628787740940',
            '6281455036132',
            '6285755676615',
            '6281615162167',
            '6281216871441',
            '6283845072626',
            '6285706375250',
            '6283116114005',
            '6285249331515',
            '6281380004104',
            '628973493775',
            '6289536051230',
            '6281938723308',
            '62895807793339',
            '6281326055096',
            '6282139637174',
            '6289506323364',
            '62895389167307',
            '6285648332784',
            '6289679620542',
            '6283857025483',
            '6287728391877',
            '6287888502611',
            '62895366704291',
            '628885855258',
            '6289678709597',
            '6287744471675',
            '6282322673132',
            '6285859238431',
            '6285850184021',
            '6281554445065',
            '62881026729187',
            '6281913675402',
            '6285785959854',
            '628992111796',
            '6281515622687',
            '62895710697900',
            '6282117167782',
            '6281328553694',
            '6285655744090',
            '6282135354942',
            '6281331049869',
            '6281802129516',
            '6285784933314',
            '6289632594719',
            '62895808290899',
            '6281223552572',
            '6285882947515',
            '6287719269030',
            '6285713482481',
            '6285947144034',
            '6281317603177',
            '6281335711727',
            '6283841042017',
            '62895397397770',
            '6289603019727',
            '6285730078027',
            '6283853097665',
            '6281217228313',
            '6285702148504',
            '6281249109304',
            '62881026367463',
            '628819732847',
            '6288210706961',
            '62895638019606',
            '6283867645302',
            '6285904355728',
            '6285225865727',
            '6285771850760',
            '6281334549433',
            '6285156152063',
            '6285608543001',
            '62895368165741',
            '6283851710770',
            '6287865590610',
            '6281232592813',
            '628986871469',
            '6282313152072',
            '6289506125530',
            '6285733931206',
            '6285708510860',
            '6283119813599',
            '6285812736087',
            '6285749091089',
        ];

        $id_jurusan = [
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
        ];

        $id_prodi = [
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
            '4',
        ];

        for ($i = 0; $i < count($email); $i++) {
            $user = User::create([
                'email'            => $email[$i],
                'password'         => Hash::make($password[$i])
            ]);
            $user->save();
            $voter = Voter::create([
                'name'            => $name[$i],
                'number'          => $number[$i],
                'id_jurusan'      => $id_jurusan[$i],
                'id_prodi'        => $id_prodi[$i],
                'id_user'         => $user->id,
            ]);
            $voter->save();
        };
    }
}
