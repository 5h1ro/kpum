<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Voter;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserInputSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $email = [
            'farhan@gmail.com',
            'yudish@gmail.com',
        ];
        $password = [
            '2Tt48lMG',
            '0Nr70iVO',
        ];

        $name = [
            'Farhan',
            'Yudish',
        ];
        $number = [
            '+6281217228313',
            '+628819732847',
        ];

        $id_jurusan = [
            '1',
            '1',
        ];

        $id_prodi = [
            '4',
            '4',
        ];

        for ($i = 0; $i < count($email); $i++) {
            $user = User::create([
                'email'             => $email[$i],
                'password'          => Hash::make($password[$i])
            ]);
            $user->save();
            $voter = Voter::create([
                'name'             => $name[$i],
                'number'           => $number[$i],
                'id_jurusan'       => $id_jurusan[$i],
                'id_prodi'         => $id_prodi[$i],
                'id_user'          => $user->id,
            ]);
            $voter->save();
        };
    }
}
