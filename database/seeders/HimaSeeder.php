<?php

namespace Database\Seeders;

use App\Models\Hima;
use Illuminate\Database\Seeder;

class HimaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $name = [
            'Muhammad Rifqi Aminuddin',
            'M.Fahri Bagus Wibysono',
            'Fabio Shelby Aul Nurhilmi',
            'Faktur Muhfroni',
            'Ilham Dwi Yanto',
            'Febrian Akbara Rizqi',
            'Abdullah Salim',
            'Nana Ainiya Khalidah',
            'Rahmat Alfiansyah',
            'Fauzan kurniawan',
            'Hafid Mustofa Yahya',
            'Moch. Isro Oktaviyanto',
            'Sandy Kusumajati',
            'Suryo Dipoyudho',
            'Rizal Surya Nuraziz',
            'Bima Rindi Prasetyo',
            'Adenia Ayu Safira',
            'Reza Larasati',
            'Shalzadilla Nova Cahyaningwandini',
            'Riska Mahrotul Kibtiyah',
            'Dwi Rismaya',
            'Natasya Oktavia Widodo',
            'Siti Nur Ida Chayati',
            'Happy Fajar Arie Anggara',
        ];
        $nickname = [
            'Amin',
            'Fahri',
            'Fabio',
            'Faktur',
            'Ilham',
            'Febrian',
            'Abdullah',
            'Nana',
            'Rahmat',
            'Fauzan',
            'Hafid',
            'Moch. Isro',
            'Sandy',
            'Suryo',
            'Rizal',
            'Bima',
            'Adenia',
            'Reza',
            'Shalzadilla',
            'Riska',
            'Dwi Rismaya',
            'Natasya',
            'Siti',
            'Fajar',
        ];
        $id_prodi = [
            '1',
            '1',
            '1',
            '2',
            '2',
            '2',
            '2',
            '4',
            '4',
            '4',
            '5',
            '5',
            '3',
            '3',
            '3',
            '3',
            '10',
            '10',
            '10',
            '9',
            '9',
            '7',
            '7',
            '7',
        ];
        $visi = [
            'Menjadikan HIMATIF sebagai wadah aspirasi mahasiswa Program Studi Tekonologi Informasi dan turut andil dalam pengembangan dan pemberdayaan sumber daya manusia di dalamnya',
            'mewujutkan himatif sebagai himpunan mahasiswa program studi teknologi informasi yang berperan aktif  dan dinamis di dalam maupun diluar kampus  Politeknik Negeri Madiun, serta membantu menciptakan mahasiswa yang solid, inovatif dan berintegritas',
            'Menjadikan HIMATIF sebagai wadah yang menjunjung tinggi nilai kekeluargaan dan kebersamaan',
            'Mewujudkan HIMATEKOM menjadi organisasi kemahasiswaan yang lebih aktif, mempunyai rasa kekeluargaan, profesional, dan dapat menjadi wadah aspirasi bagi mahasiswa HIMATEKOM.',
            'Menjadikan HIMATEKOM yang berkualitas dan profesional untuk mewujudkan insan mahasiswa yang aktif, solutif, responsif dan bertanggung jawab dalam membangun kemajuan organisasi bersama.',
            'Menjadikan Himpunan Mahasiswa Teknik Komputer Kontrol sebagai suatu organisasi yang berkarakter, bersinergis, dan profesional berasaskan kekeluargaan untuk mahasiswa Teknik Komputer Kontrol yang berkualitas.',
            'Mewujudkan HIMATEKOM menjadi organisasi mahasiswa yang aktif, profesional dan solid dalam membangun kemajuan prodi TKK secara bersama-sama',
            'Mewujudkan HIMAMETO PNM menjadi wadah kaderisasi serta wadah pengembangan potensi seluruh anggota HIMAMETO dengan menjunjung tinggi rasa solidaritas dan relevan sesuai perkembangan zaman.',
            'Mewujudkan ikatan kekeluargaan harmonis dalam HIMAMETO guna mengembangkan prestasi, kreativitas, dan riset teknologi bagi anggota HIMAMETO yang berlandaskan solidaritas, loyalitas dan totalitas untuk berkarya',
            'Membangun keluarga Himameto yang unggul di segala bidang dengan komitmen dan loyalitas tinggi dengan berlandaskan nilai nilai solidaritas serta semangat menuju himameto yang lebih prestatif',
            'Menjadikan HIMA-TKA PNM yang unggul dan terkemuka, serta dapat mengembangkan jiwa solidaritas, pemikiran cerdas, dan sikap integritas sesuai dengan nilai nilai Pancasila',
            'Mewujudkan HIMA-TKA yang berintegritas, Intelektual, serta profesional dengan menjunjung tinggi rasa kekeluargaan',
            'Mewujudkan organisasi HIMATEKLIS sebagai organisasi yang menjunjung tinggi nilai solidaritas dan kekeluargaan.',
            'Mewujudkan Himateklis menjadi ormawa yang berkualitas, disiplin, harmonis, berdasarkan iman dan taqwa',
            'Menjadikan himpunan mahasiswa Teknik Listrik menjadi himpunan yang solid, berkualitas, nasionalis, kreatif, dan taqwa kepada tuhan yang maha esa.',
            'Mewujudkan HIMATEKLIS program studi terkait yang aktif, profesional dan solid dalam membangun kemajuan prodi bersama.',
            'Mewujudkan Himka yang aktif, solutif dan responsif guna membangun kemajuan prodi bersama',
            'Mewujudkan HIMKA sebagai ormawa yang berkarakter, kreatif, inovatif, dan berkualitas serta menjunjung tinggi nilai kekeluargaan.',
            'menjadikan himka suatu wadah pemersatu, penampung aspirasi dari mahasiswa kompak dan menjadi penyalur bakat untuk mahasiswa kompak sehingga terwujudnya himpunan yang solid dan bersinergi.',
            'Menjadikan HIMAKSI sebagai wadah, pemersatu serta penyalur aspirasi, minat, dan bakat mahasiswa Prodi Akuntansi dengan menjunjung nilai kekeluargaan.',
            'Menjadikan Himpunan Mahasiwa Program Studi Akuntansi sebagai organisasi yang memiliki keunggulan dibidang akademik atau non akademik dan selalu menuju nilai solidaritas tinggi.',
            'Terwujudnya HIMAPRODI ADBIS yg aktif, profesional dan solid dalam menunjang rasa kekeluargaan dan kebersamaan',
            'Perubahan berkelanjutan untuk HIMAPRODI ADBIS yang aktif, inovatif dan kolaboratif.',
            'Menjadikan Himaprodi Adbis semakin bernilai positif, berdedikasi, dan bermanfaat utamanya bagi Program Studi Administrasi Bisnis, Politeknik Negeri Madiun, hingga ruang lingkup terluasnya yakni masyarakat, Serta bersinergi dalam proses mewujudkan misi'
        ];

        for ($i = 0; $i < count($name); $i++) {
            $user = Hima::create([
                'name'              => $name[$i],
                'nickname'          => $nickname[$i],
                'id_prodi'          => $id_prodi[$i],
                'visi'              => $visi[$i],
            ]);
            $user->save();
        };
    }
}
