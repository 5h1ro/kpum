<?php

namespace Database\Seeders;

use App\Models\MisiBem;
use Illuminate\Database\Seeder;

class MisiBemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $detail = [
            'Menciptakan internal BEM KM PNM yang loyal dan berdedikasi kepada Tri Dharma Perguruan Tinggi.',
            'Menjalin sinergitas antar-KM PNM dan Kampus sebagai bentuk Kolaborasi Konstruktif.',
            'Memberi sarana penunjang kreativitas dan pengembangan diri mahasiswa melalui perkembangan teknologi digital.',
            'Mempererat relasi dan kolaborasi dengan eksternal.',
            'Menciptakan internal BEM-KM PNM yang tangguh berlandaskan profesionalitas dan jiwa kekeluargaan.',
            'Memfasilitasi mahasiswa PNM untuk aktif berkarya, berkreasi dan berinovasi berdasarkan pengamalan Tri Dharma Perguruan Tinggi.',
            'Berkolaborasi penuh untuk mewujudkan relasi yang komunikatif antarorganisasi internal maupun eksternal kampus.',
            'Menyelaraskan ruang pemahaman bagi KM PNM mengenai birokrasi suatu organisasi dan memberikan ruang kerjasama bagi KM PNM untuk melakukan aksi nyata yang tidak hanya sekadar bersuara.',
            'Membentuk struktur BEM KM-PNM yang kokoh dalam lingkup internal maupun eksternal BEM berlandaskan kekeluargaan',
            'Mewujudkan harmonisasi antar keluarga mahasiswa PNM dan berkontribusi dalam pengabdian masyarakat',
            'Aktif dan tanggung jawab dalam menyalurkan aspirasi mahasiswa PNM',
            'Membangun sinergitas dan mengembangkan potensi minat dan bakat mahasiswa.'
        ];

        $id_bem = [
            '1',
            '1',
            '1',
            '1',
            '2',
            '2',
            '2',
            '2',
            '3',
            '3',
            '3',
            '3',
        ];

        for ($i = 0; $i < count($detail); $i++) {
            $user = MisiBem::create([
                'detail'            => $detail[$i],
                'id_bem'            => $id_bem[$i]
            ]);
            $user->save();
        };
    }
}
