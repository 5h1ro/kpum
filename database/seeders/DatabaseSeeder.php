<?php

namespace Database\Seeders;

use App\Models\MisiBem;
use App\Models\Superadmin;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RoleSeeder::class,
            UserSeeder::class,
            AdminSeeder::class,
            SuperadminSeeder::class,
            JurusanSeeder::class,
            ProdiSeeder::class,
            // VoterSeeder::class,
            BemSeeder::class,
            MisiBemSeeder::class,
            DpmSeeder::class,
            MisiDpmSeeder::class,
            HmjSeeder::class,
            MisiHmjSeeder::class,
            HimaSeeder::class,
            MisiHimaSeeder::class,
            // AdbisSeeder::class,
            // AkSeeder::class,
            // BingSeeder::class,
            // KompakSeeder::class,
            // MetoSeeder::class,
            // TeklisSeeder::class,
            TiSeeder::class,
            // TkkSeeder::class,
            // TkaSeeder::class,
            // TplSeeder::class,
            SetupSeeder::class,
        ]);
    }
}
