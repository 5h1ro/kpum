<?php

namespace Database\Seeders;

use App\Models\Dpm;
use Illuminate\Database\Seeder;

class DpmSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $name = [
            'Bagus Batra Buana Hadi Is',
            'Muhammad Rossi Fartazulloh',
            'Siska Damayanti',
            'Silvy Ayu Kusumawardani',
            'Fajar Dwi Utomo',
            'Azizah Yulia Azhari',
            'Wahyu Afan Nurrosyid',
            'Arie Fredyansyah',
            'Adam Putra Alfatan',
            'Mochamad Helmy Marshanda. H',
            'Reza Dwi Syahputra',
            'Muhammad Yahya Dzulfikri',
            'Muhammad Imam Bastowi',
            'Bimantoro Wahyu',
            'Febiana Eka Permatasari',
            'Ariska Febriyanti Putri',
            'Ferdinan Eka Wicaksana',
            'Nabila Oktavia Anggita Putri',
            'Bela Dwi Cantika',
            'Devi Agil Anjelina',
            'Kharisma Shindi',
            'Citra Elivia Febriana',
            'Natasya Putri  Digoeliandini',
            'Hadaina Ridhwanah',
            'Syifa Adya Cahya',
            'Tectona Grandis Erlangga',
            'Rafael Slamet Firdaus',
            'Annida Arawinda Nareshwara',
        ];
        $nickname = [
            'Bagus',
            'Rossi',
            'Siska',
            'Silvy',
            'Fajar',
            'Azizah',
            'Wahyu',
            'Arie',
            'Adam',
            'Helmy',
            'Reza',
            'Yahya',
            'Imam',
            'Bimantoro',
            'Febiana',
            'Ariska',
            'Ferdinan',
            'Nabila',
            'Bela',
            'Devi',
            'Kharisma',
            'Citra',
            'Natasya',
            'Hadaina',
            'Syifa',
            'Tectona',
            'Rafael',
            'Annida',
        ];
        $id_prodi = [
            '1',
            '1',
            '1',
            '1',
            '2',
            '2',
            '3',
            '3',
            '3',
            '3',
            '3',
            '3',
            '4',
            '4',
            '5',
            '7',
            '7',
            '7',
            '9',
            '9',
            '9',
            '10',
            '10',
            '10',
            '8',
            '8',
            '5',
            '7',
        ];
        $visi = [
            'Mengoptimalkan Dewan Perwakilan Mahasiswa sebagai Lembaga Legislatif yang menjunjung tinggi nilai Pancasila dengan asas kebersamaan dan kekeluargaan',
            'Terwujudnya DPM Politeknik Negeri Madiun yang sinergis dan aspiratif',
            'Menjadikan DPM Politeknik Negri Madiun sebagai organisasi yang selaras, konstributif, peduli, harmonis dan komunikatif dengan berlandaskan kekeluargaan.',
            'Mewujudkan Dewan Perwakilan Mahasiswa Politeknik Negeri Madiun sebagai lembaga legislatif yang solid, professional, peduli, dinamis, dan harmonis dengan berlandaskan kekeluargaan serta ketaqwaan kepada Tuhan Yang Maha Esa',
            'Mewujudkan DPM KM-PNM sebagai lembaga legislatif yang profesional, sinergis, aspiratif dan berkontributif dengan berlandaskan semangat kekeluargaan',
            'Menjadikan DPM-KM PNM sebagai organisasi lembaga legislatif dan yudikatif yang dapat bertanggung jawab, aspiratif, berintegritas tinggi, serta berkontribusi secara aktif untuk kampus',
            'Menciptakan mahasiswa mahasiswi untuk memiliki jiwa yang Tangguh ,mandiri, terampil, dan Berakhlak baik. Selain itu, Saya juga ingin menjadikan teman teman mahasiswa saling bahu membahu dan tolong menolong dalam hal akademik maupun non akademik.',
            'DPM PNM yang profesional dan mampu menjalankan aspirasi',
            'Mewujudkan kinerja DPM KM-PNM yang sehat secara sistematis , Professional secara personal, dan berintegritas secara kebersamaan.',
            'Menjadikan DPM lembaga yang pro aktif mampu bertindak dan berfikir kritis dalam melihat isu kekinian dengan mengutamakan kepentingan kesejahteraan mahasiswa.',
            'Menjadikan DPM-KM PNM sebagai rumah aspirasi yang baik, dalam menyuarakan aspirasi keluarga mahasiswa PNM demi terciptanya harmonisasi.',
            'menciptakan lingkungan kampus yang nyaman untuk belajar dan dapat menjadikan kampus sebagai rumah kedua bagi mahasiswa untuk belajar dan beraktivitas dengan mahasiswa lainnya.',
            'Menjadikan DPM KM PNM organisasi mahasiswa yang aktif, kreatif, dan Inovatif dalam melaksanakan program kerja nya.',
            'Mewujudkan DPM ( Dewan Perwakilan Mahasiswa) yang cekatan, terbuka dan tanggap terhadap usulan atau tanggapan mahasiswa',
            'Terwujudnya Dewan Perwakilan Mahasiswa Politeknik Negeri Madiun sebagai organisasi mahasiswa yang dapat mengembangkan rasa tanggung jawab, inspiratif, kontributif, dengan melaksanakan tugas dan wewenang dalam bidang legislaatif dan yudikatif secara efektif.',
            'Menjadikan DPM Politeknik Negeri Madiun yang lebih maju dan berkembang, serta mewujudkan anggota DPM yang dinamis dan harmonis berlandaskan kekeluargaan',
            'Menjadikan DPM PNM yang profesional dan mampu menyalurkan aspirasi-aspirasi mahasiswa.',
            'Menjadikan DPM Politeknik Negeri Madiun sebagai rumah aspirasi yang produktif,mengakar, dalam menyuarakan aspirasi demi terciptanya harmonisasi dan sinergitas antar sesama yang disiplin,kritis,berwawasan dan beakhlakul karimah.',
            'Menjadikan DPM sebagai lembaga penampung aspirasi mahasiswa',
            'Mewujudkan DPM yang profesional, aktif, dan aspiratif dalam menjalankan fungsi serta wewenangnya sebagai Lembaga Legislatif Mahasiswa"',
            'Terwujudnya DPM-KM PNM yang mengaktualisasi dalam peran kelembagaan serta mengemban tanggung jawab',
            'Terwujudnya Dewan Perwakilan Mahasiswa (DPM) sebagai Lembaga legislative yang bertanggung jawab, disiplin, kreatif, inspiratif, komunikatif, kontributif dan menjunjung tinggi nilai Pancasila.',
            'Mewujudkan Dewan Perwakilan Mahasiswa PNM menjadi lembaga legislatif yang komunikatif, non diskriminatif, berintelektual serta demokratif',
            'Berperan aktif dalam mewujudkan visi dan misi DPM-KM PNM',
            'Terwujudnya lembaga legislatif yang profesional, produktif , solid, disiplin, tanggung jawab serta peduli terhadap sesama.',
            'Mewujudkan DPM KM PNM yang profesional, aspiratif, inovatif, harmonis dan transparan sebagai lembaga legislatif mahasiswa',
            'Terwujudnya ORMAWA yang berdedikasi tinggi dan berakhlak mulia',
            'Mewujudkan DPM sebagai organisasi legislative mahasiswa yang selaras dan kontributif',
        ];

        for ($i = 0; $i < count($name); $i++) {
            $user = Dpm::create([
                'name'              => $name[$i],
                'nickname'          => $nickname[$i],
                'id_prodi'          => $id_prodi[$i],
                'visi'              => $visi[$i],
            ]);
            $user->save();
        };
    }
}
