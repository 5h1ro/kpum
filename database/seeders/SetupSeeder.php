<?php

namespace Database\Seeders;

use App\Models\Setup;
use Illuminate\Database\Seeder;

class SetupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $name = [
            'Muhammad Rio Pratama',
        ];

        $npm = [
            '213305064',
        ];

        $current_periode = [
            '2021/2022',
        ];

        $next_periode = [
            '2022/2023',
        ];

        $no = [
            '30',
        ];

        for ($i = 0; $i < count($name); $i++) {
            $user = Setup::create([
                'name'      => $name[$i],
                'npm'       => $npm[$i],
                'no'       => $no[$i],
                'current_periode'       => $current_periode[$i],
                'next_periode'       => $next_periode[$i],
            ]);
            $user->save();
        };
    }
}
