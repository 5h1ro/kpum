<?php

namespace Database\Seeders;

use App\Models\Hmj;
use Illuminate\Database\Seeder;

class HmjSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $name = [
            'Okta Dilla Risqy',
            'Dewa Auditiya Putra Pratama',
            'Reza Putri Andriyani',
            'Alen Azharun',
            'Enggi Nurcahyo',
            'Sandyka Hanggayuh Windarmoko',
            'Windi Merlita Sari',
            'Ni Luh Made Ayu Sawitri',
            'Dinda Nurjannah',
            'Lumban Cahayaningrum',
        ];
        $nickname = [
            'Okta',
            'Dewa',
            'Reza',
            'Alen',
            'Enggi',
            'Sandyka',
            'Windi',
            'Ni Luh Made',
            'Dinda',
            'Lumban',
        ];
        $id_jurusan = [
            '1',
            '1',
            '1',
            '3',
            '3',
            '3',
            '3',
            '2',
            '2',
            '2',
        ];
        $id_prodi = [
            '4',
            '1',
            '5',
            '10',
            '9',
            '9',
            '10',
            '7',
            '8',
            '7',
        ];
        $visi = [
            'Menjadikan HMJT sebuah Lembaga kemahasiswaan yang berperan aktif, mempunyai rasa solidaritas, profesionalisme yang tinggi, dan dapat menjadi wadah aspirasi bagi mahasiswa Teknik PNM.',
            'Mewujudkan HMJT sebagai organisasi yang aktif, kreatif, inovatif, serta menjunjung tinggi nilai kekeluargaan',
            'Mewujudkan Himpunan Mahasiswa Jurusan Teknik yang bersatu, professional, berkualitas, dan bersifat kekeluargaan demi tercapainya visi misi HMJT dengan semangat juang Progressive Perseverent',
            'Mewujudkan HMJKA sebagai organisasi yang berperan aktif dalam terbentuknya mahasiswa kreatif,komunikatif serta menjunjung tinggi nilai kekeluargaan dan solidaritas.',
            'Menjadikan HMJ Komputer Akuntansi sebagai rumah  kreatifitas dan aspirasi  mahasiswa yang aktif dan responsive guna  mewujudkan mahasiswa yang saling bersinergi, mampu berdaya saing,dan produktif.',
            'Mewujudkan HMJKA sebagai suatu organisasi yg memiliki jiwa sosial tinggi, unggul, kreatif inovatif dan berakhlak mulia sehingga dapat menjadi wadah bagi mahasiswa jurusan komputer akuntansi yg aktif di bidang akademik maupun non akademik',
            'Menjadikan HMJKA sebagai wadah aspirasi, aksi, dan kolaborasi pengembangan potensi diri mahasiswa jurusan komputer akuntansi di bidang akademik maupun non akademik',
            'Menjadikan HMJ yang lebih aktif, berkualitas tinggi, profesional, dengan kekeluargaan yang baik',
            'Menjadikan Himpunan Mahasiswa Administrasi Bisnis sebagai wadah pemersatu, penampung aspirasi dan penyalur bakat mahasiswa. Jurusan Administrasi Bisnis sehingga terwujud himpunan yang solid dan bersinergi.',
            'Mewujudkan Himpunan Mahasiswa Jurusan Administrasi Bisnis yang berkontribusi aktif, aspiratif, dan progresif di lingkup jurusan.',
        ];

        for ($i = 0; $i < count($name); $i++) {
            $user = Hmj::create([
                'name'              => $name[$i],
                'nickname'          => $nickname[$i],
                'id_jurusan'        => $id_jurusan[$i],
                'id_prodi'          => $id_prodi[$i],
                'visi'              => $visi[$i],
            ]);
            $user->save();
        };
    }
}
