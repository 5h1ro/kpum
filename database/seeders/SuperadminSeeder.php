<?php

namespace Database\Seeders;

use App\Models\Superadmin;
use Illuminate\Database\Seeder;

class SuperadminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $name = [
            'Dany',
        ];

        $id_user = [
            '1',
        ];

        for ($i = 0; $i < count($name); $i++) {
            $user = Superadmin::create([
                'name'             => $name[$i],
                'id_user'          => $id_user[$i],
            ]);
            $user->save();
        };
    }
}
