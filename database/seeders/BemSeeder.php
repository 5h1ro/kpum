<?php

namespace Database\Seeders;

use App\Models\Bem;
use Illuminate\Database\Seeder;

class BemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $name1 = [
            'Moh. Taufiq Mahendra',
            'Yusuf Wijaya Kusuma Putra',
            'Sumiaji'
        ];
        $name2 = [
            'Siti Sarah',
            'Rifqi Mukti Darmawan',
            'Muktamiroh'
        ];
        $nickname1 = [
            'Taufiq',
            'Yusuf',
            'Sumiaji'
        ];
        $nickname2 = [
            'Siti',
            'Rifqi',
            'Muktamiroh'
        ];
        $id_prodi1 = [
            '5',
            '2',
            '4'
        ];
        $id_prodi2 = [
            '10',
            '5',
            '9'
        ];
        $visi = [
            'Terwujudnya BEM KM PNM sebagai wadah Inklusif dan Aspiratif demi memaksimalkan potensi dan meningkatkan kinerja KM PNM',
            'BEM KM PNM sebagai ruang ekspresi mahasiswa',
            'Mewujudkan BEM KM-PNM yang komunikatif, solutif, dan inspiratif serta kontributif untuk membangun potensi keluarga mahasiswa PNM'
        ];


        for ($i = 0; $i < count($name1); $i++) {
            $user = Bem::create([
                'name1'             => $name1[$i],
                'name2'             => $name2[$i],
                'nickname1'         => $nickname1[$i],
                'nickname2'         => $nickname2[$i],
                'id_prodi1'         => $id_prodi1[$i],
                'id_prodi2'         => $id_prodi2[$i],
                'visi'              => $visi[$i],
            ]);
            $user->save();
        };
    }
}
