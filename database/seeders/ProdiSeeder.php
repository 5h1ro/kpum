<?php

namespace Database\Seeders;

use App\Models\Prodi;
use Illuminate\Database\Seeder;

class ProdiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $name = [
            'D3 Teknologi Informasi',
            'D3 Teknik Komputer Kontrol',
            'D3 Teknik Listrik',
            'D3 Mesin Otomotif',
            'D4 Perkeretaapian',
            'D2 Teknik Pembentukan Logam',
            'D3 Administrasi Bisnis',
            'D3 Bahasa Inggris',
            'D3 Akuntansi',
            'D3 Komputerisasi Akuntansi',
        ];

        $jurusan = [
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '2',
            '2',
            '3',
            '3',
        ];

        for ($i = 0; $i < count($name); $i++) {
            $user = Prodi::create([
                'name'             => $name[$i],
                'id_jurusan'       => $jurusan[$i],
            ]);
            $user->save();
        };
    }
}
