<?php

namespace Database\Seeders;

use App\Models\MisiHmj;
use Illuminate\Database\Seeder;

class MisiHmjSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $detail = [
            'Meningkatkan kebersamaan dan kekeluargaan antar Himpunan Mahasiswa Jurusan Teknik.',
            'Pengembangan sinergitas antara HMJT dengan lembaga internal maupun eksternal PNM',
            'Meningkatkan peran aktif mahasiswa teknik dalam berorganisasi, menyalurkan bakat, dan kepedulian terhadap masyarakat',
            'Merealisasikan aspirasi sesuai fungsi dan kebutuhan mahasiswa teknik secara optimal',
            'Menjadikan Himpunan sebagai sarana untuk meningkatkan kualitas mahasiswa teknik di bidang Akademik maupun Non-Akademik dengan memiliki jiwa tanggung jawab dalam menjalankan amanah',
            'Membuat dan melaksanakan program kerja yang memiliki cakupan luas dan dapat bermanfaat bagi seluruh mahasiswa teknik',
            'Membangun koordinasi yang aktif dan berkesinambungan dengan seluruh elemen yang meliputi Internal maupun eksternal kampus Politeknik Negeri Madiun',
            'Membangun komunikasi yang baik antar keluarga Mahasiswa Jurusan Teknik',
            'Mewadahi kegiatan dan penyaluran aspirasi, minat bakat,  dan bertukar pikiran (musyawarah) dengan asas kekeluargaan, gotong royong dan persatuan',
            'Menumbuhkan semangat juang Himpunan Mahasiswa Jurusan Teknik kepada Seluruh Mahasiswa Teknik',
            'Menjalin hubungan baik antar Ormawa yang ada di Politeknik Negeri Madiun dan menjalin silaturahmi dengan Ormawa dari luar Politeknik Negeri Madiun',
            'Meningkatkan kekeluargaan serta solidaritas antar pengurus dan anggota hmjka',
            'Meningkatkan kualitas mahasiswa jurusan komputer akuntansi di bidang akademik maupun non akademik melalui program kerja',
            'Menampung dan menyalurkan aspirasi mahasiswa jurusan komputer akuntansi',
            'Mengoptimalkan kinerja dalam melaksanakan program kerja',
            'Mengoptimalkan kinerja HMJ komputer akuntansi yang solid,kompak serta menjujung tinggi nilai solidaritas,kebersamaan,tanggung jawab,dan selalu memegang prinsip TERDIGITALIS (Terbentuk,Disiplin,Giat,dan Totalitas) agar membentuk karakter anggota yang bermoral dan bertanggung jawab.',
            'Melaksanakan Program Kerja HMJ Komputer Akuntansi yang dapat menciptakan hubungan positif dan harmonis bagi mahasiswa jurusan Komputer Akuntansi.',
            'Menjadikan HMJ komputer akuntansi sebagai organisasi yang adaptif terhadap perkembangan zaman guna memaksimalkan potensi  yang berprestasi bagi seluruh elemen mahasiswa jurusan komputer akuntansi.',
            'Menjadikan HMJ komputer akuntansi sebagai mediator dalam menampung,menyalurkan,dan menindak lanjuti aspirasi  guna mewujudkan  seluruh civitas akademika yang saling bersinergi.',
            'Meningkatkan relasi baik dilingkungan internal maupun ekstrnal demi kemajuan HMJKA yang lebih baik lagi',
            'Melanjutkan, memperbaiki dan memperbarui program kerja yg belum maupun sudah terlaksana agar menjadi lebih baik lagi',
            'Merencanakan dan menjalankan program kerja dengan tanggung jawab dan semangat yg tinggi',
            'Mengoptimalkan peran dan fungsi HMJKA sebagai wadah aspirasi mahasiswa jurusan komputer akuntansi',
            'Menjadi media penyampaian aspirasi dan informasi yang interaktif serta bermanfaat bagi mahasiswa jurusan komputer akuntansi',
            'Membangun kolaborasi nyata antara HMJ dengan Hima dan mahasiswa jurusan untuk menciptakan mahasiswa yang cakap',
            'Meningkatkan empati serta sikap tanggap mahasiswa terhadap keadaan sosial masyarakat',
            'Memberikan ruang untuk peningkatan minat bakat mahasiswa terutama dalam hal peningkatan softskill',
            'Berperan sebagai wadah dan penyalur aspirasi mahasiswa jurusan adbis',
            'Membangun semangat juang dalam berorganisasi',
            'Mengoptimalkan setiap kegiatan yang melibatkan partisipasi aktif mahasiswa jurusan adbis',
            'Menjadi wadah kegiatan, penyalur aspirasi, minat, bakat dan tempat tukar pikiran dengan asas kekeluargaan',
            'Mengembangkan dan menyalurkan potensi dari setiap mahasiswa untuk maju dan berkembang',
            'Menjalin hubungan yang baik dengan civitas, alumni, serta organisasi atau lembaga lainnya khususnya di lingkungan Politeknik Negeri Madiun',
            'Menyelenggarakan dan melanjutkan kegiatan yang dapat mendukung tercapainya mahasiswa yang aktif, memiliki solidaritas, berwawasan dan keterampilan yang kompeten',
            'Meningkatkan dukungan terhadap civitas akademia jurusan administrasi bisnis di bidang akademik maupun non akademik',
            'Mengoptimalkan peran dan kinerja HMJAB yang mengedepankan aksi, inovasi, dan transparansi',
            'Mengayomi seluruh elemen yang meliputi Jurusan Administrasi Bisnis dengan dilandasi rasa kekeluargaan',
        ];

        $id_hmj = [
            '1',
            '1',
            '1',
            '1',
            '2',
            '2',
            '2',
            '3',
            '3',
            '3',
            '3',
            '4',
            '4',
            '4',
            '4',
            '5',
            '5',
            '5',
            '5',
            '5',
            '6',
            '6',
            '6',
            '7',
            '7',
            '7',
            '7',
            '8',
            '8',
            '8',
            '9',
            '9',
            '9',
            '9',
            '10',
            '10',
            '10',
        ];

        for ($i = 0; $i < count($detail); $i++) {
            $user = MisiHmj::create([
                'detail'            => $detail[$i],
                'id_hmj'            => $id_hmj[$i]
            ]);
            $user->save();
        };
    }
}
