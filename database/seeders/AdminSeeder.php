<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $name = [
            'Hqq',
        ];

        $id_user = [
            '2',
        ];

        for ($i = 0; $i < count($name); $i++) {
            $user = Admin::create([
                'name'             => $name[$i],
                'id_user'          => $id_user[$i],
            ]);
            $user->save();
        };
    }
}
