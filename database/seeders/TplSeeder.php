<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Voter;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class TplSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $email = [
            'achmadfaqih1506@gmail.com',
            'mohamadadalaishalam@gmail.com',
            'agungnur914@gmail.com',
            'alcatur212@gmail.com',
            'kasturiandris@gmail.com',
            'bagusferdianto123@gmail.com',
            'bujahandriawan46@gmail.com',
            'dickyfirmansyah814@gmail.com',
            'difahamdani43@gmail.com',
            'putrad734@gmail.com',
            'fajaryoyon23@gmail.com',
            'fajaryusufhamdani123@gmail.com',
            'febriaryatama479@gmail.com',
            'firdausputrakikim@gmail.com',
            'frediprayoga97@gmail.com',
            'gabrieltegar05425@gmail.com',
            'rizkysandro9@gmail.com',
            'alicover02@gmail.com',
            'wardhani1233@gmail.com',
            'riskysyahrul3@gmail.com',
            'zainnurofiq97@gmail.com',
        ];
        $password = [
            '6Jo26kGG',
            '4Df20qXS',
            '6Bz09aLK',
            '4Df20qXS',
            '2Rg56sZB',
            '4Is28vBU',
            '4Us68cAC',
            '9Az82gAN',
            '4Xp24xWJ',
            '5Mt39cJI',
            '5Mt39cJI',
            '1Jt91xDS',
            '1Hn09kWL',
            '9Ig31lCY',
            '8Hf24iAV',
            '3Yo45tON',
            '4Kb40iZB',
            '1Hn09kWL',
            '4Hs60qTX',
            '0Bz45aWQ',
            '3Yo45tON',
        ];

        $name = [
            'Achmad Faqih Hendriarto',
            'Ade Kurniawan Krisyandi ',
            'Agung Nurcahyo',
            'Aldi Catur Wicaksono',
            'Andris Kasturi',
            'Bagus Ferdianto',
            'Buja Handriawan',
            'Dicky Firmansyah',
            'Difa Hamdani',
            'Dimas Putra Pratama',
            'Fajar Ardiansyah',
            'Fajar Yusuf Hamdani',
            'Febri Aryatama',
            'Firdaus Dita Rahmatullah',
            'Fredi Prayoga Adi Pratama',
            'Gabriel Tegar Pratama',
            'M. Nour Rizki Sandro Aprilyo',
            'Muthok Ali Muksin',
            'Rico Sholeh Wardhani',
            'Risqi Syahrul Ramadhan',
            'Zain Nur Rofiq',
        ];
        $number = [
            '6285606396160',
            '6285748964486',
            '6283833446259',
            '6283137050528',
            '6285648355229',
            '6282132893382',
            '6282143494069',
            '6287701237304',
            '6285791197215',
            '6281933042343',
            '6285704283622',
            '6281466852033',
            '6282333687992',
            '6288235215981',
            '6285655850045',
            '6285730311130',
            '6281336760727',
            '628979481131',
            '6285704001966',
            '6281529410968',
            '6285764898842',
        ];

        $id_jurusan = [
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
            '1',
        ];

        $id_prodi = [
            '6',
            '6',
            '6',
            '6',
            '6',
            '6',
            '6',
            '6',
            '6',
            '6',
            '6',
            '6',
            '6',
            '6',
            '6',
            '6',
            '6',
            '6',
            '6',
            '6',
            '6',
        ];

        for ($i = 0; $i < count($email); $i++) {
            $user = User::create([
                'email'           => $email[$i],
                'password'        => Hash::make($password[$i])
            ]);
            $user->save();
            $voter = Voter::create([
                'name'           => $name[$i],
                'number'         => $number[$i],
                'id_jurusan'     => $id_jurusan[$i],
                'id_prodi'       => $id_prodi[$i],
                'id_user'        => $user->id,
            ]);
            $voter->save();
        };
    }
}
