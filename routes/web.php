<?php

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\BemCandidateController;
use App\Http\Controllers\Admin\BemController;
use App\Http\Controllers\Admin\CalonController;
use App\Http\Controllers\Admin\CandidateController as AdminCandidateController;
use App\Http\Controllers\Admin\DpmCandidateController;
use App\Http\Controllers\Admin\DpmController;
use App\Http\Controllers\Admin\HimaCandidateController;
use App\Http\Controllers\Admin\HimaController;
use App\Http\Controllers\Admin\HmjCandidateController;
use App\Http\Controllers\Admin\HmjController;
use App\Http\Controllers\Admin\ProfileController as AdminProfileController;
use App\Http\Controllers\Admin\ReportController;
use App\Http\Controllers\Admin\ScheduleController;
use App\Http\Controllers\Admin\SetupController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\CandidateController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\Superadmin\BemController as SuperadminBemController;
use App\Http\Controllers\Superadmin\CalonController as SuperadminCalonController;
use App\Http\Controllers\Superadmin\DpmController as SuperadminDpmController;
use App\Http\Controllers\Superadmin\HimaController as SuperadminHimaController;
use App\Http\Controllers\Superadmin\HmjController as SuperadminHmjController;
use App\Http\Controllers\Superadmin\ProfileController as SuperadminProfileController;
use App\Http\Controllers\Superadmin\RoleController;
use App\Http\Controllers\Superadmin\ReportController as SuperadminReportController;
use App\Http\Controllers\Superadmin\ScheduleController as SuperadminScheduleController;
use App\Http\Controllers\Superadmin\SuperadminController;
use App\Http\Controllers\Superadmin\UserController as SuperadminUserController;
use App\Http\Controllers\Voter\BemController as VoterBemController;
use App\Http\Controllers\Voter\DpmController as VoterDpmController;
use App\Http\Controllers\Voter\HimaController as VoterHimaController;
use App\Http\Controllers\Voter\HmjController as VoterHmjController;
use App\Http\Controllers\Voter\PemilihanController;
use App\Http\Controllers\Voter\ProfileController as VoterProfileController;
use App\Http\Controllers\Voter\VoterController;
use Carbon\Carbon;
use Illuminate\Routing\RouteGroup;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use PHPUnit\TextUI\XmlConfiguration\Group;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [LoginController::class, 'home'])->name('start');

Route::get('/register-candidate', [CandidateController::class, 'register_candidate'])->name('register-candidate');
// bem
Route::get('/register-candidate-bem', [CandidateController::class, 'register_candidate_bem'])->name('register-candidate-bem');
Route::post('/register-candidate-bem-post', [CandidateController::class, 'bem_post'])->name('register-candidate-bem-post');
// dpm
Route::get('/register-candidate-dpm', [CandidateController::class, 'register_candidate_dpm'])->name('register-candidate-dpm');
Route::post('/register-candidate-dpm-post', [CandidateController::class, 'dpm_post'])->name('register-candidate-dpm-post');
// hmj
Route::get('/register-candidate-hmj', [CandidateController::class, 'register_candidate_hmj'])->name('register-candidate-hmj');
Route::post('/register-candidate-hmj-post', [CandidateController::class, 'hmj_post'])->name('register-candidate-hmj-post');
// hima
Route::get('/register-candidate-hima', [CandidateController::class, 'register_candidate_hima'])->name('register-candidate-hima');
Route::post('/register-candidate-hima-post', [CandidateController::class, 'hima_post'])->name('register-candidate-hima-post');

Route::get('/register-user', [LoginController::class, 'regis'])->name('regis');

Route::post('/register-create', [LoginController::class, 'register'])->name('register-create');

Route::get('/out', [LoginController::class, 'logout'])->name('out');

Auth::routes();

Route::middleware(['auth'])->group(function () {

    Route::get('/home', [HomeController::class, 'index'])->name('home');

    Route::middleware(['superadmin'])->group(function () {
        Route::get('superadmin', [SuperadminController::class, 'index'])->name('superadmin');
        Route::get('superadmin-role', [RoleController::class, 'index'])->name('sa-role');
        Route::post('superadmin-role-edit', [RoleController::class, 'edit'])->name('sa-role-edit');
        // profile
        Route::get('sa-profile', [SuperadminProfileController::class, 'index'])->name('sa-profile');
        Route::post('sa-profile-edit', [SuperadminProfileController::class, 'edit'])->name('sa-profile-edit');

        //calon
        Route::get('sa-data-calon', [SuperadminCalonController::class, 'index'])->name('sa-data-calon');
        //bem
        Route::get('sa-bem-calon', [SuperadminBemController::class, 'index'])->name('sa-bem-calon');
        Route::post('sa-bem-calon-add', [SuperadminBemController::class, 'add'])->name('sa-bem-calon-add');
        Route::post('sa-bem-calon-update={id}', [SuperadminBemController::class, 'update'])->name('sa-bem-calon-update');
        Route::get('sa-bem-calon-delete', [SuperadminBemController::class, 'index'])->name('sa-bem-calon-delete');
        //dpm
        Route::get('sa-dpm-calon', [SuperadminDpmController::class, 'index'])->name('sa-dpm-calon');
        Route::post('sa-dpm-calon-add', [SuperadminDpmController::class, 'add'])->name('sa-dpm-calon-add');
        Route::post('sa-dpm-calon-update={id}', [SuperadminDpmController::class, 'update'])->name('sa-dpm-calon-update');
        Route::get('sa-dpm-calon-delete', [SuperadminDpmController::class, 'index'])->name('sa-dpm-calon-delete');
        //hmj
        Route::get('sa-hmj-calon', [SuperadminHmjController::class, 'index'])->name('sa-hmj-calon');
        Route::get('sa-hmj-calon-add', [SuperadminHmjController::class, 'index'])->name('sa-hmj-calon-add');
        Route::post('sa-hmj-calon-update={id}', [SuperadminHmjController::class, 'update'])->name('sa-hmj-calon-update');
        Route::get('sa-hmj-calon-delete', [SuperadminHmjController::class, 'index'])->name('sa-hmj-calon-delete');
        //hioma
        Route::get('sa-hima-calon', [SuperadminHimaController::class, 'index'])->name('sa-hima-calon');
        Route::get('sa-hima-calon-add', [SuperadminHimaController::class, 'index'])->name('sa-hima-calon-add');
        Route::post('sa-hima-calon-update={id}', [SuperadminHimaController::class, 'update'])->name('sa-hima-calon-update');
        Route::get('sa-hima-calon-delete', [SuperadminHimaController::class, 'index'])->name('sa-hima-calon-delete');

        // user
        Route::get('sa-data-user', [SuperadminUserController::class, 'index'])->name('sa-data-user');
        Route::post('sa-data-user-edit={id}', [SuperadminUserController::class, 'edit'])->name('sa-data-user-edit');
        Route::post('sa-data-user-add', [SuperadminUserController::class, 'add'])->name('sa-data-user-add');
        Route::get('sa-data-user-delete={id}', [SuperadminUserController::class, 'delete'])->name('sa-data-user-delete');

        // schedule
        Route::get('sa-data-schedule', [SuperadminScheduleController::class, 'index'])->name('sa-data-schedule');
        Route::post('sa-data-schedule-edit={id}', [SuperadminScheduleController::class, 'edit'])->name('sa-data-schedule-edit');
        Route::post('sa-data-schedule-add', [SuperadminScheduleController::class, 'add'])->name('sa-data-schedule-add');
        Route::get('sa-data-schedule-delete={id}', [SuperadminScheduleController::class, 'delete'])->name('sa-data-schedule-delete');

        // report
        Route::get('sa-data-report', [SuperadminReportController::class, 'index'])->name('sa-data-report');
        Route::post('sa-data-report-edit={id}', [SuperadminReportController::class, 'edit'])->name('sa-data-report-edit');
        Route::post('sa-data-report-add', [SuperadminReportController::class, 'add'])->name('sa-data-report-add');
        Route::get('sa-data-report-delete={id}', [SuperadminReportController::class, 'delete'])->name('sa-data-report-delete');
    });

    Route::middleware(['admin'])->group(function () {
        Route::get('admin', [AdminController::class, 'index']);
        Route::get('admin-fresh', [AdminController::class, 'fresh'])->name('admin-fresh');
        // dpm
        Route::get('admin-dpm={id}', [AdminController::class, 'index_dpm'])->name('admin-dpm');
        Route::get('admin-fresh-dpm={id}', [AdminController::class, 'fresh_dpm'])->name('admin-fresh-dpm');
        // hmj
        Route::get('admin-hmj={id}', [AdminController::class, 'index_hmj'])->name('admin-hmj');
        Route::get('admin-fresh-hmj={id}', [AdminController::class, 'fresh_hmj'])->name('admin-fresh-hmj');
        // hima
        Route::get('admin-hima={id}', [AdminController::class, 'index_hima'])->name('admin-hima');
        Route::get('admin-fresh-hima={id}', [AdminController::class, 'fresh_hima'])->name('admin-fresh-hima');

        Route::prefix('data-calon')->group(function () {
            //calon
            Route::get('index', [CalonController::class, 'index'])->name('data-calon');
            //bem
            Route::get('bem-calon', [BemController::class, 'index'])->name('bem-calon');
            Route::post('bem-calon-add', [BemController::class, 'add'])->name('bem-calon-add');
            Route::post('bem-calon-update={id}', [BemController::class, 'update'])->name('bem-calon-update');
            Route::get('bem-calon-delete', [BemController::class, 'index'])->name('bem-calon-delete');
            Route::post('bem-calon-create', [BemController::class, 'create'])->name('bem-calon-create');
            //dpm
            Route::get('dpm-calon', [DpmController::class, 'index'])->name('dpm-calon');
            Route::post('dpm-calon-add', [DpmController::class, 'add'])->name('dpm-calon-add');
            Route::post('dpm-calon-update={id}', [DpmController::class, 'update'])->name('dpm-calon-update');
            Route::get('dpm-calon-delete', [DpmController::class, 'index'])->name('dpm-calon-delete');
            Route::post('dpm-calon-create', [DpmController::class, 'create'])->name('dpm-calon-create');
            //hmj
            Route::get('hmj-calon', [HmjController::class, 'index'])->name('hmj-calon');
            Route::get('hmj-calon-add', [HmjController::class, 'index'])->name('hmj-calon-add');
            Route::post('hmj-calon-update={id}', [HmjController::class, 'update'])->name('hmj-calon-update');
            Route::get('hmj-calon-delete', [HmjController::class, 'index'])->name('hmj-calon-delete');
            Route::post('hmj-calon-create', [HmjController::class, 'create'])->name('hmj-calon-create');
            //hioma
            Route::get('hima-calon', [HimaController::class, 'index'])->name('hima-calon');
            Route::get('hima-calon-add', [HimaController::class, 'index'])->name('hima-calon-add');
            Route::post('hima-calon-update={id}', [HimaController::class, 'update'])->name('hima-calon-update');
            Route::get('hima-calon-delete', [HimaController::class, 'index'])->name('hima-calon-delete');
            Route::post('hima-calon-create', [HimaController::class, 'create'])->name('hima-calon-create');
        });

        // user
        Route::get('data-user', [UserController::class, 'index'])->name('data-user');
        Route::post('data-user-edit={id}', [UserController::class, 'edit'])->name('data-user-edit');
        Route::post('data-user-add', [UserController::class, 'add'])->name('data-user-add');
        Route::get('data-user-delete={id}', [UserController::class, 'delete'])->name('data-user-delete');

        // schedule
        Route::get('data-schedule', [ScheduleController::class, 'index'])->name('data-schedule');
        Route::post('data-schedule-edit={id}', [ScheduleController::class, 'edit'])->name('data-schedule-edit');
        Route::post('data-schedule-add', [ScheduleController::class, 'add'])->name('data-schedule-add');
        Route::get('data-schedule-delete={id}', [ScheduleController::class, 'delete'])->name('data-schedule-delete');

        // profile
        Route::get('admin-profile', [AdminProfileController::class, 'index'])->name('admin-profile');
        Route::post('admin-profile-edit', [AdminProfileController::class, 'edit'])->name('admin-profile-edit');


        //candidate
        Route::get('admin-data-candidate', [AdminCandidateController::class, 'index'])->name('admin-data-candidate');
        //bem
        Route::get('admin-bem-candidate', [BemCandidateController::class, 'index'])->name('admin-bem-candidate');
        Route::get('admin-bem-candidate-detail={id}', [BemCandidateController::class, 'detail'])->name('admin-bem-candidate-detail');
        Route::get('admin-bem-candidate-delete={id}', [BemCandidateController::class, 'index'])->name('admin-bem-candidate-delete');
        Route::get('admin-bem-candidate-acc={id}', [BemCandidateController::class, 'acc'])->name('admin-bem-candidate-acc');
        //dpm
        //dpm
        Route::get('admin-dpm-candidate', [DpmCandidateController::class, 'index'])->name('admin-dpm-candidate');
        Route::get('admin-dpm-candidate-detail={id}', [DpmCandidateController::class, 'detail'])->name('admin-dpm-candidate-detail');
        Route::get('admin-dpm-candidate-delete={id}', [DpmCandidateController::class, 'index'])->name('admin-dpm-candidate-delete');
        Route::get('admin-dpm-candidate-acc={id}', [DpmCandidateController::class, 'acc'])->name('admin-dpm-candidate-acc');
        //hmj
        Route::get('admin-hmj-candidate', [HmjCandidateController::class, 'index'])->name('admin-hmj-candidate');
        Route::get('admin-hmj-candidate-detail={id}', [HmjCandidateController::class, 'detail'])->name('admin-hmj-candidate-detail');
        Route::get('admin-hmj-candidate-delete={id}', [HmjCandidateController::class, 'index'])->name('admin-hmj-candidate-delete');
        Route::get('admin-hmj-candidate-acc={id}', [HmjCandidateController::class, 'acc'])->name('admin-hmj-candidate-acc');
        //hima
        Route::get('admin-hima-candidate', [HimaCandidateController::class, 'index'])->name('admin-hima-candidate');
        Route::get('admin-hima-candidate-detail={id}', [HimaCandidateController::class, 'detail'])->name('admin-hima-candidate-detail');
        Route::get('admin-hima-candidate-delete={id}', [HimaCandidateController::class, 'index'])->name('admin-hima-candidate-delete');
        Route::get('admin-hima-candidate-acc={id}', [HimaCandidateController::class, 'acc'])->name('admin-hima-candidate-acc');


        // report
        Route::get('data-report', [ReportController::class, 'index'])->name('data-report');
        Route::post('data-report-edit={id}', [ReportController::class, 'edit'])->name('data-report-edit');
        Route::post('data-report-add', [ReportController::class, 'add'])->name('data-report-add');
        Route::get('data-report-delete={id}', [ReportController::class, 'delete'])->name('data-report-delete');


        // setup
        Route::get('setup', [SetupController::class, 'index'])->name('setup');
        Route::post('setup-edit', [SetupController::class, 'edit'])->name('setup-edit');
    });

    Route::middleware(['voter'])->group(function () {
        Route::prefix('dashboard')->group(function () {

            Route::get('voter', [VoterController::class, 'index'])->name('voter');
            Route::get('voter-fresh', [VoterController::class, 'fresh'])->name('voter-fresh');
            // dpm
            Route::get('voter-dpm={id}', [VoterController::class, 'index_dpm'])->name('voter-dpm');
            Route::get('voter-fresh-dpm={id}', [VoterController::class, 'fresh_dpm'])->name('voter-fresh-dpm');
            // hmj
            Route::get('voter-hmj={id}', [VoterController::class, 'index_hmj'])->name('voter-hmj');
            Route::get('voter-fresh-hmj={id}', [VoterController::class, 'fresh_hmj'])->name('voter-fresh-hmj');
            // hima
            Route::get('voter-hima={id}', [VoterController::class, 'index_hima'])->name('voter-hima');
            Route::get('voter-fresh-hima={id}', [VoterController::class, 'fresh_hima'])->name('voter-fresh-hima');
        });

        Route::prefix('pemilihan')->group(function () {
            //pemilihan
            Route::get('pemilihan', [PemilihanController::class, 'index'])->name('pemilihan');

            //bem
            Route::get('bem-calon-voter', [VoterBemController::class, 'index'])->name('bem-calon-voter');
            Route::get('bem-calon-voter-detail={id}', [VoterBemController::class, 'detail'])->name('bem-calon-voter-detail');
            Route::get('bem-calon-voter-vote={id}', [VoterBemController::class, 'vote'])->name('bem-calon-voter-vote');

            //bem
            Route::get('dpm-calon-voter', [VoterDpmController::class, 'index'])->name('dpm-calon-voter');
            Route::get('dpm-calon-voter-detail={id}', [VoterDpmController::class, 'detail'])->name('dpm-calon-voter-detail');
            Route::get('dpm-calon-voter-vote={id}', [VoterDpmController::class, 'vote'])->name('dpm-calon-voter-vote');

            //hmj
            Route::get('hmj-calon-voter', [VoterHmjController::class, 'index'])->name('hmj-calon-voter');
            Route::get('hmj-calon-voter-detail={id}', [VoterHmjController::class, 'detail'])->name('hmj-calon-voter-detail');
            Route::get('hmj-calon-voter-vote={id}', [VoterHmjController::class, 'vote'])->name('hmj-calon-voter-vote');

            //bem
            Route::get('hima-calon-voter', [VoterHimaController::class, 'index'])->name('hima-calon-voter');
            Route::get('hima-calon-voter-detail={id}', [VoterHimaController::class, 'detail'])->name('hima-calon-voter-detail');
            Route::get('hima-calon-voter-vote={id}', [VoterHimaController::class, 'vote'])->name('hima-calon-voter-vote');

            // profile
            Route::get('voter-profile', [VoterProfileController::class, 'index'])->name('voter-profile');
            Route::post('voter-profile-edit', [VoterProfileController::class, 'edit'])->name('voter-profile-edit');
            Route::post('voter-profile-edit2', [VoterProfileController::class, 'edit2'])->name('voter-profile-edit2');
        });
    });

    Route::get('/logout', function () {
        Auth::logout();
        redirect('/');
    });
});
