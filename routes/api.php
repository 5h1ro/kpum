<?php

use App\Http\Controllers\Api\BemController;
use App\Http\Controllers\Api\DpmController;
use App\Http\Controllers\Api\HimaController;
use App\Http\Controllers\Api\HmjController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Http\Request;
use Illuminate\Routing\RouteGroup;
use Illuminate\Support\Facades\Route;
use PHPUnit\Framework\Constraint\Count;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::post('login', [UserController::class, 'login']);

Route::middleware('jwt.verify')->group(function () {
    Route::get('count', [UserController::class, 'count']);
    Route::get('dpm/data={id}', [DpmController::class, 'data']);
    Route::get('hmj/data={id}', [HmjController::class, 'data']);
    Route::get('hima/data={id}', [HimaController::class, 'data']);
    Route::get('bem/vote={id}/{id_user}', [BemController::class, 'vote']);
    Route::get('bem/count', [DpmController::class, 'count']);
    Route::get('dpm/vote={id}/{id_user}', [DpmController::class, 'vote']);
    Route::get('dpm/count', [DpmController::class, 'count']);
    Route::get('hmj/vote={id}/{id_user}', [HmjController::class, 'vote']);
    Route::get('hmj/count', [HmjController::class, 'count']);
    Route::get('hima/vote={id}/{id_user}', [HimaController::class, 'vote']);
    Route::get('hima/count', [HimaController::class, 'count']);
    Route::resource('user', UserController::class);
    Route::resource('bem', BemController::class);
    Route::resource('dpm', DpmController::class);
    Route::resource('hima', HimaController::class);
    Route::resource('hmj', HmjController::class);
});
