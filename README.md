# Project

Web Pemira KPUM PNM 2022

## Fitur

- CRUD Paslon **Done**
- CRUD User **Done**
- Blast Email Username & Password **Done**
- Limitation User **Done**
- CRUD Role **Done**
- Customize Role Permission **Done**
- Register To Be Candidate **Done**
- Register Voter **Done**
- Report **Done**

## Struktur Admin

### Controller

```bash
Code/kpum/app/Http/Controllers/Admin/
├── AdminController.php
├── BemCandidateController.php
├── BemController.php
├── CalonController.php
├── CandidateController.php
├── DpmCandidateController.php
├── DpmController.php
├── HimaCandidateController.php
├── HimaController.php
├── HmjCandidateController.php
├── HmjController.php
├── ProfileController.php
├── ScheduleController.php
└── UserController.php
```

### View

```bash
Code/kpum/resources/views/admin/
├── calon
│   ├── bem
│   │   └── index.blade.php
│   ├── dpm
│   │   └── index.blade.php
│   ├── hima
│   │   └── index.blade.php
│   ├── hmj
│   │   └── index.blade.php
│   └── index.blade.php
├── candidate
│   ├── bem
│   │   ├── detail.blade.php
│   │   └── index.blade.php
│   ├── dpm
│   │   ├── detail.blade.php
│   │   └── index.blade.php
│   ├── hima
│   │   ├── detail.blade.php
│   │   └── index.blade.php
│   ├── hmj
│   │   ├── detail.blade.php
│   │   └── index.blade.php
│   └── index.blade.php
├── dpm
│   ├── home.blade.php
│   └── index.blade.php
├── hima
│   ├── home.blade.php
│   └── index.blade.php
├── hmj
│   ├── home.blade.php
│   └── index.blade.php
├── home.blade.php
├── index.blade.php
├── profile
│   └── index.blade.php
├── schedule
│   └── index.blade.php
└── user
    └── index.blade.php
```

## Struktur Voter

### Controller

```bash
Code/kpum/app/Http/Controllers/Voter/
├── BemController.php
├── DpmController.php
├── HimaController.php
├── HmjController.php
├── PemilihanController.php
├── ProfileController.php
└── VoterController.php
```

### View

```bash
Code/kpum/resources/views/voter/
├── dpm
│   ├── home.blade.php
│   └── index.blade.php
├── hima
│   ├── home.blade.php
│   └── index.blade.php
├── hmj
│   ├── home.blade.php
│   └── index.blade.php
├── home.blade.php
├── index.blade.php
├── pemilihan
│   ├── bem
│   │   ├── detail.blade.php
│   │   └── index.blade.php
│   ├── dpm
│   │   ├── detail.blade.php
│   │   └── index.blade.php
│   ├── hima
│   │   ├── detail.blade.php
│   │   └── index.blade.php
│   ├── hmj
│   │   ├── detail.blade.php
│   │   └── index.blade.php
│   └── index.blade.php
└── profile
    └── index.blade.php
```

## Cara Menggunakan

```bash
# clone repository
$ git clone git@gitlab.com:5h1ro/kpum.git
# install composer
$ composer install
# Migrate Database
$ php artisan migrate --seed
```

## Terimakasih

- Terimakasih kepada tuhan yang maha esa
- Terimakasih kepada kamu yang aku perjuangkan
