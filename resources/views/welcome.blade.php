<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ asset('src') }}/img/favicon.png">
    <title>KPUM - Politeknik Negeri Madiun 2021</title>
    <link rel="stylesheet" href="{{ asset('src') }}/css/plugins.css">
    <link rel="stylesheet" href="{{ asset('src') }}/css/theme/purple.css">
    <link rel="stylesheet" href="{{ asset('src') }}/css/font/thicccboi.css">
</head>

<body>
    <div class="content-wrapper">
        <header class="wrapper bg-soft-primary">
            <nav class="navbar center-nav transparent navbar-expand-lg navbar-light">
                <div class="container flex-lg-row flex-nowrap align-items-center">
                    <div class="navbar-brand w-100"><a href="index.html"><img
                                src="{{ asset('src') }}/img/logo-purple.png"
                                srcset="{{ asset('src') }}/img/logo-purple@2x.png 2x" alt="" /></a>
                    </div>
                    <!-- /.navbar-collapse -->
                    <div class="navbar-other w-100 d-flex ms-auto">
                        <ul class="navbar-nav flex-row align-items-center ms-auto" data-sm-skip="true">
                            @if (Route::has('login'))
                                @auth
                                    <li class="nav-item d-none d-md-block">
                                        <a class="btn btn-primary rounded-pill" href="{{ url('/home') }}">Home</a>
                                    </li>
                                @else
                                    <li class="nav-item d-none d-md-block">
                                        <a class="rounded-pill" href="{{ route('login') }}"><b
                                                class="font-weight-bolder" style="font-size: 17pt">LOGIN</b></a>
                                    </li>
                                @endauth
                            @endif
                            <li class="nav-item d-lg-none">
                                <div class="navbar-hamburger"><button class="hamburger animate plain"
                                        data-toggle="offcanvas-nav"><span></span></button></div>
                            </li>
                        </ul>
                        <!-- /.navbar-nav -->
                    </div>
                    <!-- /.navbar-other -->
                </div>
                <!-- /.container -->
            </nav>
            <!-- /.navbar -->
        </header>
        <!-- /header -->
        <section class="wrapper bg-gradient-primary">
            <div class="container pt-10 pt-md-14">
                <div class="row gx-2 gy-10 align-items-center">
                    <div class="col-md-10 offset-md-1 offset-lg-0 col-lg-5 text-center text-lg-start order-2 order-lg-0"
                        data-cues="slideInDown" data-group="page-title" data-delay="600">
                        <h1 class="display-1 mb-5 mx-md-10 mx-lg-0">Gunakan Suaramu, Untuk Masa Depan Yang Lebih Baik
                            <br /><span class="typer text-primary text-nowrap" data-delay="100" data-delim=":"
                                data-words="Jangan Golput"></span><span class="cursor text-primary"
                                data-owner="typer"></span>
                        </h1>
                        @if (Route::has('login'))
                            @auth
                                <div class="d-flex justify-content-center justify-content-lg-start mb-4"
                                    data-cues="slideInDown" data-group="page-title-buttons" data-delay="900">
                                    <span><a class="btn btn-lg btn-primary rounded-pill me-2 scroll"
                                            href="{{ url('/home') }}">Home</a></span>
                                </div>
                            @else
                                <div class="d-flex justify-content-center justify-content-lg-start mb-4"
                                    data-cues="slideInDown" data-group="page-title-buttons" data-delay="900">
                                    <span><a class="btn btn-lg btn-primary rounded-pill me-2 scroll"
                                            href="{{ route('login') }}">Login</a></span>
                                </div>
                            @endauth
                        @endif
                        <a href="{{ route('register-candidate') }}" data-cues="slideInLeft"
                            data-group="page-title-buttons" data-delay="1000">
                            <u>Daftarkan dirimu menjadi pemimpin berikutnya -></u></a>
                    </div>
                    <!-- /column -->
                    <div class="col-lg-6 ms-auto position-relative">
                        <div class="row g-4">
                            <div class="col-12" data-cues="fadeIn" data-group="col-middle">
                                <div><img class="w-100 img-fluid rounded shadow-lg mt-4"
                                        src="{{ asset('src') }}/img/demos/vc5@2x.jpg" alt="" /></div>
                            </div>
                            <!-- /column -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /column -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </section>
        <!-- /section -->
        <section class="wrapper bg-gradient-reverse-primary" id="demos">
            <div class="container py-14 py-md-18">
                <div class="row mb-10">
                    <div class="col-md-9 col-lg-7 col-xl-6 col-xxl-5 mx-auto">
                        <div class="counter-wrapper">
                            <h3 class="fs-70 mb-3 text-primary text-center counter">0</h3>
                        </div>
                        <h2 class="display-3 mb-3 text-center mb-0">Mahasiswa Telah Memilih
                        </h2>
                    </div>
                    <!-- /column -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-md-10 col-lg-12 col-xl-10 col-xxl-9 mx-auto">
                        <div class="grid grid-view projects-tiles">
                            <div class="project mt-10 mt-md-15 mt-lg-20">
                                <div class="row gy-md-8 g-lg-8 g-xl-11 isotope" data-cues="fadeIn">
                                    <div class="item col-lg-6">
                                        <figure class="itooltip itooltip-blue rounded shadow-lg"
                                            title='<h5 class="mb-0">Tahap 1, Masukkan email dan password</h5>'>
                                            <img src="{{ asset('src') }}/img/demos/tahap-1.jpg"
                                                srcset="{{ asset('src') }}/img/demos/tahap-1@2x.jpg 2x" alt="" />
                                        </figure>
                                    </div>
                                    <!-- /.item -->
                                    <div class="item col-lg-6 d-none d-md-block">
                                        <div class="project-details p-0 pt-lg-10">
                                            <div class="post-header">
                                                <h2 class="post-title fs-30 mb-4">Tahapan Pemilihan</h2>
                                            </div>
                                            <!-- /.post-content -->
                                        </div>
                                        <!-- /.project-details -->
                                    </div>
                                    <!-- /.item -->
                                    <div class="item col-lg-6 mt-5">
                                        <figure class="itooltip itooltip-blue rounded shadow-lg"
                                            title='<h5 class="mb-0">Tahap 2, masuk ke menu pemilihan</h5>'>
                                            <img src="{{ asset('src') }}/img/demos/tahap-2.jpg"
                                                srcset="{{ asset('src') }}/img/demos/tahap-2@2x.jpg 2x" alt="" />
                                        </figure>
                                    </div>
                                    <!-- /.item -->
                                    <div class="item col-lg-6 mt-5">
                                        <figure class="itooltip itooltip-blue rounded shadow-lg"
                                            title='<h5 class="mb-0">Tahap 3, pilih ormawa yang akan dipilih</h5>'>
                                            <img src="{{ asset('src') }}/img/demos/tahap-3.jpg"
                                                srcset="{{ asset('src') }}/img/demos/tahap-3@2x.jpg 2x" alt="" />
                                        </figure>
                                    </div>
                                    <!-- /.item -->
                                    <div class="item col-lg-6 mt-5">
                                        <figure class="itooltip itooltip-blue rounded shadow-lg"
                                            title='<h5 class="mb-0">Tahap 4, klik detail untuk melihat calon</h5>'>
                                            <img src="{{ asset('src') }}/img/demos/tahap-4.jpg"
                                                srcset="{{ asset('src') }}/img/demos/tahap-4@2x.jpg 2x" alt="" />
                                        </figure>
                                    </div>
                                    <!-- /.item -->
                                    <div class="item col-lg-6 mt-5">
                                        <figure class="itooltip itooltip-blue rounded shadow-lg"
                                            title='<h5 class="mb-0">Tahap 5, klik pilih untuk memilih calon</h5>'>
                                            <img src="{{ asset('src') }}/img/demos/tahap-5.jpg"
                                                srcset="{{ asset('src') }}/img/demos/tahap-5@2x.jpg 2x" alt="" />
                                        </figure>
                                    </div>
                                    <!-- /.item -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.project -->
                        </div>
                        <!-- /.projects-tiles -->
                    </div>
                    <!-- /column -->
                </div>
                <!-- /.row -->
                <div class="mb-12"></div>
            </div>
            <!-- /.container -->
        </section>
        <!-- /section -->
    </div>
    <!-- /.content-wrapper -->
    <div class="progress-wrap">
        <svg class="progress-circle svg-content" width="100%" height="100%" viewBox="-1 -1 102 102">
            <path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98" />
        </svg>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
        integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
        crossorigin="anonymous"></script>
    <script src="{{ asset('src') }}/js/plugins.js"></script>
    <script src="{{ asset('src') }}/js/scripts.js"></script>
</body>

</html>
