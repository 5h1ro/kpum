@extends('layouts.simple.master')
@section('title', 'Product')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendors/select2.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendors/owlcarousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendors/range-slider.css') }}">
@endsection

@section('style')
@endsection

@section('breadcrumb-title')
    <h3>DPM</h3>
@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item">Pemilihan</li>
    <li class="breadcrumb-item active">HMJ</li>
@endsection

@section('content')
    <div class="container-fluid product-wrapper">
        <div class="product-grid">
            <div class="product-wrapper-grid">
                <div class="row justify-content-center">
                    @foreach ($hmj as $data)
                        <div class="col-xl-3 col-sm-6 xl-4">
                            <div class="card">
                                <div class="product-box">
                                    <div class="product-img">
                                        <img class="img-fluid" src="{{ $data->img2 }}" alt="">
                                        <div class="product-hover">
                                            <ul>
                                                <li>
                                                    <a href="{{ route('hmj-calon-voter-detail', ['id' => Crypt::encrypt($data->id)]) }}"
                                                        class="btn" type="button"><i
                                                            class="icon-eye"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="product-details">
                                        <div class="row justify-content-between">
                                            <h4 class="col-6" style="font-size: 13pt">{{ $data->nickname }}</h4>
                                        </div>
                                        <div class="row justify-content-between">
                                            <p class="col-6">{{ $data->prodi->name }}</p>
                                        </div>
                                    </div>
                                    <div>
                                        <a href="{{ route('hmj-calon-voter-detail', ['id' => Crypt::encrypt($data->id)]) }}"
                                            class="btn btn-primary m-r-10 col-12" type="button" title="">
                                            <i class="fa fa-info me-1"></i>Detail</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/js/range-slider/ion.rangeSlider.min.js') }}"></script>
    <script src="{{ asset('assets/js/range-slider/rangeslider-script.js') }}"></script>
    <script src="{{ asset('assets/js/touchspin/vendors.min.js') }}"></script>
    <script src="{{ asset('assets/js/touchspin/touchspin.js') }}"></script>
    <script src="{{ asset('assets/js/touchspin/input-groups.min.js') }}"></script>
    <script src="{{ asset('assets/js/owlcarousel/owl.carousel.js') }}"></script>
    <script src="{{ asset('assets/js/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/select2/select2-custom.js') }}"></script>
    <script src="{{ asset('assets/js/product-tab.js') }}"></script>
@endsection
