@extends('layouts.simple.master')
@section('title', 'Product Page')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendors/owlcarousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendors/rating.css') }}">
@endsection

@section('style')
@endsection

@section('breadcrumb-title')
    <h3>Detail No Urut {{ $hima->id }}</h3>
@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item">Pemilihan</li>
    <li class="breadcrumb-item">Hima</li>
    <li class="breadcrumb-item active">Detail No Urut {{ $hima->id }}</li>
@endsection

@section('content')
    <div class="container-fluid">
        <div>
            <div class="row product-page-main p-0">
                <div class="col-xl-4 xl-cs-65 box-col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="product-slider owl-carousel owl-theme" id="sync1">
                                <div class="item"><img src="{{ $hima->img2 }}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 xl-100 box-col-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="product-page-details">
                                <h4>Visi</h4>
                            </div>
                            <p>{{ $hima->visi }}</p>
                            <hr>
                            <div class="product-page-details">
                                <h4>Misi</h4>
                            </div>
                            @foreach ($hima->misi as $a)
                                <li class="ms-3 mb-2">
                                    {{ $a->detail }}
                                </li>
                            @endforeach
                            <hr>
                            @if ($user->voter->hima == 0)
                                <div class="m-t-15 d-flex justify-content-end">
                                    <a href="{{ route('hima-calon-voter-vote', ['id' => Crypt::encrypt($hima->id)]) }}"
                                        class="btn btn-success m-r-10" type="button" title=""> <i
                                            class="fa fa-check me-1"></i>Pilih</a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 xl-cs-35 box-col-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="filter-block">
                                <h4>Calon Ketua Himpunan Mahasiswa {{ $hima->prodi->name }}</h4>
                                <div class="row">
                                    <div class="col-3 col-sm-4 col-lg-3 col-xxl-3">
                                        <p>Nama</p>
                                    </div>
                                    <div class="col-1 col-sm-1 col-lg-1 col-xxl-1">
                                        <p>:</p>
                                    </div>
                                    <div class="col-8 col-sm-7 col-lg-12 col-xxl-8">
                                        <p>{{ $hima->name }}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-3 col-sm-4 col-lg-3 col-xxl-3">
                                        <p>Prodi</p>
                                    </div>
                                    <div class="col-1 col-sm-1 col-lg-1 col-xxl-1">
                                        <p>:</p>
                                    </div>
                                    <div class="col-8 col-sm-7 col-lg-12 col-xxl-8">
                                        <p>{{ $hima->prodi->name }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/js/sidebar-menu.js') }}"></script>
    <script src="{{ asset('assets/js/rating/jquery.barrating.js') }}"></script>
    <script src="{{ asset('assets/js/rating/rating-script.js') }}"></script>
    <script src="{{ asset('assets/js/owlcarousel/owl.carousel.js') }}"></script>
    <script src="{{ asset('assets/js/ecommerce.js') }}"></script>
@endsection
