@extends('layouts.simple.master')
@section('title', 'User Cards')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendors/sweetalert2.css') }}">
@endsection

@section('style')
@endsection

@section('breadcrumb-title')
    <h3>Pemilihan</h3>
@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item active">Pemilihan</li>
@endsection

@section('content')
    <div class="container-fluid">
        @if ($schedule->where([['id_prodi', '=', $user->voter->id_prodi], ['category', '=', 'BEM']])->first() == null && $schedule->where([['id_prodi', '=', $user->voter->id_prodi], ['category', '=', 'DPM']])->first() == null)
            <div class="row justify-content-center">
                @if ($schedule->where([['id_prodi', '=', $user->voter->id_prodi], ['category', '=', 'HMJ']])->first() == null && $schedule->where([['id_prodi', '=', $user->voter->id_prodi], ['category', '=', 'HIMA']])->first() == null)
                    <center>
                        <div class="row align-items-center" style="height: 80vh">
                            <h1>Silahkan memilih sesuai jadwal yang telah diberikan</h1>
                        </div>
                    </center>
                @else
                    @if ($schedule->where([['id_prodi', '=', $user->voter->id_prodi], ['category', '=', 'HMJ']])->first() != null)
                        @if (Carbon\Carbon::now()->format('YmdHi') >= preg_replace('/[^0-9]/', '', $schedule::where([['id_prodi', '=', $user->voter->id_prodi], ['category', '=', 'HMJ']])->first()->start) && Carbon\Carbon::now()->format('YmdHi') <= preg_replace('/[^0-9]/', '', $schedule::where([['id_prodi', '=', $user->voter->id_prodi], ['category', '=', 'HMJ']])->first()->end))
                            <div class="col-md-6 col-lg-6 col-xl-4 box-col-6">
                                <a href="{{ route('hmj-calon-voter') }}">
                                    <div class="card custom-card">
                                        <div class="card-header mt-5"><img class="img-fluid"
                                                src="{{ asset('assets/images/user-card/1.jpg') }}" alt=""></div>
                                        <div class="card-profile"><img class="rounded-circle"
                                                src="{{ asset('assets/images/avtar/hmj.jpg') }}" alt=""></div>
                                        <div class="text-center profile-details">
                                            <h4>HMJ</h4>
                                            <h6>Himpunan Mahasiswa Jurusan</h6>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @else
                            <center>
                                <div class="row align-items-center" style="height: 80vh">
                                    <h1>Silahkan memilih sesuai jadwal yang telah diberikan</h1>
                                </div>
                            </center>
                        @endif
                    @endif

                    @if ($schedule->where([['id_prodi', '=', $user->voter->id_prodi], ['category', '=', 'HIMA']])->first() != null)
                        @if (Carbon\Carbon::now()->format('YmdHi') >= preg_replace('/[^0-9]/', '', $schedule::where([['id_prodi', '=', $user->voter->id_prodi], ['category', '=', 'HIMA']])->first()->start) && Carbon\Carbon::now()->format('YmdHi') <= preg_replace('/[^0-9]/', '', $schedule::where([['id_prodi', '=', $user->voter->id_prodi], ['category', '=', 'HIMA']])->first()->end))
                            <div class="col-md-6 col-lg-6 col-xl-4 box-col-6">
                                <a href="{{ route('hima-calon-voter') }}">
                                    <div class="card custom-card">
                                        <div class="card-header mt-5"><img class="img-fluid"
                                                src="{{ asset('assets/images/user-card/2.jpg') }}" alt=""></div>
                                        <div class="card-profile"><img class="rounded-circle"
                                                src="{{ asset('assets/images/avtar/hima.jpg') }}" alt=""></div>
                                        <div class="text-center profile-details">
                                            <h4>HIMAPRODI</h4>
                                            <h6>Himpunan Mahasiswa Prodi</h6>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @else
                            <center>
                                <div class="row align-items-center" style="height: 80vh">
                                    <h1>Silahkan memilih sesuai jadwal yang telah diberikan</h1>
                                </div>
                            </center>
                        @endif
                    @endif
                @endif
            </div>
        @else
            @if ($schedule->where([['id_prodi', '=', $user->voter->id_prodi], ['category', '=', 'BEM']])->first() != null && $schedule->where([['id_prodi', '=', $user->voter->id_prodi], ['category', '=', 'DPM']])->first() == null)
                @if (Carbon\Carbon::now()->format('YmdHi') >= preg_replace('/[^0-9]/', '', $schedule::where([['id_prodi', '=', $user->voter->id_prodi], ['category', '=', 'BEM']])->first()->start) && Carbon\Carbon::now()->format('YmdHi') <= preg_replace('/[^0-9]/', '', $schedule::where([['id_prodi', '=', $user->voter->id_prodi], ['category', '=', 'BEM']])->first()->end))
                    <div class="row justify-content-center">
                        <div class="col-md-6 col-lg-6 col-xl-4 box-col-6">
                            <a href="{{ route('bem-calon-voter') }}">
                                <div class="card custom-card">
                                    <div class="card-header mt-5"><img class="img-fluid"
                                            src="{{ asset('assets/images/user-card/1.jpg') }}" alt=""></div>
                                    <div class="card-profile"><img class="rounded-circle"
                                            src="{{ asset('assets/images/avtar/bem.jpg') }}" alt=""></div>
                                    <div class="text-center profile-details">
                                        <h4>BEM</h4>
                                        <h6>Badan Eksekutif Mahasiswa</h6>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                @else
                    <center>
                        <div class="row align-items-center" style="height: 80vh">
                            <h1>Silahkan memilih sesuai jadwal yang telah diberikan</h1>
                        </div>
                    </center>
                @endif
            @else
                @if ($schedule->where([['id_prodi', '=', $user->voter->id_prodi], ['category', '=', 'BEM']])->first() == null && $schedule->where([['id_prodi', '=', $user->voter->id_prodi], ['category', '=', 'DPM']])->first() != null)
                    @if (Carbon\Carbon::now()->format('YmdHi') >= preg_replace('/[^0-9]/', '', $schedule::where([['id_prodi', '=', $user->voter->id_prodi], ['category', '=', 'DPM']])->first()->start) && Carbon\Carbon::now()->format('YmdHi') <= preg_replace('/[^0-9]/', '', $schedule::where([['id_prodi', '=', $user->voter->id_prodi], ['category', '=', 'DPM']])->first()->end))
                        <div class="col-md-6 col-lg-6 col-xl-4 box-col-6">
                            <a href="{{ route('dpm-calon-voter') }}">
                                <div class="card custom-card">
                                    <div class="card-header mt-5"><img class="img-fluid"
                                            src="{{ asset('assets/images/user-card/2.jpg') }}" alt=""></div>
                                    <div class="card-profile"><img class="rounded-circle"
                                            src="{{ asset('assets/images/avtar/dpm.jpg') }}" alt=""></div>
                                    <div class="text-center profile-details">
                                        <h4>DPM</h4>
                                        <h6>Dewan Perwakilan Mahasiswa</h6>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @else
                        <center>
                            <div class="row align-items-center" style="height: 80vh">
                                <h1>Silahkan memilih sesuai jadwal yang telah diberikan</h1>
                            </div>
                        </center>
                    @endif
                @else
                    @if (Carbon\Carbon::now()->format('YmdHi') >= preg_replace('/[^0-9]/', '', $schedule::where([['id_prodi', '=', $user->voter->id_prodi], ['category', '=', 'BEM']])->first()->start) && Carbon\Carbon::now()->format('YmdHi') <= preg_replace('/[^0-9]/', '', $schedule::where([['id_prodi', '=', $user->voter->id_prodi], ['category', '=', 'BEM']])->first()->end))
                        <div class="row justify-content-center">
                            <div class="col-md-6 col-lg-6 col-xl-4 box-col-6">
                                <a href="{{ route('bem-calon-voter') }}">
                                    <div class="card custom-card">
                                        <div class="card-header mt-5"><img class="img-fluid"
                                                src="{{ asset('assets/images/user-card/1.jpg') }}" alt=""></div>
                                        <div class="card-profile"><img class="rounded-circle"
                                                src="{{ asset('assets/images/avtar/bem.jpg') }}" alt=""></div>
                                        <div class="text-center profile-details">
                                            <h4>BEM</h4>
                                            <h6>Badan Eksekutif Mahasiswa</h6>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            @if (Carbon\Carbon::now()->format('YmdHi') >= preg_replace('/[^0-9]/', '', $schedule::where([['id_prodi', '=', $user->voter->id_prodi], ['category', '=', 'DPM']])->first()->start) && Carbon\Carbon::now()->format('YmdHi') <= preg_replace('/[^0-9]/', '', $schedule::where([['id_prodi', '=', $user->voter->id_prodi], ['category', '=', 'DPM']])->first()->end))
                                <div class="col-md-6 col-lg-6 col-xl-4 box-col-6">
                                    <a href="{{ route('dpm-calon-voter') }}">
                                        <div class="card custom-card">
                                            <div class="card-header mt-5"><img class="img-fluid"
                                                    src="{{ asset('assets/images/user-card/2.jpg') }}" alt=""></div>
                                            <div class="card-profile"><img class="rounded-circle"
                                                    src="{{ asset('assets/images/avtar/dpm.jpg') }}" alt=""></div>
                                            <div class="text-center profile-details">
                                                <h4>DPM</h4>
                                                <h6>Dewan Perwakilan Mahasiswa</h6>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endif
                        </div>
                    @else
                        <center>
                            <div class="row align-items-center" style="height: 80vh">
                                <h1>Silahkan memilih sesuai jadwal yang telah diberikan</h1>
                            </div>
                        </center>
                    @endif
                @endif
            @endif
        @endif
    </div>
@endsection

@section('script')

    <script src="{{ asset('assets/js/sweet-alert/sweetalert.min.js') }}"></script>
    @if (Session::has('message'))
        <script src="{{ asset('assets/js/sweet-alert/vote_success.js') }}"></script>
    @endif
    @if ($errors->any())
        <script src="{{ asset('assets/js/sweet-alert/user.js') }}"></script>
    @endif
@endsection
