@extends('layouts.simple.master')
@section('title', 'User Cards')

@section('css')
@endsection

@section('style')
@endsection

@section('breadcrumb-title')
    <h3>Profil</h3>
@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item active">Profil</li>
@endsection

@section('content')

    <div class="container-fluid">
        <div class="edit-profile">
            <div class="row">
                <div class="col-xl-4">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title mb-0">Profilku</h4>
                            <div class="card-options"><a class="card-options-collapse" href="#"
                                    data-bs-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a
                                    class="card-options-remove" href="#" data-bs-toggle="card-remove"><i
                                        class="fe fe-x"></i></a></div>
                        </div>
                        <div class="card-body">
                            <form method="POST" action="{{ route('voter-profile-edit') }}">
                                @csrf
                                <div class="row mb-2">
                                    <div class="profile-title">
                                        <div class="media">
                                            <img class="img-70 rounded-circle" alt="" src="{{ $user->voter->image }}">
                                            <div class="media-body">
                                                <h4 class="mb-1">{{ $user->voter->name }}</h4>
                                                <p>{{ $user->npm }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Email</label>
                                    <input class="form-control" type="text" name="email" id="email"
                                        value="{{ $user->email }}">
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Password</label>
                                    <input class="form-control" type="password" name="password" id="password">
                                </div>
                                <div class="form-footer">
                                    <button class="btn btn-primary btn-block" type="submit">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-xl-8">
                    <form class="card" method="POST" action="{{ route('voter-profile-edit2') }}"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="card-header">
                            <h4 class="card-title mb-0">Detail Profilku</h4>
                            <div class="card-options"><a class="card-options-collapse" href="#"
                                    data-bs-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a
                                    class="card-options-remove" href="#" data-bs-toggle="card-remove"><i
                                        class="fe fe-x"></i></a></div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="mb-3">
                                        <label class="form-label">Nama Lengkap</label>
                                        <input class="form-control" type="text" name="fullname" id="fullname"
                                            value="{{ $user->voter->fullname }}">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="mb-3">
                                        <label class="form-label">Nama</label>
                                        <input class="form-control" type="text" name="name" id="name"
                                            value="{{ $user->voter->name }}">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="mb-3">
                                        <label class="form-label">Nomor</label>
                                        <input class="form-control" type="number" name="number" id="number"
                                            value="{{ $user->voter->number }}">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="mb-3">
                                        <label class="form-label">Tempat Lahir</label>
                                        <input class="form-control" type="text" name="city" id="city"
                                            value="{{ $user->voter->city }}">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="mb-3">
                                        <label class="form-label">Tanggal Lahir</label>
                                        <input class="form-control" type="date" name="birthday" id="birthday"
                                            value="{{ $user->voter->birthday }}">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="mb-3">
                                        <label class="form-label">Jenis Kelamin</label>
                                        <select class="form-control btn-square" id="gender" name="gender">
                                            @if ($user->voter->gender == 'perempuan')
                                                <option value="perempuan" selected>Perempuan</option>
                                                <option value="laki-laki">Laki-Laki</option>
                                            @else
                                                <option value="perempuan">Perempuan</option>
                                                <option value="laki-laki" selected>Laki-Laki</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-8">
                                    <div class="mb-3">
                                        <label class="form-label">Agama</label>
                                        <select class="form-control btn-square" id="religion" name="religion">
                                            @if ($user->voter->religion == 'kristen')
                                                <option value="islam">Islam</option>
                                                <option value="kristen" selected>Kristen</option>
                                                <option value="katholik">Katholik</option>
                                                <option value="hindu">Hindu</option>
                                                <option value="budha">Budha</option>
                                                <option value="konghucu">Konghucu</option>
                                            @elseif ($user->voter->religion == 'katholik')
                                                <option value="islam">Islam</option>
                                                <option value="kristen">Kristen</option>
                                                <option value="katholik" selected>Katholik</option>
                                                <option value="hindu">Hindu</option>
                                                <option value="budha">Budha</option>
                                                <option value="konghucu">Konghucu</option>
                                            @elseif ($user->voter->religion == 'hindu')
                                                <option value="islam">Islam</option>
                                                <option value="kristen">Kristen</option>
                                                <option value="katholik">Katholik</option>
                                                <option value="hindu" selected>Hindu</option>
                                                <option value="budha">Budha</option>
                                                <option value="konghucu">Konghucu</option>
                                            @elseif ($user->voter->religion == 'budha')
                                                <option value="islam">Islam</option>
                                                <option value="kristen">Kristen</option>
                                                <option value="katholik">Katholik</option>
                                                <option value="hindu">Hindu</option>
                                                <option value="budha" selected>Budha</option>
                                                <option value="konghucu">Konghucu</option>
                                            @elseif ($user->voter->religion == 'konghucu')
                                                <option value="islam">Islam</option>
                                                <option value="kristen">Kristen</option>
                                                <option value="katholik">Katholik</option>
                                                <option value="hindu">Hindu</option>
                                                <option value="budha">Budha</option>
                                                <option value="konghucu" selected>Konghucu</option>
                                            @else
                                                <option value="islam" selected>Islam</option>
                                                <option value="kristen">Kristen</option>
                                                <option value="katholik">Katholik</option>
                                                <option value="hindu">Hindu</option>
                                                <option value="budha">Budha</option>
                                                <option value="konghucu">Konghucu</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="mb-3">
                                        <label class="form-label">Foto</label>
                                        <input class="form-control" id="image" name="image" type="file">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div>
                                        <label class="form-label">Alamat</label>
                                        <textarea class="form-control" rows="5" id="address"
                                            name="address">{{ $user->voter->address }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-end">
                            <button class="btn btn-primary" type="submit">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
@endsection
