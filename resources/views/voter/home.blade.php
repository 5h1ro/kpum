<div class="row justify-content-center">
    @for ($i = 0; $i < count($bem); $i++)
        <div class="col-xl-3 box-col-6">
            <div class="card custom-card">
                <div class="card-header mt-5"><img class="img-fluid" src="{{ $bem[$i]->img }}" alt=""></div>
                <div class="card-profile"><img class="rounded-circle"
                        src="{{ asset('assets/images/avtar') }}/{{ $bem[$i]->id }}.jpg" alt=""></div>
                <div class="text-center profile-details mb-3">
                    <h4>{{ $bem[$i]->nickname1 }} - {{ $bem[$i]->nickname2 }}</h4>
                </div>
                <div class="card-footer row">
                    <div class="col-6 col-sm-6">
                        <h6>Perolehan Suara</h6>
                        <h3><span class="counter">{{ $persentase[$i] }}</span>%</h3>
                    </div>
                    <div class="col-6 col-sm-6">
                        <h6>Total Suara</h6>
                        <h3><span class="counter">{{ $bem[$i]->vote }}</span> Suara</h3>
                    </div>
                </div>
            </div>
        </div>
    @endfor
</div>
<div class="col-md-12 d-none d-sm-none d-md-block">
    <div class="card browser-widget">
        <div class="media card-body">
            <div class="media-body align-self-center justify-content-center row">
                <div class="col-4">
                    <p>Jumlah Pemilih </p>
                    <div class="row justify-content-center">
                        <div class="col-4 text-end">
                            <h4><span class="counter">{{ $pemilihpersen }}</span>%
                            </h4>
                        </div>
                        <div class="col-2 text-center">
                            <h4>|</h4>
                        </div>
                        <div class="col-4 text-start">
                            <h4><span class="counter">{{ $pemilih }}</span>
                            </h4>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <p>Suara Sah </p>
                    <div class="row justify-content-center">
                        <div class="col-4 text-end">
                            <h4><span class="counter">{{ $suarasahpersen }}</span>%
                            </h4>
                        </div>
                        <div class="col-2 text-center">
                            <h4>|</h4>
                        </div>
                        <div class="col-4 text-start">
                            <h4><span class="counter">{{ $suarasah }}</span>
                            </h4>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <p>Suara Tidak Sah (Golput) </p>
                    <div class="row justify-content-center">
                        <div class="col-4 text-end">
                            <h4><span class="counter">{{ $suaranonsahpersen }}</span>%
                            </h4>
                        </div>
                        <div class="col-2 text-center">
                            <h4>|</h4>
                        </div>
                        <div class="col-4 text-start">
                            <h4><span class="counter">{{ $suaranonsah }}</span>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12  d-md-block">
    <div class="card">
        <div class="card-header">
            <h4>
                Dewan Perwakilan Mahasiswa
            </h4>
        </div>
        <div class="card-body">
            <div class="justify-content-center">
                <div class="row justify-content-center my-0">
                    <a href="{{ route('voter-dpm', 1) }}"
                        class="col-9 col-md-3 btn btn-outline-primary-2x my-1 my-md-3 mx-1 mx-md-3">Teknologi
                        Informasi</a>
                    <a href="{{ route('voter-dpm', 2) }}"
                        class="col-9 col-md-3 btn btn-outline-primary-2x my-1 my-md-3 mx-1 mx-md-3">Teknik
                        Komputer Kontrol</a>
                    <a href="{{ route('voter-dpm', 3) }}"
                        class="col-9 col-md-3 btn btn-outline-primary-2x my-1 my-md-3 mx-1 mx-md-3">Teknik
                        Listrik</a>
                </div>
                <div class="row justify-content-center my-0">
                    <a href="{{ route('voter-dpm', 4) }}"
                        class="col-9 col-md-3 btn btn-outline-primary-2x my-1 my-md-3 mx-1 mx-md-3">Mesin
                        Otomotif</a>
                    <a href="{{ route('voter-dpm', 5) }}"
                        class="col-9 col-md-3 btn btn-outline-primary-2x my-1 my-md-3 mx-1 mx-md-3">Teknik
                        Perkeretaapian</a>
                    <a href="{{ route('voter-dpm', 8) }}"
                        class="col-9 col-md-3 btn btn-outline-primary-2x my-1 my-md-3 mx-1 mx-md-3">Bahasa
                        Inggris</a>
                </div>
                <div class="row justify-content-center my-0">
                    <a href="{{ route('voter-dpm', 7) }}"
                        class="col-9 col-md-3 btn btn-outline-primary-2x my-1 my-md-3 mx-1 mx-md-3">Administrasi
                        Bisnis</a>
                    <a href="{{ route('voter-dpm', 9) }}"
                        class="col-9 col-md-3 btn btn-outline-primary-2x my-1 my-md-3 mx-1 mx-md-3">Komputerisasi
                        Akuntansi</a>
                    <a href="{{ route('voter-dpm', 10) }}"
                        class="col-9 col-md-3 btn btn-outline-primary-2x my-1 my-md-3 mx-1 mx-md-3">Akuntansi</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12  d-md-block">
    <div class="card">
        <div class="card-header">
            <h4>
                Himpunan Mahasiswa Jurusan
            </h4>
        </div>
        <div class="card-body">
            <div class="justify-content-center">
                <div class="row justify-content-center my-0">
                    <a href="{{ route('voter-hmj', 1) }}"
                        class="col-9 col-md-3 btn btn-outline-primary-2x my-1 my-md-3 mx-1 mx-md-3">Teknik</a>
                    <a href="{{ route('voter-hmj', 2) }}"
                        class="col-9 col-md-3 btn btn-outline-primary-2x my-1 my-md-3 mx-1 mx-md-3">Administrasi
                        Bisnis</a>
                    <a href="{{ route('voter-hmj', 3) }}"
                        class="col-9 col-md-3 btn btn-outline-primary-2x my-1 my-md-3 mx-1 mx-md-3">Komputer
                        Akuntansi</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12  d-md-block">
    <div class="card">
        <div class="card-header">
            <h4>
                Himpunan Mahasiswa Program Studi
            </h4>
        </div>
        <div class="card-body">
            <div class="justify-content-center">
                <div class="row justify-content-center my-0">
                    <a href="{{ route('voter-hima', 1) }}"
                        class="col-9 col-md-3 btn btn-outline-primary-2x my-1 my-md-3 mx-1 mx-md-3">Teknologi
                        Informasi</a>
                    <a href="{{ route('voter-hima', 2) }}"
                        class="col-9 col-md-3 btn btn-outline-primary-2x my-1 my-md-3 mx-1 mx-md-3">Teknik
                        Komputer Kontrol</a>
                    <a href="{{ route('voter-hima', 3) }}"
                        class="col-9 col-md-3 btn btn-outline-primary-2x my-1 my-md-3 mx-1 mx-md-3">Teknik
                        Listrik</a>
                </div>
                <div class="row justify-content-center my-0">
                    <a href="{{ route('voter-hima', 4) }}"
                        class="col-9 col-md-3 btn btn-outline-primary-2x my-1 my-md-3 mx-1 mx-md-3">Mesin
                        Otomotif</a>
                    <a href="{{ route('voter-hima', 5) }}"
                        class="col-9 col-md-3 btn btn-outline-primary-2x my-1 my-md-3 mx-1 mx-md-3">Teknik
                        Perkeretaapian</a>
                    <a href="{{ route('voter-hima', 8) }}"
                        class="col-9 col-md-3 btn btn-outline-primary-2x my-1 my-md-3 mx-1 mx-md-3">Bahasa
                        Inggris</a>
                </div>
                <div class="row justify-content-center my-0">
                    <a href="{{ route('voter-hima', 7) }}"
                        class="col-9 col-md-3 btn btn-outline-primary-2x my-1 my-md-3 mx-1 mx-md-3">Administrasi
                        Bisnis</a>
                    <a href="{{ route('voter-hima', 9) }}"
                        class="col-9 col-md-3 btn btn-outline-primary-2x my-1 my-md-3 mx-1 mx-md-3">Komputerisasi
                        Akuntansi</a>
                    <a href="{{ route('voter-hima', 10) }}"
                        class="col-9 col-md-3 btn btn-outline-primary-2x my-1 my-md-3 mx-1 mx-md-3">Akuntansi</a>
                </div>
                {{-- <div class="row justify-content-center my-0">
                    <a href="{{ route('voter-himatpl') }}"
                        class="col-9 col-md-3 btn btn-outline-primary-2x my-1 my-md-3 mx-1 mx-md-3">Teknik
                        Pembentukan Logam</a>
                </div> --}}
            </div>
        </div>
    </div>
</div>
