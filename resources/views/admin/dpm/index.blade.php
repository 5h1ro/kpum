<div class="row justify-content-center">
    @for ($i = 0; $i < count($dpm); $i++)
        <div class="col-xl-3 box-col-6">
            <div class="card custom-card">
                <div class="card-header mt-5"><img class="img-fluid" src="{{ $dpm[$i]->img }}" alt=""></div>
                <div class="card-profile"><img class="rounded-circle"
                        src="{{ asset('assets/images/avtar') }}/{{ $dpm[$i]->id }}.jpg" alt=""></div>
                <div class="text-center profile-details mb-3">
                    <h4>{{ $dpm[$i]->nickname }}</h4>
                </div>
                <div class="card-footer row">
                    <div class="col-6 col-sm-6">
                        <h6>Perolehan Suara</h6>
                        <h3><span class="counter">{{ $persentase[$i] }}</span>%</h3>
                    </div>
                    <div class="col-6 col-sm-6">
                        <h6>Total Suara</h6>
                        <h3><span class="counter">{{ $dpm[$i]->vote }}</span> </h3>
                    </div>
                </div>
            </div>
        </div>
    @endfor
</div>
<div class="col-md-12 d-none d-sm-none d-md-block">
    <div class="card browser-widget">
        <div class="media card-body">
            <div class="media-body align-self-center justify-content-center row">
                <div class="col-4">
                    <p>Jumlah Pemilih </p>
                    <div class="row justify-content-center">
                        <div class="col-4 text-end">
                            <h4><span class="counter">{{ $pemilihpersen }}</span>%
                            </h4>
                        </div>
                        <div class="col-2 text-center">
                            <h4>|</h4>
                        </div>
                        <div class="col-4 text-start">
                            <h4><span class="counter">{{ $pemilih }}</span>
                            </h4>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <p>Suara Sah </p>
                    <div class="row justify-content-center">
                        <div class="col-4 text-end">
                            <h4><span class="counter">{{ $suarasahpersen }}</span>%
                            </h4>
                        </div>
                        <div class="col-2 text-center">
                            <h4>|</h4>
                        </div>
                        <div class="col-4 text-start">
                            <h4><span class="counter">{{ $suarasah }}</span>
                            </h4>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <p>Suara Tidak Sah (Golput) </p>
                    <div class="row justify-content-center">
                        <div class="col-4 text-end">
                            <h4><span class="counter">{{ $suaranonsahpersen }}</span>%
                            </h4>
                        </div>
                        <div class="col-2 text-center">
                            <h4>|</h4>
                        </div>
                        <div class="col-4 text-start">
                            <h4><span class="counter">{{ $suaranonsah }}</span>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
