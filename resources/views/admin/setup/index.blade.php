@extends('layouts.simple.master')
@section('title', 'User Cards')

@section('css')
@endsection

@section('style')
@endsection

@section('breadcrumb-title')
    <h3>Setup</h3>
@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item active">Setup</li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="edit-profile">
            <div class="row justify-content-center">
                <div class="col-xl-4">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title mb-0">Setup</h4>
                            <div class="card-options"><a class="card-options-collapse" href="#"
                                    data-bs-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a
                                    class="card-options-remove" href="#" data-bs-toggle="card-remove"><i
                                        class="fe fe-x"></i></a></div>
                        </div>
                        <div class="card-body">
                            <form method="POST" action="{{ route('setup-edit') }}">
                                @csrf
                                <div class="mb-3">
                                    <label class="form-label">Nama Ketua KPUM KM PNM</label>
                                    <input class="form-control" type="text" name="name" id="name"
                                        value="{{ $setup->name }}">
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">NPM Ketua KPUM KM PNM</label>
                                    <input class="form-control" type="text" name="npm" id="npm"
                                        value="{{ $setup->npm }}">
                                </div>
                                <div class="mb-3 row">
                                    <div class="col-6">
                                        <label class="form-label">Tanda Tangan</label>
                                        <input class="form-control" type="text" name="url_signature" id="url_signature"
                                            value="{{ $setup->url_signature }}">
                                    </div>
                                    <div class="col-6">
                                        <label class="form-label">Stempel</label>
                                        <input class="form-control" type="text" name="url_stamp" id="url_stamp"
                                            value="{{ $setup->url_stamp }}">
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <div class="col-6">
                                        <label class="form-label">Periode Sekarang</label>
                                        <input class="form-control" type="text" name="current_periode"
                                            id="current_periode" value="{{ $setup->current_periode }}">
                                    </div>
                                    <div class="col-6">
                                        <label class="form-label">Periode Selanjutnya</label>
                                        <input class="form-control" type="text" name="next_periode" id="next_periode"
                                            value="{{ $setup->next_periode }}">
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">No Surat Awal</label>
                                    <input class="form-control" type="number" name="no" id="no"
                                        value="{{ $setup->no }}">
                                </div>
                                <div class="form-footer">
                                    <button class="btn btn-primary btn-block" type="submit">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
@endsection
