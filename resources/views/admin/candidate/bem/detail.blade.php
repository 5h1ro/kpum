@extends('layouts.simple.master')
@section('title', 'HTML 5 Data Export')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendors/datatables.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendors/datatable-extension.css') }}">
@endsection

@section('style')
@endsection

@section('breadcrumb-title')
    <h3>Data Pendaftar BEM</h3>
@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item">Data Pendaftar</li>
    <li class="breadcrumb-item active">Data Pendaftar BEM</li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="col-auto">
                            <h5>Data Pendaftar BEM</h5>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <div class="col-auto">
                            <h5>Pendaftar Presiden BEM</h5>
                        </div>
                    </div>
                    <div class="card-body row">
                        <div class="col-12">
                            <div class="row mb-3">
                                <div class="col-12"><b>NPM</b></div>
                                <div class="col-12">{{ $calon->npm1 }}</div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-12"><b>Nama</b></div>
                                <div class="col-12">{{ $calon->fullname1 }}</div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-12"><b>Nama Panggilan</b></div>
                                <div class="col-12">{{ $calon->name1 }}</div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-12"><b>Tempat, Tanggal Lahir</b></div>
                                <div class="col-12">{{ $calon->birthplace1 }}, {{ $calon->birthday1 }}</div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-12"><b>Jenis Kelamin</b></div>
                                <div class="col-12">{{ $calon->gender1 }}</div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-12"><b>Jurusan</b></div>
                                <div class="col-12">{{ $calon->jurusan1->name }}</div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-12"><b>Program Studi/Kelas</b></div>
                                <div class="col-12">{{ $calon->prodi1->name }}/{{ $calon->class1 }}</div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-12"><b>Foto</b></div>
                                <div class="col-12">
                                    <img src="{{ $calon->photo1 }}" style="height: 200px;">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <div class="col-auto">
                            <h5>Pendaftar Wakil Presiden BEM</h5>
                        </div>
                    </div>
                    <div class="card-body row">
                        <div class="col-12">
                            <div class="row mb-3">
                                <div class="col-12"><b>NPM</b></div>
                                <div class="col-12">{{ $calon->npm2 }}</div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-12"><b>Nama</b></div>
                                <div class="col-12">{{ $calon->fullname2 }}</div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-12"><b>Nama Panggilan</b></div>
                                <div class="col-12">{{ $calon->name2 }}</div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-12"><b>Tempat, Tanggal Lahir</b></div>
                                <div class="col-12">{{ $calon->birthplace2 }}, {{ $calon->birthday2 }}</div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-12"><b>Jenis Kelamin</b></div>
                                <div class="col-12">{{ $calon->gender2 }}</div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-12"><b>Jurusan</b></div>
                                <div class="col-12">{{ $calon->jurusan2->name }}</div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-12"><b>Program Studi/Kelas</b></div>
                                <div class="col-12">{{ $calon->prodi2->name }}/{{ $calon->class2 }}</div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-12"><b>Foto</b></div>
                                <div class="col-12">
                                    <img src="{{ $calon->photo2 }}" style="height: 200px;">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <div class="col-auto">
                            <h5>Visi Misi dan Berkas Berkas</h5>
                        </div>
                    </div>
                    <div class="card-body row">
                        <div class="col-12">
                            <div class="row mb-3">
                                <div class="col-12"><b>Visi</b></div>
                                <div class="col-12">{{ $calon->visi }}</div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-12"><b>Misi</b></div>
                                @foreach ($calon->misi as $misi)
                                    <div class="col-12">{{ $loop->iteration }}. {{ $misi->detail }}</div>
                                @endforeach
                            </div>
                            <div class="row mb-3">
                                <div class="col-12"><b>Berkas Formulir</b></div>
                                <div class="col-12">
                                    <a class="btn btn-info" href="{{ $calon->formulir }}" target="_blank">Formulir</a>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-12"><b>Berkas Persyaratan</b></div>
                                <div class="col-12">
                                    <a class="btn btn-info" href="{{ $calon->persyaratan }}"
                                        target="_blank">Persyaratan</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer d-flex justify-content-end">
                        <button class="btn btn-success" id="acc" onclick="acc({{ $calon->id }})">
                            <i class="fa fa-check me-1"></i>
                            Terima
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        function acc(id) {
            location.href = "admin-bem-candidate-acc=" + id;
        }
    </script>
    <script src="{{ asset('assets/js/datatable/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.scroller.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/custom.js') }}"></script>
@endsection
