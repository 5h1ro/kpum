@extends('layouts.simple.master')
@section('title', 'HTML 5 Data Export')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendors/datatables.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendors/datatable-extension.css') }}">
@endsection

@section('style')
@endsection

@section('breadcrumb-title')
    <h3>Data DPM</h3>
@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item">Data Calon</li>
    <li class="breadcrumb-item active">Data DPM</li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <div class="col-auto">
                            <h5>Data BEM</h5>
                        </div>
                        <div class="col-auto">
                            <button class="btn btn-success align-self-end" data-bs-toggle="modal"
                                data-bs-target="#modalCreate" type="button">Buat Laporan</button>
                        </div>
                        <div class="modal fade bd-example-modal-lg" id="modalCreate" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <form action="{{ route('dpm-calon-create') }}" method="POST"
                                        enctype="multipart/form-data">
                                        @csrf
                                        <div class="modal-header">
                                            <h5 class="modal-title">Tambah Calon</h5>
                                            <button class="btn-close" type="button" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="mb-3 col-12">
                                                    <label class="col-form-label" for="place">Tempat:</label>
                                                    <input class="form-control" id="place" name="place" type="text"
                                                        required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-secondary" type="button"
                                                data-bs-dismiss="modal">Close</button>
                                            <button class="btn btn-primary" type="submit">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="col-auto">
                            <button class="btn btn-success align-self-end" data-bs-toggle="modal" data-bs-target="#modalAdd"
                                type="button">Tambah Calon</button>
                        </div>
                        <div class="modal fade bd-example-modal-lg" id="modalAdd" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <form action="{{ route('dpm-calon-add') }}" method="POST"
                                        enctype="multipart/form-data">
                                        @csrf
                                        <div class="modal-header">
                                            <h5 class="modal-title">Add Data</h5>
                                            <button class="btn-close" type="button" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="mb-3">
                                                    <label class="col-form-label" for="name">Nama Anggota
                                                        DPM:</label>
                                                    <input class="form-control" id="name" name="name" type="text"
                                                        required>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="mb-3">
                                                    <label class="col-form-label" for="nickname">Nama Pendek Anggota
                                                        DPM:</label>
                                                    <input class="form-control" id="nickname" name="nickname" type="text"
                                                        required>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="mb-3">
                                                    <label class="col-form-label" for="id_prodi">Program
                                                        Studi Calon Anggota DPM:</label>
                                                    <select class="form-select digits" id="id_prodi" name="id_prodi"
                                                        required>
                                                        @foreach ($prodi as $data2)
                                                            <option value="{{ $data2->id }}">
                                                                {{ $data2->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="mb-3 row">
                                                <div class="col-6">
                                                    <label class="col-form-label">Foto 1:</label>
                                                    <input class="form-control" type="file" name="image" id="image"
                                                        required>
                                                </div>
                                                <div class="col-6">
                                                    <label class="col-form-label">Foto 2:</label>
                                                    <input class="form-control" type="file" name="image2" id="image2"
                                                        required>
                                                </div>
                                            </div>
                                            <div class="mb-3">
                                                <label class="col-form-label" for="visi">Visi:</label>
                                                <textarea class="form-control" name="visi" id="visi" required></textarea>
                                            </div>
                                            <div class="mb-3">
                                                <label class="col-form-label" for="misi">Misi:</label>
                                                @for ($i = 0; $i <= 5; $i++)
                                                    <textarea class="form-control mb-2" name="misi{{ $i + 1 }}"
                                                        id="misi"></textarea>
                                                @endfor
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-secondary" type="button"
                                                data-bs-dismiss="modal">Close</button>
                                            <button class="btn btn-primary" type="submit">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                    <div class="card-body">
                        <div class="dt-ext table-responsive">
                            <table class="display" id="export-button">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Calon Anggota DPM</th>
                                        <th>Program Studi</th>
                                        <th>Jumlah Suara</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($calon as $data)
                                        <tr>
                                            <td>{{ $data->id }}</td>
                                            <td>{{ $data->name }}</td>
                                            <td>{{ $data->prodi->name }}</td>
                                            <td>{{ $data->vote }}</td>
                                            <td class="col-2">
                                                <button class="btn btn-icon icon-left btn-primary" data-bs-toggle="modal"
                                                    data-bs-target="#modalEdit{{ $loop->iteration }}" type="button">
                                                    <i class="fa fa-edit"></i> Edit</button>
                                            </td>
                                        </tr>
                                        <div class="modal fade bd-example-modal-lg" id="modalEdit{{ $loop->iteration }}"
                                            tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <form action="{{ route('dpm-calon-update', $data->id) }}"
                                                        method="POST">
                                                        @csrf
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Edit Data</h5>
                                                            <button class="btn-close" type="button"
                                                                data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="mb-3">
                                                                    <label class="col-form-label" for="name">Nama Anggota
                                                                        DPM:</label>
                                                                    <input class="form-control" id="name" name="name"
                                                                        type="text" value="{{ $data->name }}">
                                                                    <input class="form-control" id="id" name="id"
                                                                        type="text" value="{{ $data->id }}" hidden>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="mb-3">
                                                                    <label class="col-form-label" for="id_prodi">Program
                                                                        Studi Calon Anggota DPM:</label>
                                                                    <select class="form-select digits" id="id_prodi"
                                                                        name="id_prodi">
                                                                        @foreach ($prodi as $data2)
                                                                            @if ($data->id_prodi == $data2->id)
                                                                                <option value="{{ $data2->id }}"
                                                                                    selected>{{ $data2->name }}</option>
                                                                            @else
                                                                                <option value="{{ $data2->id }}">
                                                                                    {{ $data2->name }}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="mb-3 row">
                                                                <div class="col-6">
                                                                    <label class="col-form-label">Foto 1:</label>
                                                                    <input class="form-control" type="file" name="image"
                                                                        id="image">
                                                                </div>
                                                                <div class="col-6">
                                                                    <label class="col-form-label">Foto 2:</label>
                                                                    <input class="form-control" type="file" name="image2"
                                                                        id="image2">
                                                                </div>
                                                            </div>
                                                            <div class="mb-3">
                                                                <label class="col-form-label" for="visi">Visi:</label>
                                                                <textarea class="form-control" name="visi"
                                                                    id="visi">{{ $data->visi }}</textarea>
                                                            </div>
                                                            <div class="mb-3">
                                                                <label class="col-form-label" for="misi">Misi:</label>
                                                                @foreach ($data->misi as $misi)
                                                                    <textarea class="form-control mb-2"
                                                                        name="misi{{ $loop->iteration }}"
                                                                        id="misi">{{ $misi->detail }}</textarea>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button class="btn btn-secondary" type="button"
                                                                data-bs-dismiss="modal">Close</button>
                                                            <button class="btn btn-primary" type="submit">Save</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/js/datatable/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.scroller.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/custom.js') }}"></script>
@endsection
