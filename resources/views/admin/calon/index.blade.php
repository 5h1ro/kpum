@extends('layouts.simple.master')
@section('title', 'User Cards')

@section('css')
@endsection

@section('style')
@endsection

@section('breadcrumb-title')
    <h3>Data Calon</h3>
@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item active">Data Calon</li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-6 col-xl-4 box-col-6">
                <a href="{{ route('bem-calon') }}">
                    <div class="card custom-card">
                        <div class="card-header"><img class="img-fluid"
                                src="{{ asset('assets/images/user-card/1.jpg') }}" alt=""></div>
                        <div class="card-profile"><img class="rounded-circle"
                                src="{{ asset('assets/images/avtar/bem.jpg') }}" alt=""></div>
                        <div class="text-center profile-details">
                            <h4>BEM</h4>
                            <h6>Badan Eksekutif Mahasiswa</h6>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-4 box-col-6">
                <a href="{{ route('dpm-calon') }}">
                    <div class="card custom-card">
                        <div class="card-header"><img class="img-fluid"
                                src="{{ asset('assets/images/user-card/2.jpg') }}" alt=""></div>
                        <div class="card-profile"><img class="rounded-circle"
                                src="{{ asset('assets/images/avtar/dpm.jpg') }}" alt=""></div>
                        <div class="text-center profile-details">
                            <h4>DPM</h4>
                            <h6>Dewan Perwakilan Mahasiswa</h6>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-6 col-xl-4 box-col-6">
                <a href="{{ route('hmj-calon') }}">
                    <div class="card custom-card">
                        <div class="card-header"><img class="img-fluid"
                                src="{{ asset('assets/images/user-card/1.jpg') }}" alt=""></div>
                        <div class="card-profile"><img class="rounded-circle"
                                src="{{ asset('assets/images/avtar/hmj.jpg') }}" alt=""></div>
                        <div class="text-center profile-details">
                            <h4>HMJ</h4>
                            <h6>Himpunan Mahasiswa Jurusan</h6>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-4 box-col-6">
                <a href="{{ route('hima-calon') }}">
                    <div class="card custom-card">
                        <div class="card-header"><img class="img-fluid"
                                src="{{ asset('assets/images/user-card/2.jpg') }}" alt=""></div>
                        <div class="card-profile"><img class="rounded-circle"
                                src="{{ asset('assets/images/avtar/hima.jpg') }}" alt=""></div>
                        <div class="text-center profile-details">
                            <h4>HIMA</h4>
                            <h6>Himpunan Mahasiswa Program Studi</h6>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
@endsection

@section('script')

@endsection
