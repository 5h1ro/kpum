@extends('layouts.simple.master')
@section('title', 'HTML 5 Data Export')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendors/datatables.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendors/datatable-extension.css') }}">
@endsection

@section('style')
@endsection

@section('breadcrumb-title')
    <h3>Data User</h3>
@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item active">Data User</li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-6">
                                <h5>Data Pemilih</h5>
                            </div>
                            {{-- <div class="col-6 d-flex justify-content-end">
                                <button data-bs-toggle="modal" data-bs-target="#modalAdd"
                                    class="btn btn-success col-auto">Add User</button>
                            </div> --}}
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="dt-ext table-responsive">
                            <table class="display" id="export-button">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Jurusan</th>
                                        <th>Program Studi</th>
                                        <th>Suara BEM</th>
                                        <th>Suara DPM</th>
                                        <th>Suara HMJ</th>
                                        <th>Suara HIMA</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($voter as $data)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $data->voter->name }}</td>
                                            <td>{{ $data->voter->jurusan->name }}</td>
                                            <td>{{ $data->voter->prodi->name }}</td>
                                            <td>
                                                @if ($data->voter->bem == 'Sudah Memilih')
                                                    <div class="btn btn-success" style="cursor: default">
                                                        {{ $data->voter->bem }}</div>
                                                @else
                                                    <div class="btn btn-danger" style="cursor: default">
                                                        {{ $data->voter->bem }}</div>
                                                @endif
                                            </td>
                                            <td>
                                                @if ($data->voter->dpm == 'Sudah Memilih')
                                                    <div class="btn btn-success" style="cursor: default">
                                                        {{ $data->voter->dpm }}</div>
                                                @else
                                                    <div class="btn btn-danger" style="cursor: default">
                                                        {{ $data->voter->dpm }}</div>
                                                @endif
                                            </td>
                                            <td>
                                                @if ($data->voter->hmj == 'Sudah Memilih')
                                                    <div class="btn btn-success" style="cursor: default">
                                                        {{ $data->voter->hmj }}</div>
                                                @else
                                                    <div class="btn btn-danger" style="cursor: default">
                                                        {{ $data->voter->hmj }}</div>
                                                @endif
                                            </td>
                                            <td>
                                                @if ($data->voter->hima == 'Sudah Memilih')
                                                    <div class="btn btn-success" style="cursor: default">
                                                        {{ $data->voter->hima }}</div>
                                                @else
                                                    <div class="btn btn-danger" style="cursor: default">
                                                        {{ $data->voter->hima }}</div>
                                                @endif
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-12 d-flex justify-content-center">
                                                        <button type="button" data-bs-toggle="modal"
                                                            data-bs-target="#modalEdit{{ $data->voter->id }}"
                                                            class="btn btn-primary" title="">
                                                            <i class="fa fa-edit me-1"></i>
                                                            Edit
                                                        </button>
                                                    </div>
                                                    <div class="col-12 d-flex justify-content-center mt-2">
                                                        <button class="btn btn-danger" id="remove"
                                                            onclick="remove({{ $data->voter->id }})">
                                                            <i class="fa fa-trash me-1"></i>
                                                            Delete
                                                        </button>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <div class="modal fade bd-example-modal-lg" id="modalEdit{{ $data->voter->id }}"
                                            tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <form action="{{ route('data-user-edit', $data->voter->id) }}"
                                                        method="POST" enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Edit Data</h5>
                                                            <button class="btn-close" type="button"
                                                                data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="mb-3 col-6">
                                                                    <label class="col-form-label" for="name">Name :</label>
                                                                    <input class="form-control" id="name" name="name"
                                                                        type="text" value="{{ $data->voter->name }}">
                                                                    <input class="form-control" id="id" name="id"
                                                                        type="text" value="{{ $data->voter->id }}"
                                                                        hidden>
                                                                </div>
                                                                <div class="mb-3 col-6">
                                                                    <label class="col-form-label" for="number">Number
                                                                        :</label>
                                                                    <input class="form-control" type="number"
                                                                        name="number" id="number"
                                                                        value="{{ $data->voter->number }}"></input>
                                                                </div>
                                                            </div>
                                                            <div class="mb-3 row">
                                                                <div class="col-6">
                                                                    <label class="col-form-label" for="jurusan">Jurusan
                                                                        :</label>
                                                                    <select class="form-select" name="jurusan"
                                                                        id="jurusan">
                                                                        @foreach ($jurusan as $jurusans)
                                                                            @if ($jurusans->id == $data->voter->id_jurusan)
                                                                                <option value="{{ $jurusans->id }}"
                                                                                    selected>
                                                                                    {{ $jurusans->name }}</option>
                                                                            @else
                                                                                <option value="{{ $jurusans->id }}">
                                                                                    {{ $jurusans->name }}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="col-6">
                                                                    <label class="col-form-label" for="prodi">Program
                                                                        Studi
                                                                        :</label>
                                                                    <select class="form-select" name="prodi" id="prodi">
                                                                        @foreach ($prodi as $prodis)
                                                                            @if ($prodis->id == $data->voter->id_prodi)
                                                                                <option value="{{ $prodis->id }}"
                                                                                    selected>
                                                                                    {{ $prodis->name }}</option>
                                                                            @else
                                                                                <option value="{{ $prodis->id }}">
                                                                                    {{ $prodis->name }}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="mb-3 row">
                                                                <div class="col-6"><label class="col-form-label"
                                                                        for="bem">Suara BEM
                                                                        :</label>
                                                                    <select class="form-select" name="bem" id="bem">
                                                                        @if ($data->voter->bem == 'Belum Memilih')
                                                                            <option value="0" selected>
                                                                                Belum Memilih</option>
                                                                            <option value="1">
                                                                                Sudah Memilih</option>
                                                                        @else
                                                                            <option value="0">
                                                                                Belum Memilih</option>
                                                                            <option value="1" selected>
                                                                                Sudah Memilih</option>
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                                <div class="col-6"><label class="col-form-label"
                                                                        for="dpm">Suara DPM
                                                                        :</label>
                                                                    <select class="form-select" name="dpm" id="dpm">
                                                                        @if ($data->voter->dpm == 'Belum Memilih')
                                                                            <option value="0" selected>
                                                                                Belum Memilih</option>
                                                                            <option value="1">
                                                                                Sudah Memilih</option>
                                                                        @else
                                                                            <option value="0">
                                                                                Belum Memilih</option>
                                                                            <option value="1" selected>
                                                                                Sudah Memilih</option>
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="mb-3 row">
                                                                <div class="col-6"><label class="col-form-label"
                                                                        for="hmj">Suara HMJ
                                                                        :</label>
                                                                    <select class="form-select" name="hmj" id="hmj">
                                                                        @if ($data->voter->hmj == 'Belum Memilih')
                                                                            <option value="0" selected>
                                                                                Belum Memilih</option>
                                                                            <option value="1">
                                                                                Sudah Memilih</option>
                                                                        @else
                                                                            <option value="0">
                                                                                Belum Memilih</option>
                                                                            <option value="1" selected>
                                                                                Sudah Memilih</option>
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                                <div class="col-6"><label class="col-form-label"
                                                                        for="hima">Suara HIMA
                                                                        :</label>
                                                                    <select class="form-select" name="hima" id="hima">
                                                                        @if ($data->voter->hima == 'Belum Memilih')
                                                                            <option value="0" selected>
                                                                                Belum Memilih</option>
                                                                            <option value="1">
                                                                                Sudah Memilih</option>
                                                                        @else
                                                                            <option value="0">
                                                                                Belum Memilih</option>
                                                                            <option value="1" selected>
                                                                                Sudah Memilih</option>
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="mb-3 row">
                                                                <div class="col-6"><label class="col-form-label"
                                                                        for="os">Sistem Operasi
                                                                        :</label>
                                                                    <select class="form-select" name="os" id="os">
                                                                        @foreach ($os as $oss)
                                                                            @if ($oss == $data->os)
                                                                                <option value="{{ $oss }}"
                                                                                    selected>
                                                                                    {{ $oss }}</option>
                                                                            @else
                                                                                <option value="{{ $oss }}">
                                                                                    {{ $oss }}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="col-6"><label class="col-form-label"
                                                                        for="browser">Browser
                                                                        :</label>
                                                                    <select class="form-select" name="browser"
                                                                        id="browser">
                                                                        @foreach ($browser as $browsers)
                                                                            @if ($browsers == $data->browser)
                                                                                <option value="{{ $browsers }}"
                                                                                    selected>
                                                                                    {{ $browsers }}</option>
                                                                            @else
                                                                                <option value="{{ $browsers }}">
                                                                                    {{ $browsers }}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button class="btn btn-secondary" type="button"
                                                                data-bs-dismiss="modal">Close</button>
                                                            <button class="btn btn-primary" type="submit">Save</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </tbody>
                            </table>
                            <nav aria-label="Page navigation example"
                                class="d-flex justify-content-end align-content-end align-self-end">
                                <ul class="pagination pagination-primary">
                                    @if ($voter->onFirstPage())
                                        <li class="page-item disabled">
                                            <a class="page-link" href="#" tabindex="-1">Previous</a>
                                        </li>
                                    @else
                                        <li class="page-item"><a class="page-link"
                                                href="{{ $voter->previousPageUrl() }}">Previous</a></li>
                                    @endif
                                    @for ($i = 1; $i <= $voter->lastPage(); $i++)
                                        @php
                                            $half_total_links = floor(3);
                                            $from = $voter->currentPage() - $half_total_links;
                                            $to = $voter->currentPage() + $half_total_links;
                                            if ($voter->currentPage() < $half_total_links) {
                                                $to += $half_total_links - $voter->currentPage();
                                            }
                                            if ($voter->lastPage() - $voter->currentPage() < $half_total_links) {
                                                $from -= $half_total_links - ($voter->lastPage() - $voter->currentPage()) - 1;
                                            }
                                        @endphp
                                        @if ($from < $i && $i < $to)
                                            <li class="page-item {{ $voter->currentPage() == $i ? ' disabled' : '' }}">
                                                <a href="{{ $voter->url($i) }}"
                                                    class="page-link">{{ $i }}</a>
                                            </li>
                                        @endif
                                    @endfor

                                    @if ($voter->hasMorePages())
                                        <li class="page-item">
                                            <a class="page-link" href="{{ $voter->nextPageUrl() }}"
                                                rel="next">Next</a>
                                        </li>
                                    @else
                                        <li class="page-item disabled">
                                            <a class="page-link" href="#">Next</a>
                                        </li>
                                    @endif
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        function remove(id) {
            location.href = "data-user-delete=" + id;
        }
    </script>
    <script src="{{ asset('assets/js/datatable/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.scroller.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/custom.js') }}"></script>
@endsection
