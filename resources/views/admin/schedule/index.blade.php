@extends('layouts.simple.master')
@section('title', 'HTML 5 Data Export')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendors/datatables.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendors/datatable-extension.css') }}">
@endsection

@section('style')
@endsection

@section('breadcrumb-title')
    <h3>Data Jadwal</h3>
@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item active">Data Jadwal</li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-6">
                                <h5>Data Jadwal</h5>
                            </div>
                            <div class="col-6 d-flex justify-content-end">
                                <button data-bs-toggle="modal" data-bs-target="#modalAdd"
                                    class="btn btn-success col-auto">Tambah Jadwal</button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="dt-ext table-responsive">
                            <table class="display" id="export-button">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Mulai</th>
                                        <th>Selesai</th>
                                        <th>Jurusan</th>
                                        <th>Prodi</th>
                                        <th>Kategori</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @for ($i = 0; $i < count($schedule); $i++)
                                        <tr>
                                            <td>{{ $i + 1 }}</td>
                                            <td>{{ $schedule[$i]->name }}</td>
                                            <td>{{ $schedule[$i]->start }}</td>
                                            <td>{{ $schedule[$i]->end }}</td>
                                            <td>{{ $schedule[$i]->jurusan->name }}</td>
                                            <td>{{ $schedule[$i]->prodi->name }}</td>
                                            <td>{{ $schedule[$i]->category }}</td>
                                            <td class="row">
                                                <div class="col-auto">
                                                    <button type="button" data-bs-toggle="modal"
                                                        data-bs-target="#modalEdit{{ $schedule[$i]->id }}"
                                                        class="btn btn-primary" title="">
                                                        <i class="fa fa-edit me-1"></i>
                                                        Edit
                                                    </button>
                                                </div>
                                                <div class="col-auto">
                                                    <button class="btn btn-danger" id="remove"
                                                        onclick="remove({{ $schedule[$i]->id }})">
                                                        <i class="fa fa-trash me-1"></i>
                                                        Delete
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                        <div class="modal fade bd-example-modal-lg" id="modalEdit{{ $schedule[$i]->id }}"
                                            tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <form action="{{ route('data-schedule-edit', $schedule[$i]->id) }}"
                                                        method="POST" enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Edit Jadwal</h5>
                                                            <button class="btn-close" type="button"
                                                                data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="mb-3 col-6">
                                                                    <label class="col-form-label" for="name">Name :</label>
                                                                    <input class="form-control" id="name" name="name"
                                                                        type="text" value="{{ $schedule[$i]->name }}">
                                                                    iman
                                                                </div>
                                                                <div class="mb-3 col-6">
                                                                    <label class="col-form-label" for="category">Kategori
                                                                        :</label>
                                                                    <select class="form-select" name="category"
                                                                        id="category">
                                                                        @foreach ($category as $categories)
                                                                            @if ($categories == $schedule[$i]->category)
                                                                                <option value="{{ $categories }}"
                                                                                    selected>
                                                                                    {{ $categories }}</option>
                                                                            @else
                                                                                <option value="{{ $categories }}">
                                                                                    {{ $categories }}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="mb-3 row">
                                                                <div class="col-6">
                                                                    <label class="col-form-label" for="start">Mulai
                                                                        :</label>
                                                                    <input class="form-control digits" id="start"
                                                                        name="start" type="datetime-local"
                                                                        value="{{ $startedit[$i] }}">
                                                                </div>
                                                                <div class="col-6">
                                                                    <label class="col-form-label" for="end">Selesai
                                                                        :</label>
                                                                    <input class="form-control digits" id="end" name="end"
                                                                        type="datetime-local" value="{{ $endedit[$i] }}">
                                                                </div>
                                                            </div>
                                                            <div class="mb-3 row">
                                                                <div class="col-6">
                                                                    <label class="col-form-label" for="id_jurusan">Jurusan
                                                                        :</label>
                                                                    <select class="form-select" name="id_jurusan"
                                                                        id="id_jurusan">
                                                                        @foreach ($jurusan as $jurusans)
                                                                            @if ($jurusans->id == $schedule[$i]->jurusan->id)
                                                                                <option value="{{ $jurusans->id }}"
                                                                                    selected>
                                                                                    {{ $jurusans->name }}</option>
                                                                            @else
                                                                                <option value="{{ $jurusans->id }}">
                                                                                    {{ $jurusans->name }}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="col-6">
                                                                    <label class="col-form-label" for="id_prodi">Program
                                                                        Studi
                                                                        :</label>
                                                                    <select class="form-select" name="id_prodi"
                                                                        id="id_prodi">
                                                                        @foreach ($prodi as $prodis)
                                                                            @if ($prodis->id == $schedule[$i]->prodi->id)
                                                                                <option value="{{ $prodis->id }}"
                                                                                    selected>
                                                                                    {{ $prodis->name }}</option>
                                                                            @else
                                                                                <option value="{{ $prodis->id }}">
                                                                                    {{ $prodis->name }}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button class="btn btn-secondary" type="button"
                                                                data-bs-dismiss="modal">Close</button>
                                                            <button class="btn btn-primary" type="submit">Save</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endfor
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-lg" id="modalAdd" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="{{ route('data-schedule-add') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Jadwal</h5>
                        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="mb-3 col-6">
                                <label class="col-form-label" for="name">Name :</label>
                                <input class="form-control" id="name" name="name" type="text" required>
                            </div>
                            <div class="mb-3 col-6">
                                <label class="col-form-label" for="category">Kategori
                                    :</label>
                                <select class="form-select" name="category" id="category">
                                    @foreach ($category as $categories)
                                        <option value="{{ $categories }}">
                                            {{ $categories }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <div class="col-6">
                                <label class="col-form-label" for="start">Mulai
                                    :</label>
                                <input class="form-control digits" id="start" name="start" type="datetime-local"
                                    value="{{ $nowadd }}">
                            </div>
                            <div class="col-6">
                                <label class="col-form-label" for="end">Selesai
                                    :</label>
                                <input class="form-control digits" id="end" name="end" type="datetime-local"
                                    value="{{ $nowadd }}">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <div class="col-6">
                                <label class="col-form-label" for="id_jurusan">Jurusan
                                    :</label>
                                <select class="form-select" name="id_jurusan" id="id_jurusan">
                                    @foreach ($jurusan as $jurusans)
                                        <option value="{{ $jurusans->id }}">
                                            {{ $jurusans->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-6">
                                <label class="col-form-label" for="id_prodi">Program Studi
                                    :</label>
                                <select class="form-select" name="id_prodi" id="id_prodi">
                                    @foreach ($prodi as $prodis)
                                        <option value="{{ $prodis->id }}">
                                            {{ $prodis->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button>
                        <button class="btn btn-primary" type="submit">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        function remove(id) {
            location.href = "data-schedule-delete=" + id;
        }
    </script>
    <script src="{{ asset('assets/js/datatable/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.scroller.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/custom.js') }}"></script>
@endsection
