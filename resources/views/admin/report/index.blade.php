@extends('layouts.simple.master')
@section('title', 'HTML 5 Data Export')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendors/datatables.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendors/datatable-extension.css') }}">
@endsection

@section('style')
@endsection

@section('breadcrumb-title')
    <h3>Data Jadwal</h3>
@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item active">Data Laporan</li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-6">
                                <h5>Data Laporan</h5>
                            </div>
                            <div class="col-6 d-flex justify-content-end">
                                <button data-bs-toggle="modal" data-bs-target="#modalAdd"
                                    class="btn btn-success col-auto">Tambah Laporan</button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="dt-ext table-responsive">
                            <table class="display" id="export-button">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Kategori</th>
                                        <th>Program Studi</th>
                                        <th>No Surat</th>
                                        <th>Surat Keputusan TAP</th>
                                        <th>Surat Keputusan Perhitungan</th>
                                        <th>Berita Acara</th>
                                        <th>Surat Keputusan Penetapan</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @for ($i = 0; $i < count($report); $i++)
                                        <tr>
                                            <td>{{ $i + 1 }}</td>
                                            <td>{{ $report[$i]->name }}</td>
                                            <td>{{ $report[$i]->category }}</td>
                                            <td>{{ empty($report[$i]->prodi->name) ? '' : $report[$i]->prodi->name }}</td>
                                            <td>{{ $report[$i]->no }}</td>
                                            <td><a href="{{ $report[$i]->url_sk_tap }}"
                                                    class="btn btn-success {{ empty($report[$i]->url_sk_tap) ? 'd-none' : '' }}">SK
                                                    TAP</a></td>
                                            <td><a href="{{ $report[$i]->url_sk_perhitungan }}"
                                                    class="btn btn-success {{ empty($report[$i]->url_sk_perhitungan) ? 'd-none' : '' }}">SK
                                                    Perhitungan</a></td>
                                            <td><a href="{{ $report[$i]->url_ba }}"
                                                    class="btn btn-success {{ empty($report[$i]->url_ba) ? 'd-none' : '' }}">Berita
                                                    Acara</a></td>
                                            <td><a href="{{ $report[$i]->url_sk_penetapan }}"
                                                    class="btn btn-success {{ empty($report[$i]->url_sk_penetapan) ? 'd-none' : '' }}">SK
                                                    Penetapan</a></td>
                                            <td class="row">
                                                <div class="col-auto">
                                                    <button class="btn btn-danger" id="remove"
                                                        onclick="remove({{ $report[$i]->id }})">
                                                        <i class="fa fa-trash me-1"></i>
                                                        Delete
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    @endfor
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-lg" id="modalAdd" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="{{ route('data-report-add') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Laporan</h5>
                        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="mb-3 col-12">
                                <label class="col-form-label" for="place">Tempat :</label>
                                <input class="form-control" id="place" name="place" type="text" required>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <div class="col-12">
                                <label class="col-form-label" for="category">Kategori
                                    :</label>
                                <select class="form-select" name="category" id="category">
                                    @foreach ($category as $categories)
                                        <option value="{{ $categories }}">
                                            {{ $categories }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <div class="col-12 d-none" id="jurusan">
                                <label class="col-form-label" for="id_jurusan">Jurusan
                                    :</label>
                                <select class="form-select" name="id_jurusan" id="id_jurusan">
                                    @foreach ($jurusan as $prodis)
                                        <option value="{{ $prodis->id }}">
                                            {{ $prodis->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-12 d-none" id="prodi">
                                <label class="col-form-label" for="id_prodi">Program Studi
                                    :</label>
                                <select class="form-select" name="id_prodi" id="id_prodi">
                                    @foreach ($prodi as $prodis)
                                        <option value="{{ $prodis->id }}">
                                            {{ $prodis->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button>
                        <button class="btn btn-primary" type="submit">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/js/datatable/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.scroller.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/custom.js') }}"></script>
    <script type="text/javascript">
        function remove(id) {
            location.href = "data-report-delete=" + id;
        }

        $('#category').on('change', function() {
            const select = $('#category').val();
            if (select == 'HMJ' || select == 'HIMA') {
                if (select == 'HMJ') {
                    $('#jurusan').removeClass('d-none');
                    $('#prodi').addClass('d-none');
                } else {
                    $('#jurusan').addClass('d-none');
                    $('#prodi').removeClass('d-none');
                }
            } else {
                $('#jurusan').addClass('d-none');
                $('#prodi').addClass('d-none');
            }
        });
    </script>
@endsection
