@extends('layouts.authentication.master')
@section('title', 'Login-one')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendors/sweetalert2.css') }}">
@endsection

@section('style')
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="login-card">
                    <div>
                        <div><a class="logo text-start" href="#">
                                <img class="img-fluid for-light" src="{{ asset('assets/images/logo/login.png') }}"
                                    alt="looginpage">
                                <img class="img-fluid for-dark" src="{{ asset('assets/images/logo/logo_dark.png') }}"
                                    alt="looginpage">
                            </a>
                        </div>
                        <div class="login-main">
                            <form class="theme-form" action="{{ route('login') }}" method="POST">
                                @csrf
                                <h4>Selamat Datang Di PEMIRA 2021</h4>
                                <p>Masukkan NPM & Password</p>
                                <div class="form-group">
                                    <label class="col-form-label">Masukkan NPM</label>
                                    <input class="form-control" type="number" required="" placeholder="masukkan NPM anda"
                                        name="npm" id="npm">
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label">Masukkan Password</label>
                                    <input class="form-control" type="password" name="password" id="password" required=""
                                        placeholder="*********">
                                    <div class="show-hide mt-3"><span class="show"></span></div>
                                </div>
                                <div class="form-group mb-0">
                                    <button class="btn btn-primary btn-block" type="submit">Masuk</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

    <script src="{{ asset('assets/js/sweet-alert/sweetalert.min.js') }}"></script>
    @if ($errors->any())
        <script src="{{ asset('assets/js/sweet-alert/login.js') }}"></script>
    @endif
    @if (Session::has('Success'))
        <script src="{{ asset('assets/js/sweet-alert/register_success.js') }}"></script>
    @endif
    @if (Session::has('Error'))
        <script src="{{ asset('assets/js/sweet-alert/register_error.js') }}"></script>
    @endif
    @if (Session::has('NotAuth'))
        <script src="{{ asset('assets/js/sweet-alert/no_auth.js') }}"></script>
    @endif
@endsection
