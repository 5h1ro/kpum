@extends('layouts.authentication.master')
@section('title', 'Login-one')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendors/sweetalert2.css') }}">
@endsection

@section('style')
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="login-card">
                    <div>
                        <div><a class="logo text-start" href="#">
                                <img class="img-fluid for-light" src="{{ asset('assets/images/logo/login.png') }}"
                                    alt="looginpage">
                                <img class="img-fluid for-dark" src="{{ asset('assets/images/logo/logo_dark.png') }}"
                                    alt="looginpage">
                            </a>
                        </div>
                        <div class="login-main">
                            <form class="theme-form" action="{{ route('register-create') }}" method="POST">
                                @csrf
                                <h4>Selamat Datang Di PEMIRA 2021</h4>
                                <p>Masukkan Sesuai Ketentuan</p>
                                <div class="form-group">
                                    <label class="col-form-label">Nama</label>
                                    <input class="form-control bg-white" type="text" required="" placeholder="Andi"
                                        name="name" id="name">
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label">Nama Lengkap</label>
                                    <input class="form-control bg-white" type="text" required="" placeholder="Andi Budiarto"
                                        name="fullname" id="fullname">
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label">NPM</label>
                                    <input class="form-control bg-white" type="text" required="" placeholder="120000001"
                                        name="npm" id="npm">
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label">No WA</label>
                                    <input class="form-control bg-white" type="text" required="" placeholder="628580007000"
                                        name="number" id="number">
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label">Program Studi</label>
                                    <select class="form-select" id="id_prodi" name="id_prodi">
                                        @foreach ($prodi as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label">Email</label>
                                    <input class="form-control bg-white" type="text" required=""
                                        placeholder="example@email.com" name="email" id="email">
                                </div>
                                <div class="form-group">
                                    <label class="col-form-label">Password</label>
                                    <input class="form-control bg-white" type="password" name="password" id="password"
                                        required="" placeholder="*********">
                                    <div class="show-hide mt-3"><span class="show"></span></div>
                                </div>
                                <div class="form-group mb-0">
                                    <p class="txt-primary">Sudah punya akun? klik <a
                                            href="{{ route('login') }}"><u>disini</u></a></p>
                                    <button class="btn btn-primary btn-block" type="submit">Register</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

    <script src="{{ asset('assets/js/sweet-alert/sweetalert.min.js') }}"></script>
    @if ($errors->any())
        <script src="{{ asset('assets/js/sweet-alert/login.js') }}"></script>
    @endif
@endsection
