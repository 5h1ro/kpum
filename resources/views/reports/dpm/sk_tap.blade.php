<!DOCTYPE html>
<html>

<head>
    <title>Surat Keterangan</title>
    <style>
        .mt-05 {
            margin-top: 0.5cm;
        }

        .mt-1 {
            margin-top: 1cm;
        }

        .mt-2 {
            margin-top: 2cm;
        }

        .mt-5 {
            margin-top: 5cm;
        }

        .container {
            margin-left: 1.5cm;
            margin-top: 1cm;
            margin-right: 1.5cm;
            margin-bottom: 1.5cm;
        }

        td {
            vertical-align: top;
        }

        .point {
            width: 1cm;
        }

        .justify {
            text-align: justify;
        }

        .page_break {
            page-break-before: always;
        }

    </style>
</head>

<body>

    <div class="container">
        <center>
            <img src="{{ public_path('image/icon/main_icon.png') }}" style="width:2.5cm;">
            <p style="font-weight: bold">
                KEPUTUSAN
                <br>
                KOMISI PEMILIHAN UMUM MAHASISWA
                <br>
                POLITEKNIK NEGERI MADIUN
                <br>
                PERIODE {{ $setup->current_periode }}
                <hr>
                <span style="font-weight: bold">
                    Nomor : {{ $setup->no }}
                </span>
            </p>
            <div class="mt-1">
                <p>
                    TENTANG :
                    <br>
                    <span style="font-weight: bold">
                        PENETAPAN
                        <br>
                        ANGGOTA DEWAN PERWAKILAN MAHASISWA – KELUARGA MAHASISWA
                        <br>
                        POLITEKNIK NEGERI MADIUN
                        <br>
                        PERIODE {{ $setup->next_periode }}
                    </span>
                </p>
            </div>
            <div class="mt-05">
                <p>
                    KETUA KOMISI PEMILIHAN UMUM MAHASISWA,
                </p>
            </div>
        </center>
        <br />
        <table>
            <tr>
                <td>Menimbang</td>
                <td>:</td>
                <td class="point">a.</td>
                <td class="justify">bahwa Pemilihan Raya Organisasi Kemahasiswaan Politeknik Negeri Madiun adalah
                    ajang regenerasi dan
                    suksesi kepemimpinan berlandaskan demokrasi yang menjunjung tinggi azas-azas Pemilihan Umum;
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td class="point">c.</td>
                <td class="justify">bahwa Komisi Pemilihan Umum Mahasiswa memiliki tugas, wewenang dan kewajiban
                    dalam menetapkan Calon Anggota Dewan Perwakilan Mahasiswa – Keluarga Mahasiswa bahwa berdasarkan
                    pertimbangan sebagaimana dimaksud dalam huruf a dan huruf b, maka perlu menetapkan Keputusan Komisi
                    Pemilihan Umum Mahasiswa tentang Penetapan Calon Anggota Dewan Perwakilan Mahasiswa – Keluarga
                    Mahasiswa Politeknik Negeri Madiun Periode {{ $setup->next_periode }}.
                </td>
            </tr>
            <tr>
                <td style="height: 0.2cm;"></td>
            </tr>
            <tr>
                <td>Mengingat</td>
                <td>:</td>
                <td class="point"></td>
                <td class="justify">Pasal 10 Undang-Undang Pemilihan Raya Organisasi Kemahasiswaan Politeknik
                    Negeri Madiun.
                </td>
            </tr>
            <tr>
                <td style="height: 0.2cm;"></td>
            </tr>
            <tr>
                <td>Memerhatikan</td>
                <td>:</td>
                <td class="point"></td>
                <td class="justify">Pembahasan dan masukan – masukan dari tim verifikator Komisi Pemilihan Umum
                    Mahasiswa;
                </td>
            </tr>
        </table>
        <div class="page_break"></div>
        <center>
            <p>Dengan senantiasa memohon petunjuk dari Tuhan Yang Maha Kuasa,
                <br>
            </p>
            <p style="font-weight: bold; letter-spacing: 0.1cm; margin-top: -0.2cm">
                MEMUTUSKAN
            </p>
        </center>
        <div class="mt-1">
            <table>
                <tr>
                    <td>Menetapkan </td>
                    <td>:</td>
                    <td class="point"></td>
                    <td class="justify">KEPUTUSAN KOMISI PEMILIHAN UMUM MAHASISWA TENTANG CALON ANGGOTA DEWAN
                        PERWAKILAN MAHASISWA – KELUARGA MAHASISWAPOLITEKNIK NEGERI MADIUN PERIODE
                        {{ $setup->next_periode }};
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>PERTAMA</td>
                    <td>:</td>
                    <td class="point"></td>
                    <td class="justify">Menetapkan hasil dari tim verifikator Komisi Pemilihan Umum Mahasiswa
                        Tahun {{ $setup->current_periode }}, sebagai berikut:
                    </td>
                </tr>
            </table>
            <p style="margin-top: 1mm">A. Jurusan Administrasi Bisnis</p>
            <table style="margin-top: -0.3cm; margin-left: 5mm">
                @foreach ($dpm->adbis as $data)
                    <tr>
                        <td style="width: 0.5cm">{{ $loop->iteration }}. </td>
                        <td class="justify">{{ $data->name }} ({{ $data->prodi->name }})
                        </td>
                    </tr>
                @endforeach
            </table>
            <p style="margin-top: 1mm">B. Jurusan Komputerisasi Akuntansi</p>
            <table style="margin-top: -0.3cm; margin-left: 5mm">
                @foreach ($dpm->kompak as $data)
                    <tr>
                        <td style="width: 0.5cm">{{ $loop->iteration }}. </td>
                        <td class="justify">{{ $data->name }} ({{ $data->prodi->name }})
                        </td>
                    </tr>
                @endforeach
            </table>
            <p style="margin-top: 1mm">C. Jurusan Teknik</p>
            <table style="margin-top: -0.3cm; margin-left: 5mm">
                @foreach ($dpm->teknik as $data)
                    <tr>
                        <td style="width: 0.5cm">{{ $loop->iteration }}. </td>
                        <td class="justify">{{ $data->name }} ({{ $data->prodi->name }})
                        </td>
                    </tr>
                @endforeach
            </table>
            <table>
                <tr>
                    <td style="height: 0.1cm;"></td>
                </tr>
                <tr>
                    <td style="width: 2cm">KEDUA</td>
                    <td>:</td>
                    <td class="point"></td>
                    <td class="justify">Hasil dari keputusan ini selanjutnya ditindak lanjuti oleh yang
                        bersangkutan mengikuti petunjuk dari Komisi Pemilihan Umum Mahasiswa.
                    </td>
                </tr>
                <tr>
                    <td style="width: 2cm">KEDUA</td>
                    <td>:</td>
                    <td class="point"></td>
                    <td class="justify">Keputusan ini berlaku sejak tanggal ditetapkan.
                    </td>
                </tr>
            </table>
            <div>
                <p>Salinan Keputusan ini disampaikan kepada :</p>
                <p>Ketua Dewan Perwakilan Mahasiswa-KM {{ $setup->current_periode }};</p>
            </div>
            <div>
                <table>
                    <tr>
                        <td style="height: 0.1cm;"></td>
                    </tr>
                    <tr>
                        <td>Ditetapkan di</td>
                        <td>:</td>
                        <td style="width: 0.5cm"></td>
                        <td class="justify">{{ $request->place }}
                        </td>
                    </tr>
                    <tr>
                        <td>Pada tanggal</td>
                        <td>:</td>
                        <td style="width: 0.5cm"></td>
                        <td class="justify">{{ $setup->now }}
                        </td>
                    </tr>
                </table>
            </div>
            <div style="display: flex ">
                <div style="width: 7cm; margin-left: 8.6cm">
                    <center>
                        <p>
                            Hormat Kami,
                            <br>
                            Ketua KPUM
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <span style="font-weight: bold;"><u>{{ $setup->name }}</u></span>
                            <br>
                            <span style="font-weight: bold;">NIM. {{ $setup->npm }}</span>
                        </p>
                    </center>
                </div>
            </div>
        </div>
    </div>

</body>

</html>
