<!DOCTYPE html>
<html>

<head>
    <title>Surat Keterangan</title>
    <style>
        .mt-05 {
            margin-top: 0.5cm;
        }

        .mt-1 {
            margin-top: 1cm;
        }

        .mt-2 {
            margin-top: 2cm;
        }

        .mt-5 {
            margin-top: 5cm;
        }

        .container {
            margin-left: 1.5cm;
            margin-top: 0cm;
            margin-right: 1.5cm;
            margin-bottom: 1.5cm;
        }

        td {
            vertical-align: top;
        }

        .point {
            width: 1cm;
        }

        .justify {
            text-align: justify;
        }

        .page_break {
            page-break-before: always;
        }

        .text {
            font-size: 11pt;
            line-height: 25px;
        }

    </style>
</head>

<body>

    <div class="container">
        <center>
            <table>
                <tr>
                    <td>
                        <img src="{{ public_path('image/icon/main_icon.png') }}" style="width:2.5cm;">
                    </td>
                    <td>
                        <center style="margin-top: -10px">
                            <p style="font-weight: bold; font-size: 14pt">
                                KOMISI PEMILIHAN UMUM MAHASISWA POLITEKNIK NEGERI MADIUN
                            </p>
                            <p style="font-size: 9pt; margin-top: -15px"> Jalan Serayu No. 84 Madiun Telp. (0351) 452970
                                Fax. (0351)
                                492960
                                E-mail: sekretariat.kpumpnm@gmail.com</p>
                        </center>
                    </td>
                    <td>
                        <img src="{{ public_path('image/icon/main_icon.png') }}" style="width:2.5cm;">
                    </td>
                </tr>
            </table>
            <hr>
        </center>
        <br />
        <center style="margin-top: -20px">
            <p style="font-weight: bold; font-size: 11pt;margin-top: -10px">
                BERITA ACARA
            </p>
            <p style="font-weight: bold; font-size: 11pt;margin-top: -10px">
                PEMILIHAN RAYA ANGGOTA
            </p>
            <p style="font-weight: bold; font-size: 11pt;margin-top: -10px">
                DEWAN PERWAKILAN MAHASISWA – KELUARGA MAHASISWA
            </p>
            <p style="font-weight: bold; font-size: 11pt;margin-top: -10px">
                POLITEKNIK NEGERI MADIUN
            </p>
            <p style="font-weight: bold; font-size: 11pt;margin-top: -10px">
                PERIODE {{ $setup->next_periode }}
            </p>
        </center>

        <p class="justify mt-05" style="text-indent: 0.5in; font-size: 11pt; line-height: 25px">
            Pada hari {{ $setup->now_text }} ({{ $setup->date }}) telah dilaksanakan Pemilihan dan Penghitungan
            Suara Calon Anggota Dewan Perwakilan Mahasiswa – Keluarga Mahasiswa (DPM-KM) Periode
            {{ $setup->next_periode }}, di {{ $request->place }}
            dengan
            rincian sebagai berikut:
        </p>
        <p class="text" style="margin-top: -10px">
            Daftar nama calon anggota DPM-KM Periode {{ $setup->next_periode }} per jurusan:
        </p>
        <p style="margin-top: 1mm">A. Jurusan Administrasi Bisnis</p>
        <table style="margin-top: -0.3cm; margin-left: 5mm">
            @foreach ($dpm->adbis as $data)
                <tr>
                    <td style="width: 0.5cm">{{ $loop->iteration }}. </td>
                    <td class="justify">{{ $data->name }} ({{ $data->prodi->name }})
                    </td>
                </tr>
            @endforeach
        </table>
        <p style="margin-top: 1mm">B. Jurusan Komputerisasi Akuntansi</p>
        <table style="margin-top: -0.3cm; margin-left: 5mm">
            @foreach ($dpm->kompak as $data)
                <tr>
                    <td style="width: 0.5cm">{{ $loop->iteration }}. </td>
                    <td class="justify">{{ $data->name }} ({{ $data->prodi->name }})
                    </td>
                </tr>
            @endforeach
        </table>
        <p style="margin-top: 1mm">C. Jurusan Teknik</p>
        <table style="margin-top: -0.3cm; margin-left: 5mm">
            @foreach ($dpm->teknik as $data)
                <tr>
                    <td style="width: 0.5cm">{{ $loop->iteration }}. </td>
                    <td class="justify">{{ $data->name }} ({{ $data->prodi->name }})
                    </td>
                </tr>
            @endforeach
        </table>
        <p class="text" style="margin-top: 5px">
            Dengan hasil penghitungan:
        </p>
        <p style="margin-top: 1mm">A. Jurusan Administrasi Bisnis</p>
        <table style="margin-top: -0.3cm; margin-left: 5mm">
            @foreach ($dpm->adbis as $data)
                <tr>
                    <td style="width: 0.5cm">{{ $loop->iteration }}. </td>
                    <td class="justify">{{ $data->name }} ({{ $data->prodi->name }}), sebanyak
                        {{ $data->votes }} suara sah
                    </td>
                </tr>
            @endforeach
        </table>
        <p style="margin-top: 1mm">B. Jurusan Komputerisasi Akuntansi</p>
        <table style="margin-top: -0.3cm; margin-left: 5mm">
            @foreach ($dpm->kompak as $data)
                <tr>
                    <td style="width: 0.5cm">{{ $loop->iteration }}. </td>
                    <td class="justify">{{ $data->name }} ({{ $data->prodi->name }}), sebanyak
                        {{ $data->votes }} suara sah
                    </td>
                </tr>
            @endforeach
        </table>
        <p style="margin-top: 1mm">C. Jurusan Teknik</p>
        <table style="margin-top: -0.3cm; margin-left: 5mm">
            @foreach ($dpm->teknik as $data)
                <tr>
                    <td style="width: 0.5cm">{{ $loop->iteration }}. </td>
                    <td class="justify">{{ $data->name }} ({{ $data->prodi->name }}), sebanyak
                        {{ $data->votes }} suara sah
                    </td>
                </tr>
            @endforeach
        </table>
        <p class="text" style="margin-left: 65px; margin-top: 5px">
            Mahasiswa Penuh = {{ $setup->voter }}
        </p>
        <p class="text" style="margin-left: 65px; margin-top: -20px">
            Total Pemilih = {{ $dpm->total }}
        </p>
        <p class="text" style="margin-left: 65px; margin-top: -20px">
            Cat : Suara tidak sah (golput) = {{ $setup->voter - $dpm->total }}
        </p>
        <p class="text" style="margin-left: 65px; margin-top: -20px">
            Demikian, surat ini kami buat dengan sebenarnya untuk dipergunakan seperlunya.
        </p>
        <div style="display: flex ">
            <div style="width: 7cm; margin-left: 8.6cm">
                <center class="text">
                    <p>
                        Hormat Kami,
                        <br>
                        Ketua KPUM
                        <br>
                        <br>
                        <br>
                        <br>
                        <span style="font-weight: bold;"><u>{{ $setup->name }}</u></span>
                        <br>
                        <span style="font-weight: bold;">NIM. {{ $setup->npm }}</span>
                    </p>
                </center>
            </div>
        </div>
    </div>

</body>

</html>
