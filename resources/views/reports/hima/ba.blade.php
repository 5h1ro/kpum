<!DOCTYPE html>
<html>

<head>
    <title>Surat Keterangan</title>
    <style>
        .mt-05 {
            margin-top: 0.5cm;
        }

        .mt-1 {
            margin-top: 1cm;
        }

        .mt-2 {
            margin-top: 2cm;
        }

        .mt-5 {
            margin-top: 5cm;
        }

        .container {
            margin-left: 1.5cm;
            margin-top: 0cm;
            margin-right: 1.5cm;
            margin-bottom: 1.5cm;
        }

        td {
            vertical-align: top;
        }

        .point {
            width: 1cm;
        }

        .justify {
            text-align: justify;
        }

        .page_break {
            page-break-before: always;
        }

        .text {
            font-size: 11pt;
            line-height: 25px;
        }

    </style>
</head>

<body>

    <div class="container">
        <center>
            <table>
                <tr>
                    <td>
                        <img src="{{ public_path('image/icon/main_icon.png') }}" style="width:2.5cm;">
                    </td>
                    <td>
                        <center style="margin-top: -10px">
                            <p style="font-weight: bold; font-size: 14pt">
                                KOMISI PEMILIHAN UMUM MAHASISWA POLITEKNIK NEGERI MADIUN
                            </p>
                            <p style="font-size: 9pt; margin-top: -15px"> Jalan Serayu No. 84 Madiun Telp. (0351) 452970
                                Fax. (0351)
                                492960
                                E-mail: sekretariat.kpumpnm@gmail.com</p>
                        </center>
                    </td>
                    <td>
                        <img src="{{ public_path('image/icon/main_icon.png') }}" style="width:2.5cm;">
                    </td>
                </tr>
            </table>
            <hr>
        </center>
        <br />
        <center style="margin-top: -20px">
            <p style="font-weight: bold; font-size: 11pt;margin-top: -10px">
                BERITA ACARA
            </p>
            <p style="font-weight: bold; font-size: 11pt;margin-top: -10px">
                PEMILIHAN PRESIDEN DAN WAKIL PRESIDEN
            </p>
            <p style="font-weight: bold; font-size: 11pt;margin-top: -10px">
                HIMPUNAN MAHASISWA PROGRAM STUDI {{ strtoupper($prodi->name) }}
            </p>
            <p style="font-weight: bold; font-size: 11pt;margin-top: -10px">
                POLITEKNIK NEGERI MADIUN
            </p>
            <p style="font-weight: bold; font-size: 11pt;margin-top: -10px">
                PERIODE {{ $setup->next_periode }}
            </p>
        </center>

        <p class="justify mt-05" style="text-indent: 0.5in; font-size: 11pt; line-height: 25px">
            Pada hari {{ $setup->now_text }} ({{ $setup->date }}) telah
            dilaksanakan Pemilihan dan Penghitungan Suara Calon Ketua Umum Himpunan Mahasiswa Program Studi
            {{ ucwords($prodi->name) }} Periode {{ $setup->next_periode }}, di {{ $request->place }}
            dengan
            rincian sebagai berikut:
        </p>
        <p class="text" style="margin-top: -10px">
            Nama Kandidat Ketua Umum Himpunan Mahasiswa Program Studi {{ ucwords($prodi->name) }} Periode
            {{ $setup->next_periode }} :
        </p>
        <table style="margin-left: 20px; margin-top: -20px">
            @foreach ($hima as $data)
                <tr>
                    <td class="point text">
                        {{ $loop->iteration }}.
                    </td>
                    <td class="text justify">
                        {{ $data->name }}
                    </td>
                </tr>
            @endforeach
        </table>
        <p class="text" style="margin-top: 0px">
            Dengan hasil penghitungan:
        </p>
        <table style="margin-left: 20px; margin-top: -15px">
            @foreach ($hima as $item)
                <tr>
                    <td class="point text">
                        {{ $loop->iteration }}.
                    </td>
                    <td class="text justify">
                        Calon Ketua Umum Himpunan Mahasiswa Program Studi {{ ucwords($prodi->name) }} No.
                        {{ $loop->iteration }} {{ $item->name }} memperoleh
                        {{ $item->votes == 0 ? 0 : $item->votes }}
                        suara sah.
                    </td>
                </tr>
            @endforeach
        </table>
        <p class="text" style="margin-left: 65px; margin-top: 5px">
            Mahasiswa Penuh = {{ $setup->voter }}
        </p>
        <p class="text" style="margin-left: 65px; margin-top: -20px">
            Total Pemilih = {{ $hima->total }}
        </p>
        <p class="text" style="margin-left: 65px; margin-top: -20px">
            Cat : Suara tidak sah (golput) = {{ $setup->voter - $hima->total }}
        </p>
        <p class="text" style="margin-left: 65px; margin-top: -20px">
            Demikian, surat ini kami buat dengan sebenarnya untuk dipergunakan seperlunya.
        </p>
        <div style="display: flex ">
            <div style="width: 7cm; margin-left: 8.6cm">
                <center class="text">
                    <p>
                        Hormat Kami,
                        <br>
                        Ketua KPUM
                        <br>
                        <br>
                        <br>
                        <br>
                        <span style="font-weight: bold;"><u>{{ $setup->name }}</u></span>
                        <br>
                        <span style="font-weight: bold;">NIM. {{ $setup->npm }}</span>
                    </p>
                </center>
            </div>
        </div>
    </div>

</body>

</html>
