<!DOCTYPE html>
<html>

<head>
    <title>Surat Keterangan</title>
    <style>
        .mt-n03 {
            margin-top: -0.3cm;
        }

        .mt-05 {
            margin-top: 0.5cm;
        }

        .mt-1 {
            margin-top: 1cm;
        }

        .mt-2 {
            margin-top: 2cm;
        }

        .mt-5 {
            margin-top: 5cm;
        }

        .container {
            margin-left: 1.5cm;
            margin-top: 1cm;
            margin-right: 1.5cm;
            margin-bottom: 1.5cm;
        }

        td {
            vertical-align: top;
        }

        .point {
            width: 1cm;
        }

        .justify {
            text-align: justify;
        }

        .page_break {
            page-break-before: always;
        }

    </style>
</head>

<body>

    <div class="container">
        <center>
            <img src="{{ public_path('image/icon/main_icon.png') }}" style="width:2.5cm;">
            <p style="font-weight: bold">
                KEPUTUSAN
                <br>
                KOMISI PEMILIHAN UMUM MAHASISWA
                <br>
                POLITEKNIK NEGERI MADIUN
                <br>
                PERIODE {{ $setup->current_periode }}
                <hr class="mt-n03">
                <span class="mt-n03" style="font-weight: bold">
                    Nomor : {{ $setup->no }}
                </span>
            </p>
            <div class="mt-1">
                <p>
                    TENTANG :
                    <br>
                    <span style="font-weight: bold">
                        PENETAPAN REKAPITULASI HASIL PENGHITUNGAN SUARA PEMILIHAN
                        <br>
                        KETUA UMUM HIMPUNAN MAHASISWA JURUSAN {{ strtoupper($jurusan->name) }}
                        <br>
                        POLITEKNIK NEGERI MADIUN
                        <br>
                        PERIODE {{ $setup->next_periode }}
                    </span>
                </p>
            </div>
            <div class="mt-05">
                <p>
                    KETUA KOMISI PEMILIHAN UMUM MAHASISWA,
                </p>
            </div>
        </center>
        <br />
        <table>
            <tr>
                <td>Menimbang</td>
                <td>:</td>
                <td class="point">a.</td>
                <td class="justify">bahwa Pemilihan Raya Organisasi Kemahasiswaan Politeknik Negeri Madiun adalah
                    ajang regenerasi dan
                    suksesi kepemimpinan berlandaskan demokrasi yang menjunjung tinggi azas-azas Pemilihan Umum;
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td class="point">b.</td>
                <td class="justify">bahwa Komisi Pemilihan Umum Mahasiswa memiliki tugas, wewenang dan kewajiban
                    dalam menetapkan Calon Ketua Umum Himpunan Mahasiswa Jurusan {{ ucwords($jurusan->name) }}
                    berdasarkan
                    verifikasi;
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td class="point">c.</td>
                <td class="justify">bahwa berdasarkan pertimbangan sebagaimana dimaksud dalam huruf a dan huruf
                    b,
                    maka perlu menetapkan Keputusan Komisi Pemilihan Umum Mahasiswa tentang Penetapan Calon Ketua Umum
                    Himpunan Mahasiswa Jurusan {{ ucwords($jurusan->name) }} Politeknik Negeri Madiun
                    Periode {{ $setup->next_periode }}.
                </td>
            </tr>
            <tr>
                <td style="height: 0.2cm;"></td>
            </tr>
            <tr>
                <td>Mengingat</td>
                <td>:</td>
                <td class="point"></td>
                <td class="justify">Pasal 10 Undang-Undang Pemilihan Raya Organisasi Kemahasiswaan Politeknik
                    Negeri Madiun.
                </td>
            </tr>
            <tr>
                <td style="height: 0.2cm;"></td>
            </tr>
            <tr>
                <td>Memerhatikan</td>
                <td>:</td>
                <td class="point">a.</td>
                <td class="justify">Keputusan KPUM No.{{ $report->no }} tentang Penetapan Calon
                    Ketua Umum Himpunan Mahasiswa Jurusan {{ ucwords($jurusan->name) }} Politeknik Negeri Madiun
                    Periode
                    {{ $setup->next_periode }}.
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td class="point">b.</td>
                <td class="justify">Berita Acara Rekapitulasi Hasil Penghitungan Suara di Tingkat Kampus dalam
                    Pemilihan Raya Calon Ketua Umum Himpunan Mahasiswa Jurusan {{ ucwords($jurusan->name) }}
                    Politeknik Negeri Madiun Periode {{ $setup->next_periode }}.
                </td>
            </tr>
        </table>

        <div class="page_break"></div>
        <center>
            <p>Dengan senantiasa memohon petunjuk dari Tuhan Yang Maha Kuasa,
                <br>
            </p>
            <p style="font-weight: bold; letter-spacing: 0.1cm; margin-top: -0.2cm">
                MEMUTUSKAN
            </p>
        </center>
        <div class="mt-1">
            <table>
                <tr>
                    <td>Menetapkan </td>
                    <td>:</td>
                    <td class="point"></td>
                    <td class="justify">PENETAPAN REKAPITULASI HASIL PENGHITUNGAN SUARA DAN HASIL PEMILIHAN RAYA
                        KETUA UMUM HIMPUNAN MAHASISWA JURUSAN {{ strtoupper($jurusan->name) }} POLITEKNIK NEGERI
                        MADIUN
                        PERIODE
                        {{ $setup->current_periode }};
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>PERTAMA</td>
                    <td>:</td>
                    <td class="point"></td>
                    <td class="justify">Menetapkan Rekapitulasi Hasil Penghitungan Suara dan Hasil Pemilihan Raya
                        Ketua Umum Himpunan Mahasiswa Jurusan {{ ucwords($jurusan->name) }} Politeknik Negeri
                        Madiun Periode
                        {{ $setup->next_periode }}
                        sebagaimana tercantum dalam Lampiran Keputusan ini yang merupakan bagian yang tidak terpisahkan
                        dari Keputusan ini.
                    </td>
                </tr>
                <tr>
                    <td style="height: 0.1cm;"></td>
                </tr>
                <tr>
                    <td>KEDUA</td>
                    <td>:</td>
                    <td class="point"></td>
                    <td class="justify">Keputusan KPUM No.{{ $report->no }} tentang Penetapan Calon Ketua Umum
                        Himpunan Mahasiswa Jurusan {{ ucwords($jurusan->name) }} Politeknik Negeri Madiun Periode
                        {{ $setup->next_periode }}.
                    </td>
                </tr>
            </table>
            <table>
                @foreach ($hmj as $item)
                    <tr>
                        <td style="width: 3.4cm"></td>
                        <td style="width: 0.5cm">
                            @switch($loop->iteration)
                                @case(1)
                                    a
                                @break

                                @case(2)
                                    b
                                @break

                                @case(3)
                                    c
                                @break

                                @case(4)
                                    d
                                @break

                                @case(5)
                                    e
                                @break

                                @case(6)
                                    f
                                @break
                            @endswitch.</td>
                        <td>Calon Ketua Umum Himpunan Mahasiswa Jurusan {{ ucwords($jurusan->name) }} Nomor urut
                            {{ $loop->iteration }}
                            {{ $item->name }} memperoleh {{ $item->votes == 0 ? 0 : $item->votes }} suara sah;
                        </td>
                    </tr>
                @endforeach
            </table>
            <table>
                <tr>
                    <td>KETIGA</td>
                    <td>:</td>
                    <td class="point"></td>
                    <td class="justify">Hasil dari keputusan ini selanjutnya ditindak lanjuti oleh yang
                        bersangkutan mengikuti petunjuk dari Komisi Pemilihan Umum Mahasiswa.
                    </td>
                </tr>
                <tr>
                    <td>KEEMPAT</td>
                    <td>:</td>
                    <td class="point"></td>
                    <td class="justify">Keputusan ini berlaku sejak tanggal ditetapkan.
                    </td>
                </tr>
            </table>
            <div>
                <p>Salinan Keputusan ini disampaikan kepada :</p>
            </div>
            <table class="mt-n03">
                <tr>
                    <td>1.</td>
                    <td style="width: 0.2cm"></td>
                    <td class="justify">Ketua Dewan Perwakilan Mahasiswa – KM Periode
                        {{ $setup->current_periode }};
                    </td>
                </tr>
                <tr>
                    <td>2.</td>
                    <td style="width: 0.2cm"></td>
                    <td class="justify">Presiden Badan Eksekutif Mahasiswa – KM Periode
                        {{ $setup->current_periode }};
                    </td>
                </tr>
                <tr>
                    <td>3.</td>
                    <td style="width: 0.2cm"></td>
                    <td class="justify">Ketua Umum Himpunan Mahasiswa Jurusan {{ ucwords($jurusan->name) }}
                        Periode
                        {{ $setup->current_periode }};
                    </td>
                </tr>
            </table>
            <div>
                <table>
                    <tr>
                        <td style="height: 0.1cm;"></td>
                    </tr>
                    <tr>
                        <td>Ditetapkan di</td>
                        <td>:</td>
                        <td style="width: 0.5cm"></td>
                        <td class="justify">{{ $request->place }}
                        </td>
                    </tr>
                    <tr>
                        <td>Pada tanggal</td>
                        <td>:</td>
                        <td style="width: 0.5cm"></td>
                        <td class="justify">{{ $setup->now }}
                        </td>
                    </tr>
                </table>
            </div>
            <div style="display: flex ">
                <div style="width: 7cm; margin-left: 8.6cm">
                    <center>
                        <p>
                            Hormat Kami,
                            <br>
                            Ketua KPUM
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <span style="font-weight: bold;"><u>{{ $setup->name }}</u></span>
                            <br>
                            <span style="font-weight: bold;">NIM. {{ $setup->npm }}</span>
                        </p>
                    </center>
                </div>
            </div>
        </div>
    </div>

</body>

</html>
