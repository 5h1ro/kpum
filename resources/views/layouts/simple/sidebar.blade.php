<div class="sidebar-wrapper">
    <div>
        <div class="logo-wrapper">
            <a href="#"><img class="img-fluid for-light" src="{{ asset('assets/images/logo/logo.png') }}" alt=""><img
                    class="img-fluid for-dark" src="{{ asset('assets/images/logo/logo_dark.png') }}" alt=""></a>
            <div class="back-btn"><i class="fa fa-angle-left"></i></div>
            <div class="toggle-sidebar"><i class="status_toggle middle sidebar-toggle" data-feather="grid"> </i></div>
        </div>
        <div class="logo-icon-wrapper"><a href="#"><img class="img-fluid"
                    src="{{ asset('assets/images/logo/logo-icon.png') }}" alt=""></a></div>
        <nav class="sidebar-main">
            <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
            <div id="sidebar-menu">
                <ul class="sidebar-links" id="simple-bar">
                    <li class="back-btn">
                        <a href="#"><img class="img-fluid" src="{{ asset('assets/images/logo/logo-icon.png') }}"
                                alt=""></a>
                        <div class="mobile-back text-end"><span>Back</span><i class="fa fa-angle-right ps-2"
                                aria-hidden="true"></i></div>
                    </li>
                    <li class="sidebar-main-title">
                        <div>
                            <h6>KPUM PNM</h6>
                            <p>Web Pemilihan Raya PNM</p>
                        </div>
                    </li>
                    {{-- admin --}}
                    @if ($user->role->name == 'Admin')
                        <li class="sidebar-list">
                            <a href="{{ route('home') }}" class="sidebar-link sidebar-title link-nav">
                                <i data-feather="home"></i><span>Dashboard</span>
                            </a>
                        </li>
                        {{-- user --}}
                    @elseif ($user->role->name == 'Voter')
                        <li class="sidebar-list">
                            <a href="{{ route('home') }}" class="sidebar-link sidebar-title link-nav">
                                <i data-feather="home"></i><span>Dashboard</span>
                            </a>
                        </li>
                    @endif
                    @if ($user->role->permission != null)
                        @if (strpos('a' . implode(', ', $user->role->permission), 'role'))
                            <li class="sidebar-list">
                                <a class="sidebar-link sidebar-title link-nav " href="{{ route('sa-role') }}">
                                    <i data-feather="user"></i><span>Data Role</span>
                                </a>
                            </li>
                        @endif
                        @if (strpos('a' . implode(', ', $user->role->permission), 'candidate'))
                            <li class="sidebar-list">
                                <a class="sidebar-link sidebar-title link-nav"
                                    href="{{ $user->role->name === 'Super Admin' ? route('sa-data-calon') : route('data-calon') }}">
                                    <i data-feather="server"></i><span>Data Calon</span>
                                </a>
                            </li>
                        @endif
                        @if (strpos('a' . implode(', ', $user->role->permission), 'schedule'))
                            <li class="sidebar-list">
                                <a class="sidebar-link sidebar-title link-nav "
                                    href="{{ $user->role->name === 'Super Admin' ? route('sa-data-schedule') : route('data-schedule') }}">
                                    <i data-feather="clock"></i><span>Data Jadwal</span>
                                </a>
                            </li>
                        @endif
                        @if (strpos('a' . implode(', ', $user->role->permission), 'pendaftar'))
                            <li class="sidebar-list">
                                <a class="sidebar-link sidebar-title link-nav "
                                    href="{{ $user->role->name === 'Super Admin' ? route('sa-data-candidate') : route('admin-data-candidate') }}">
                                    <i data-feather="users"></i><span>Data Pendaftar</span>
                                </a>
                            </li>
                        @endif
                        @if (strpos('a' . implode(', ', $user->role->permission), 'voter'))
                            <li class="sidebar-list">
                                <a class="sidebar-link sidebar-title link-nav "
                                    href="{{ $user->role->name === 'Super Admin' ? route('sa-data-user') : route('data-user') }}">
                                    <i data-feather="user"></i><span>Data User</span>
                                </a>
                            </li>
                        @endif
                        @if (strpos('a' . implode(', ', $user->role->permission), 'choice'))
                            <li class="sidebar-list">
                                <a class="sidebar-link sidebar-title link-nav"
                                    href="{{ $user->role->name === 'Super Admin' ? route('sa-pemilihan') : route('pemilihan') }}">
                                    <i data-feather="box"></i><span>Pemilihan</span>
                                </a>
                            </li>
                        @endif
                        @if (strpos('a' . implode(', ', $user->role->permission), 'report'))
                            <li class="sidebar-list">
                                <a class="sidebar-link sidebar-title link-nav"
                                    href="{{ $user->role->name === 'Super Admin' ? route('sa-data-report') : route('data-report') }}">
                                    <i data-feather="file-text"></i><span>Laporan</span>
                                </a>
                            </li>
                        @endif
                    @endif
                </ul>
            </div>
            <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
        </nav>
    </div>
</div>
