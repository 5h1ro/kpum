@extends('layouts.authentication.master')
@section('title', 'Login-one')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendors/sweetalert2.css') }}">
@endsection

@section('style')
@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="login-card">
                    <div class="card-body row justify-content-center">
                        <div class="container-fluid">
                            <div class="row justify-content-center">
                                <div class="col-12 col-xl-3">
                                    <a href="{{ route('register-candidate-bem') }}">
                                        <div class="card custom-card">
                                            <div class="card-header"><img class="img-fluid"
                                                    src="{{ asset('assets/images/user-card/1.jpg') }}" alt=""></div>
                                            <div class="card-profile"><img class="rounded-circle"
                                                    src="{{ asset('assets/images/avtar/bem.jpg') }}" alt=""></div>
                                            <div class="text-center profile-details">
                                                <h4>BEM</h4>
                                                <h6>Badan Eksekutif Mahasiswa</h6>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-12 col-xl-3">
                                    <a href="{{ route('register-candidate-dpm') }}">
                                        <div class="card custom-card">
                                            <div class="card-header"><img class="img-fluid"
                                                    src="{{ asset('assets/images/user-card/2.jpg') }}" alt=""></div>
                                            <div class="card-profile"><img class="rounded-circle"
                                                    src="{{ asset('assets/images/avtar/dpm.jpg') }}" alt=""></div>
                                            <div class="text-center profile-details">
                                                <h4>DPM</h4>
                                                <h6>Dewan Perwakilan Mahasiswa</h6>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="container-fluid">
                            <div class="row justify-content-center">
                                <div class="col-12 col-xl-3">
                                    <a href="{{ route('register-candidate-hmj') }}">
                                        <div class="card custom-card">
                                            <div class="card-header"><img class="img-fluid"
                                                    src="{{ asset('assets/images/user-card/1.jpg') }}" alt=""></div>
                                            <div class="card-profile"><img class="rounded-circle"
                                                    src="{{ asset('assets/images/avtar/hmj.jpg') }}" alt=""></div>
                                            <div class="text-center profile-details">
                                                <h4>HMJ</h4>
                                                <h6>Himpunan Mahasiswa Jurusan</h6>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-12 col-xl-3">
                                    <a href="{{ route('register-candidate-hima') }}">
                                        <div class="card custom-card">
                                            <div class="card-header"><img class="img-fluid"
                                                    src="{{ asset('assets/images/user-card/2.jpg') }}" alt=""></div>
                                            <div class="card-profile"><img class="rounded-circle"
                                                    src="{{ asset('assets/images/avtar/hima.jpg') }}" alt=""></div>
                                            <div class="text-center profile-details">
                                                <h4>HIMA</h4>
                                                <h6>Himpunan Mahasiswa Program Studi</h6>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/js/form-wizard/form-wizard-three.js') }}"></script>
    <script src="{{ asset('assets/js/form-wizard/jquery.backstretch.min.js') }}"></script>
    <script src="{{ asset('assets/js/sweet-alert/sweetalert.min.js') }}"></script>
    @if ($errors->any())
        <script src="{{ asset('assets/js/sweet-alert/login.js') }}"></script>
    @endif
    @if (Session::has('Success'))
        <script src="{{ asset('assets/js/sweet-alert/register_success.js') }}"></script>
    @endif
    @if (Session::has('Error'))
        <script src="{{ asset('assets/js/sweet-alert/register_error.js') }}"></script>
    @endif
    @if (Session::has('NotAuth'))
        <script src="{{ asset('assets/js/sweet-alert/no_auth.js') }}"></script>
    @endif
    <script src="{{ asset('assets/js/register/bem.js') }}"></script>
@endsection
