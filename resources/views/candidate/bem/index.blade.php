@extends('layouts.authentication.master')
@section('title', 'Login-one')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendors/sweetalert2.css') }}">
@endsection

@section('style')
@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="login-card">
                    <div class="card-body row justify-content-center">
                        <div class="col-12 col-lg-6 card p-4 p-lg-5">
                            <h2>Daftar Calon Presiden BEM</h2>
                            <form action="{{ route('register-candidate-bem-post') }}" class="f1" method="POST"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="f1-steps">
                                    <div class="f1-progress">
                                        <div class="f1-progress-line" data-now-value="16.66" data-number-of-steps="3"></div>
                                    </div>
                                    <div class="f1-step active">
                                        <div class="f1-step-icon"><i class="fa fa-user"></i></div>
                                        <p>Data Diri Presiden BEM</p>
                                    </div>
                                    <div class="f1-step">
                                        <div class="f1-step-icon"><i class="fa fa-user"></i></div>
                                        <p>Data Diri Wakil Presiden BEM</p>
                                    </div>
                                    <div class="f1-step">
                                        <div class="f1-step-icon"><i class="fa fa-file"></i></div>
                                        <p>Data Lainnya</p>
                                    </div>
                                </div>
                                <fieldset>
                                    <div class="row ">
                                        <div class="mb-3 mb-2 col-12 col-lg-8">
                                            <label for="fullname1">Nama Lengkap</label>
                                            <input class="form-control" id="fullname1" type="text" name="fullname1"
                                                placeholder="Alex Andreas" required="">
                                        </div>
                                        <div class="mb-3 mb-2 col-12 col-lg-4">
                                            <label for="name1">Nama Panggilan</label>
                                            <input class="form-control" id="name1" type="text" name="name1"
                                                placeholder="Alex" required="">
                                        </div>
                                    </div>
                                    <div class="row mb-3 mb-2">
                                        <div class="col-12">
                                            <label for="npm1">NPM</label>
                                            <input class="form-control" id="npm1" type="number" name="npm1"
                                                placeholder="192200001" required="">
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="mb-3 mb-2 col-12 col-lg-6">
                                            <label for="birthplace1">Tempat Lahir</label>
                                            <input class="birthplace form-control" id="birthplace1" type="text"
                                                name="birthplace1" placeholder="Madiun" required="">
                                        </div>
                                        <div class="mb-3 mb-2 col-12 col-lg-6">
                                            <label for="birthday1">Tanggal Lahir</label>
                                            <input class="form-control" id="birthday1" type="date" name="birthday1"
                                                required>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="mb-3 mb-2 col-12 col-lg-4">
                                            <label for="gender1">Jenis Kelamin</label>
                                            <select class="form-control form-select" id="gender1" name="gender1" required>
                                                <option value="Laki-laki">Laki-laki</option>
                                                <option value="Perempuan">Perempuan</option>
                                            </select>
                                        </div>
                                        <div class="mb-3 mb-2 col-12 col-lg-8">
                                            <label for="email1">Email</label>
                                            <input class="form-control" id="email1" type="email" name="email1"
                                                placeholder="xyz@email.com" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-lg-12 mb-3 mb-2">
                                            <label for="id_jurusan1">Jurusan</label>
                                            <select class="form-control form-select" id="id_jurusan1" name="id_jurusan1"
                                                required>
                                                @foreach ($jurusan as $jurusans)
                                                    <option value="{{ $jurusans->id }}">{{ $jurusans->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-12 col-lg-7 mb-3 mb-2">
                                            <label for="id_prodi1">Program Studi</label>
                                            <select class="form-control form-select" id="id_prodi1" name="id_prodi1"
                                                required>
                                                @foreach ($prodi as $prodis)
                                                    <option value="{{ $prodis->id }}">{{ $prodis->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-12 col-lg-5 mb-3 mb-2">
                                            <label for="class1">Semester/Kelas</label>
                                            <input class="form-control" id="class1" type="text" name="class1"
                                                placeholder="3/A" required="">
                                        </div>
                                    </div>
                                    <div class="f1-buttons">
                                        <button class="btn btn-primary btn-next" type="button">Next</button>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <div class="row ">
                                        <div class="mb-3 mb-2 col-12 col-lg-8">
                                            <label for="fullname2">Nama Lengkap</label>
                                            <input class="form-control" id="fullname2" type="text" name="fullname2"
                                                placeholder="Alex Andreas" required="">
                                        </div>
                                        <div class="mb-3 mb-2 col-12 col-lg-4">
                                            <label for="name2">Nama Panggilan</label>
                                            <input class="form-control" id="name2" type="text" name="name2"
                                                placeholder="Alex" required="">
                                        </div>
                                    </div>
                                    <div class="row mb-3 mb-2">
                                        <div class="col-12">
                                            <label for="npm2">NPM</label>
                                            <input class="form-control" id="npm2" type="number" name="npm2"
                                                placeholder="192200001" required="">
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="mb-3 mb-2 col-12 col-lg-6">
                                            <label for="birthplace2">Tempat Lahir</label>
                                            <input class="birthplace form-control" id="birthplace2" type="text"
                                                name="birthplace2" placeholder="Madiun" required="">
                                        </div>
                                        <div class="mb-3 mb-2 col-12 col-lg-6">
                                            <label for="birthday2">Tanggal Lahir</label>
                                            <input class="form-control" id="birthday2" type="date" name="birthday2"
                                                required>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="mb-3 mb-2 col-12 col-lg-4">
                                            <label for="gender2">Jenis Kelamin</label>
                                            <select class="form-control form-select" id="gender2" name="gender2" required>
                                                <option value="Laki-laki">Laki-laki</option>
                                                <option value="Perempuan">Perempuan</option>
                                            </select>
                                        </div>
                                        <div class="mb-3 mb-2 col-12 col-lg-8">
                                            <label for="email2">Email</label>
                                            <input class="form-control" id="email2" type="email" name="email2"
                                                placeholder="xyz@email.com" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-lg-12 mb-3 mb-2">
                                            <label for="id_jurusan2">Jurusan</label>
                                            <select class="form-control form-select" id="id_jurusan2" name="id_jurusan2"
                                                required>
                                                @foreach ($jurusan as $jurusans)
                                                    <option value="{{ $jurusans->id }}">{{ $jurusans->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-12 col-lg-7 mb-3 mb-2">
                                            <label for="id_prodi2">Program Studi</label>
                                            <select class="form-control form-select" id="id_prodi2" name="id_prodi2"
                                                required>
                                                @foreach ($prodi as $prodis)
                                                    <option value="{{ $prodis->id }}">{{ $prodis->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-12 col-lg-5 mb-3 mb-2">
                                            <label for="class2">Semester/Kelas</label>
                                            <input class="form-control" id="class2" type="text" name="class2"
                                                placeholder="3/A" required="">
                                        </div>
                                    </div>
                                    <div class="f1-buttons">
                                        <button class="btn btn-primary btn-previous" type="button">Previous</button>
                                        <button class="btn btn-primary btn-next" type="button">Next</button>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <div class="row" id="form-visi">
                                        <div class="col-12 mb-3 mb-2 align-self-center">
                                            <label for="visi">Visi</label>
                                            <textarea class="form-control" id="visi" name="visi" required=""></textarea>
                                        </div>
                                    </div>
                                    <div class="row" id="form-misi">
                                        <div class="col-9 col-lg-11 mb-3 mb-2 align-self-center">
                                            <label for="misi1">Misi</label>
                                            <textarea class="form-control" id="misi1" name="misi1" required=""></textarea>
                                        </div>
                                        <div class="col-3 col-lg-1 mb-3 mb-2 align-self-center">
                                            <label for="btn-add-1" style="color: rgba(0, 0, 0, 0);">Add</label>
                                            <button class="btn-lg btn-outline-success shadow-none" id="btn-add-1"
                                                onclick="add(1)" name="btn-add-1">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="mb-3 mb-2">
                                        <label class="col-form-label" for="formulir">Formulir</label>
                                        <input class="form-control" id="formulir" name="formulir" type="file">
                                    </div>
                                    <div class="mb-3 mb-2">
                                        <label class="col-form-label" for="persyaratan">Persyaratan</label>
                                        <input class="form-control" id="persyaratan" name="persyaratan" type="file">
                                    </div>
                                    <div class="mb-3 mb-2">
                                        <label class="col-form-label" for="photo1">Foto 3x4 Presiden BEM</label>
                                        <input class="form-control" id="photo1" name="photo1" type="file">
                                    </div>
                                    <div class="mb-3 mb-2">
                                        <label class="col-form-label" for="photo2">Foto 3x4 Wakil Presiden BEM</label>
                                        <input class="form-control" id="photo2" name="photo2" type="file">
                                    </div>
                                    <div class="f1-buttons">
                                        <button class="btn btn-primary btn-previous" type="button">Previous</button>
                                        <button class="btn btn-primary btn-submit" type="submit">Submit</button>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/js/form-wizard/form-wizard-three.js') }}"></script>
    <script src="{{ asset('assets/js/form-wizard/jquery.backstretch.min.js') }}"></script>
    <script src="{{ asset('assets/js/sweet-alert/sweetalert.min.js') }}"></script>
    @if ($errors->any())
        <script src="{{ asset('assets/js/sweet-alert/login.js') }}"></script>
    @endif
    @if (Session::has('Success'))
        <script src="{{ asset('assets/js/sweet-alert/register_success.js') }}"></script>
    @endif
    @if (Session::has('Error'))
        <script src="{{ asset('assets/js/sweet-alert/register_error.js') }}"></script>
    @endif
    @if (Session::has('NotAuth'))
        <script src="{{ asset('assets/js/sweet-alert/no_auth.js') }}"></script>
    @endif
    <script src="{{ asset('assets/js/register/bem.js') }}"></script>
@endsection
