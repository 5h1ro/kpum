@extends('layouts.authentication.master')
@section('title', 'Login-one')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendors/sweetalert2.css') }}">
@endsection

@section('style')
@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="login-card">
                    <div class="card-body row justify-content-center">
                        <div class="col-12 col-lg-6 card p-4 p-lg-5">
                            <h2>Daftar Calon Ketua HIMA</h2>
                            <form action="{{ route('register-candidate-hima-post') }}" class="f1" method="POST"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="f1-steps">
                                    <div class="f1-progress">
                                        <div class="f1-progress-line" data-now-value="16.66" data-number-of-steps="3"></div>
                                    </div>
                                    <div class="f1-step active">
                                        <div class="f1-step-icon"><i class="fa fa-user"></i></div>
                                        <p>Data Diri</p>
                                    </div>
                                    <div class="f1-step">
                                        <div class="f1-step-icon"><i class="fa fa-graduation-cap"></i></div>
                                        <p>Akademis</p>
                                    </div>
                                    <div class="f1-step">
                                        <div class="f1-step-icon"><i class="fa fa-file"></i></div>
                                        <p>Berkas</p>
                                    </div>
                                </div>
                                <fieldset>
                                    <div class="row ">
                                        <div class="mb-3 mb-2 col-12 col-lg-8">
                                            <label for="fullname">Nama Lengkap</label>
                                            <input class="form-control" id="fullname" type="text" name="fullname"
                                                placeholder="Alex Andreas" required="">
                                        </div>
                                        <div class="mb-3 mb-2 col-12 col-lg-4">
                                            <label for="name">Nama Panggilan</label>
                                            <input class="form-control" id="name" type="text" name="name"
                                                placeholder="Alex" required="">
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="mb-3 mb-2 col-12 col-lg-6">
                                            <label for="birthplace">Tempat Lahir</label>
                                            <input class="birthplace form-control" id="birthplace" type="text"
                                                name="birthplace" placeholder="Madiun" required="">
                                        </div>
                                        <div class="mb-3 mb-2 col-12 col-lg-6">
                                            <label for="birthday">Tanggal Lahir</label>
                                            <input class="form-control" id="birthday" type="date" name="birthday"
                                                required>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="mb-3 mb-2 col-12 col-lg-4">
                                            <label for="gender">Jenis Kelamin</label>
                                            <select class="form-control form-select" id="gender" name="gender" required>
                                                <option value="Laki-laki">Laki-laki</option>
                                                <option value="Perempuan">Perempuan</option>
                                            </select>
                                        </div>
                                        <div class="mb-3 mb-2 col-12 col-lg-8">
                                            <label for="email">Email</label>
                                            <input class="form-control" id="email" type="email" name="email"
                                                placeholder="xyz@email.com" required>
                                        </div>
                                    </div>
                                    <div class="f1-buttons">
                                        <button class="btn btn-primary btn-next" type="button">Next</button>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <div class="row mb-3 mb-2">
                                        <div class="col-12">
                                            <label for="npm">NPM</label>
                                            <input class="form-control" id="npm" type="number" name="npm"
                                                placeholder="192200001" required="">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-lg-5 mb-3 mb-2">
                                            <label for="id_jurusan">Jurusan</label>
                                            <select class="form-control form-select" id="id_jurusan" name="id_jurusan"
                                                required>
                                                @foreach ($jurusan as $jurusans)
                                                    <option value="{{ $jurusans->id }}">{{ $jurusans->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-12 col-lg-5 mb-3 mb-2">
                                            <label for="id_prodi">Program Studi</label>
                                            <select class="form-control form-select" id="id_prodi" name="id_prodi" required>
                                                @foreach ($prodi as $prodis)
                                                    <option value="{{ $prodis->id }}">{{ $prodis->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-12 col-lg-2 mb-3 mb-2">
                                            <label for="class">Semester/Kelas</label>
                                            <input class="form-control" id="class" type="text" name="class"
                                                placeholder="3/A" required="">
                                        </div>
                                    </div>
                                    <div class="row" id="form-visi">
                                        <div class="col-12 mb-3 mb-2 align-self-center">
                                            <label for="visi">Visi</label>
                                            <textarea class="form-control" id="visi" name="visi" required=""></textarea>
                                        </div>
                                    </div>
                                    <div class="row" id="form-misi">
                                        <div class="col-9 col-lg-11 mb-3 mb-2 align-self-center">
                                            <label for="misi1">Misi</label>
                                            <textarea class="form-control" id="misi1" name="misi1" required=""></textarea>
                                        </div>
                                        <div class="col-3 col-lg-1 mb-3 mb-2 align-self-center">
                                            <label for="btn-add-1" style="color: rgba(0, 0, 0, 0);">Add</label>
                                            <button class="btn-lg btn-outline-success shadow-none" id="btn-add-1"
                                                onclick="add(1)" name="btn-add-1">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="f1-buttons">
                                        <button class="btn btn-primary btn-previous" type="button">Previous</button>
                                        <button class="btn btn-primary btn-next" type="button">Next</button>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <div class="mb-3 mb-2">
                                        <label class="col-form-label" for="formulir">Formulir</label>
                                        <input class="form-control" id="formulir" name="formulir" type="file">
                                    </div>
                                    <div class="mb-3 mb-2">
                                        <label class="col-form-label" for="persyaratan">Persyaratan</label>
                                        <input class="form-control" id="persyaratan" name="persyaratan" type="file">
                                    </div>
                                    <div class="mb-3 mb-2">
                                        <label class="col-form-label" for="photo">Foto 3x4</label>
                                        <input class="form-control" id="photo" name="photo" type="file">
                                    </div>
                                    <div class="f1-buttons">
                                        <button class="btn btn-primary btn-previous" type="button">Previous</button>
                                        <button class="btn btn-primary btn-submit" type="submit">Submit</button>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/js/form-wizard/form-wizard-three.js') }}"></script>
    <script src="{{ asset('assets/js/form-wizard/jquery.backstretch.min.js') }}"></script>
    <script src="{{ asset('assets/js/sweet-alert/sweetalert.min.js') }}"></script>
    @if ($errors->any())
        <script src="{{ asset('assets/js/sweet-alert/login.js') }}"></script>
    @endif
    @if (Session::has('Success'))
        <script src="{{ asset('assets/js/sweet-alert/register_success.js') }}"></script>
    @endif
    @if (Session::has('Error'))
        <script src="{{ asset('assets/js/sweet-alert/register_error.js') }}"></script>
    @endif
    @if (Session::has('NotAuth'))
        <script src="{{ asset('assets/js/sweet-alert/no_auth.js') }}"></script>
    @endif
    <script src="{{ asset('assets/js/register/bem.js') }}"></script>
@endsection
