@extends('layouts.simple.master')
@section('title', 'HTML 5 Data Export')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendors/datatables.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendors/datatable-extension.css') }}">
@endsection

@section('style')
@endsection

@section('breadcrumb-title')
    <h3>Data HMJ</h3>
@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item">Data Calon</li>
    <li class="breadcrumb-item active">Data HMJ</li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Data HMJ</h5>
                    </div>
                    <div class="card-body">
                        <div class="dt-ext table-responsive">
                            <table class="display" id="export-button">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Calon Ketua</th>
                                        <th>Jurusan</th>
                                        <th>Program Studi</th>
                                        <th>Jumlah Suara</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($calon as $data)
                                        <tr>
                                            <td>{{ $data->id }}</td>
                                            <td>{{ $data->name }}</td>
                                            <td>{{ $data->jurusan->name }}</td>
                                            <td>{{ $data->prodi->name }}</td>
                                            <td>{{ $data->vote }}</td>
                                            <td class="col-2">
                                                <button class="btn btn-icon icon-left btn-primary" data-bs-toggle="modal"
                                                    data-bs-target="#modalEdit{{ $loop->iteration }}" type="button">
                                                    <i class="fa fa-edit"></i> Edit</button>
                                            </td>
                                        </tr>
                                        <div class="modal fade bd-example-modal-lg" id="modalEdit{{ $loop->iteration }}"
                                            tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <form action="{{ route('sa-hmj-calon-update', $data->id) }}"
                                                        method="POST">
                                                        @csrf
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Edit Data</h5>
                                                            <button class="btn-close" type="button"
                                                                data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="mb-3">
                                                                    <label class="col-form-label" for="name">Nama Calon
                                                                        Ketua:</label>
                                                                    <input class="form-control" id="name" name="name"
                                                                        type="text" value="{{ $data->name }}">
                                                                    <input class="form-control" id="id" name="id"
                                                                        type="text" value="{{ $data->id }}" hidden>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="mb-3 col-6">
                                                                    <label class="col-form-label" for="id_jurusan">Jurusan
                                                                        Calon Ketua:</label>
                                                                    <select class="form-select digits" id="id_jurusan"
                                                                        name="id_jurusan">
                                                                        @foreach ($jurusan as $data2)
                                                                            @if ($data->id_jurusan == $data2->id)
                                                                                <option value="{{ $data2->id }}"
                                                                                    selected>{{ $data2->name }}</option>
                                                                            @else
                                                                                <option value="{{ $data2->id }}">
                                                                                    {{ $data2->name }}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="mb-3 col-6">
                                                                    <label class="col-form-label" for="id_prodi">Program
                                                                        Studi Calon Ketua:</label>
                                                                    <select class="form-select digits" id="id_prodi"
                                                                        name="id_prodi">
                                                                        @foreach ($prodi as $data2)
                                                                            @if ($data->id_prodi == $data2->id)
                                                                                <option value="{{ $data2->id }}"
                                                                                    selected>{{ $data2->name }}</option>
                                                                            @else
                                                                                <option value="{{ $data2->id }}">
                                                                                    {{ $data2->name }}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="mb-3">
                                                                <label class="col-form-label">Foto:</label>
                                                                <div>
                                                                    <input class="form-control" type="file" name="img">
                                                                </div>
                                                            </div>
                                                            <div class="mb-3">
                                                                <label class="col-form-label" for="visi">Visi:</label>
                                                                <textarea class="form-control" name="visi"
                                                                    id="visi">{{ $data->visi }}</textarea>
                                                            </div>
                                                            <div class="mb-3">
                                                                <label class="col-form-label" for="misi">Misi:</label>
                                                                @foreach ($data->misi as $misi)
                                                                    <textarea class="form-control mb-2"
                                                                        name="misi{{ $loop->iteration }}"
                                                                        id="misi">{{ $misi->detail }}</textarea>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button class="btn btn-secondary" type="button"
                                                                data-bs-dismiss="modal">Close</button>
                                                            <button class="btn btn-primary" type="submit">Save</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/js/datatable/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.autoFill.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.colReorder.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.rowReorder.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.scroller.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/custom.js') }}"></script>
@endsection
