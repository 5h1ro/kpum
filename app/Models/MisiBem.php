<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MisiBem extends Model
{
    use HasFactory;

    protected $fillable = [
        'detail', 'id_bem'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function bem()
    {
        return $this->belongsTo(Bem::class, 'id_bem');
    }
}
