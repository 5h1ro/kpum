<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prodi extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'id_jurusan'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function jurusan()
    {
        return $this->belongsTo(Jurusan::class, 'id_jurusan');
    }

    public function bem1()
    {
        return $this->hasMany(Bem::class, 'id_prodi1');
    }

    public function bem2()
    {
        return $this->hasMany(Bem::class, 'id_prodi2');
    }

    public function dpm()
    {
        return $this->hasMany(Dpm::class, 'id_prodi');
    }

    public function hmj()
    {
        return $this->hasMany(Hmj::class, 'id_prodi');
    }

    public function hima()
    {
        return $this->hasMany(Hima::class, 'id_prodi');
    }

    public function schedule()
    {
        return $this->hasMany(Schedule::class, 'id_prodi');
    }

    public function bem_candidate1()
    {
        return $this->hasMany(BemCandidate::class, 'id_prodi1');
    }

    public function bem_candidate2()
    {
        return $this->hasMany(BemCandidate::class, 'id_prodi2');
    }

    public function dpm_candidate()
    {
        return $this->hasMany(DpmCandidate::class, 'id_prodi');
    }

    public function hmj_candidate()
    {
        return $this->hasMany(HmjCandidate::class, 'id_prodi');
    }

    public function hima_candidate()
    {
        return $this->hasMany(HimaCandidate::class, 'id_prodi');
    }

    public function report()
    {
        return $this->hasMany(Report::class, 'id_prodi');
    }
}
