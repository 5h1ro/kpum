<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jurusan extends Model
{
    use HasFactory;

    protected $fillable = [
        'name'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function prodi()
    {
        return $this->hasMany(Prodi::class, 'id_jurusan');
    }

    public function voter()
    {
        return $this->hasMany(Voter::class, 'id_jurusan');
    }

    public function hmj()
    {
        return $this->hasMany(Hmj::class, 'id_jurusan');
    }

    public function schedule()
    {
        return $this->hasMany(Schedule::class, 'id_jurusan');
    }

    public function bem_candidate1()
    {
        return $this->hasMany(BemCandidate::class, 'id_jurusan1');
    }

    public function bem_candidate2()
    {
        return $this->hasMany(BemCandidate::class, 'id_jurusan2');
    }

    public function dpm_candidate()
    {
        return $this->hasMany(DpmCandidate::class, 'id_jurusan');
    }

    public function hmj_candidate()
    {
        return $this->hasMany(HmjCandidate::class, 'id_jurusan');
    }

    public function hima_candidate()
    {
        return $this->hasMany(HimaCandidate::class, 'id_jurusan');
    }
}
