<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_bem', 'id_dpm', 'id_hmj', 'id_hima', 'id_user'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function bem()
    {
        return $this->belongsTo(Bem::class, 'id_bem');
    }

    public function dpm()
    {
        return $this->belongsTo(Dpm::class, 'id_dpm');
    }
    public function hmj()
    {
        return $this->belongsTo(Hmj::class, 'id_hmj');
    }
    public function hima()
    {
        return $this->belongsTo(Hima::class, 'id_hima');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'id_user');
    }
}
