<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hmj extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'nickname', 'id_jurusan', 'id_prodi', 'img', 'visi', 'vote', 'img2'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function misi()
    {
        return $this->hasMany(MisiHmj::class, 'id_hmj');
    }

    public function jurusan()
    {
        return $this->belongsTo(Jurusan::class, 'id_jurusan');
    }

    public function prodi()
    {
        return $this->belongsTo(Prodi::class, 'id_prodi');
    }

    public function vote()
    {
        return $this->hasMany(Vote::class, 'id_hmj');
    }
}
