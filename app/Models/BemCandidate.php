<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BemCandidate extends Model
{
    use HasFactory;

    protected $fillable = [
        'fullname1',
        'name1',
        'npm1',
        'birthplace1',
        'birthday1',
        'gender1',
        'email1',
        'id_jurusan1',
        'id_prodi1',
        'class1',
        'fullname2',
        'name2',
        'npm2',
        'birthplace2',
        'birthday2',
        'gender2',
        'email2',
        'id_jurusan2',
        'id_prodi2',
        'class2',
        'visi',
        'formulir',
        'persyaratan',
        'photo1',
        'photo2',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function prodi1()
    {
        return $this->belongsTo(Prodi::class, 'id_prodi1');
    }

    public function prodi2()
    {
        return $this->belongsTo(Prodi::class, 'id_prodi2');
    }

    public function jurusan1()
    {
        return $this->belongsTo(Jurusan::class, 'id_jurusan1');
    }

    public function jurusan2()
    {
        return $this->belongsTo(Jurusan::class, 'id_jurusan2');
    }

    public function misi()
    {
        return $this->hasMany(MisiBemCandidate::class, 'id_bem_candidate');
    }
}
