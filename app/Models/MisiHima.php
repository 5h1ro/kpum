<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MisiHima extends Model
{
    use HasFactory;

    protected $fillable = [
        'detail', 'id_hima'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function hima()
    {
        return $this->belongsTo(Hima::class, 'id_hima');
    }
}
