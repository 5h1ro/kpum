<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Voter extends Model
{
    use HasFactory;
    protected $fillable = [
        'name', 'fullname', 'number', 'birthday', 'city', 'gender', 'religion', 'address', 'id_prodi', 'id_jurusan', 'id_user', 'bem', 'dpm', 'hmj', 'hima', 'image'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function prodi()
    {
        return $this->belongsTo(Prodi::class, 'id_prodi');
    }

    public function jurusan()
    {
        return $this->belongsTo(Jurusan::class, 'id_jurusan');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'id_user');
    }
}
