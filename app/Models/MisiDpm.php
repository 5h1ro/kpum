<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MisiDpm extends Model
{
    use HasFactory;

    protected $fillable = [
        'detail', 'id_dpm'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function dpm()
    {
        return $this->belongsTo(Dpm::class, 'id_dpm');
    }
}
