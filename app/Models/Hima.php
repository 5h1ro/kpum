<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hima extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'nickname', 'id_prodi', 'img', 'visi', 'vote', 'img2'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function misi()
    {
        return $this->hasMany(MisiHima::class, 'id_hima');
    }

    public function prodi()
    {
        return $this->belongsTo(Prodi::class, 'id_prodi');
    }

    public function vote()
    {
        return $this->hasMany(Vote::class, 'id_hima');
    }
}
