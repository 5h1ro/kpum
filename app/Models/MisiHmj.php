<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MisiHmj extends Model
{
    use HasFactory;

    protected $fillable = [
        'detail', 'id_hmj'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function hmj()
    {
        return $this->belongsTo(Hmj::class, 'id_hmj');
    }
}
