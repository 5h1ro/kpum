<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bem extends Model
{
    use HasFactory;

    protected $fillable = [
        'name1', 'name2', 'nickname1', 'nickname2', 'id_prodi1', 'id_prodi2', 'img', 'visi', 'vote', 'img2'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function misi()
    {
        return $this->hasMany(MisiBem::class, 'id_bem');
    }

    public function prodi1()
    {
        return $this->belongsTo(Prodi::class, 'id_prodi1');
    }

    public function prodi2()
    {
        return $this->belongsTo(Prodi::class, 'id_prodi2');
    }

    public function vote()
    {
        return $this->hasMany(Vote::class, 'id_bem');
    }
}
