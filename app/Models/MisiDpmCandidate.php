<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MisiDpmCandidate extends Model
{
    use HasFactory;

    protected $fillable = [
        'detail', 'id_dpm_candidate'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function dpm()
    {
        return $this->belongsTo(DpmCandidate::class, 'id_dpm_candidate');
    }
}
