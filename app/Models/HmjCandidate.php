<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HmjCandidate extends Model
{
    use HasFactory;

    protected $fillable = [
        'fullname',
        'name',
        'npm',
        'birthplace',
        'birthday',
        'gender',
        'email',
        'id_jurusan',
        'id_prodi',
        'class',
        'visi',
        'formulir',
        'persyaratan',
        'photo',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function prodi()
    {
        return $this->belongsTo(Prodi::class, 'id_prodi');
    }

    public function jurusan()
    {
        return $this->belongsTo(Jurusan::class, 'id_jurusan');
    }

    public function misi()
    {
        return $this->hasMany(MisiHmjCandidate::class, 'id_hmj_candidate');
    }
}
