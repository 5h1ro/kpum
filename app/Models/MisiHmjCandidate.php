<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MisiHmjCandidate extends Model
{
    use HasFactory;

    protected $fillable = [
        'detail', 'id_hmj_candidate'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function hmj()
    {
        return $this->belongsTo(HmjCandidate::class, 'id_hmj_candidate');
    }
}
