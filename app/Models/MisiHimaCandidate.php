<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MisiHimaCandidate extends Model
{
    use HasFactory;

    protected $fillable = [
        'detail', 'id_hima_candidate'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function hima()
    {
        return $this->belongsTo(HimaCandidate::class, 'id_hima_candidate');
    }
}
