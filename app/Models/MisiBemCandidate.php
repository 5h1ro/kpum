<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MisiBemCandidate extends Model
{
    use HasFactory;

    protected $fillable = [
        'detail', 'id_bem_candidate'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function bem()
    {
        return $this->belongsTo(BemCandidate::class, 'id_bem_candidate');
    }
}
