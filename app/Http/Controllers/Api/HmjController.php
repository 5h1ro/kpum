<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Hmj;
use App\Models\User;
use App\Models\Vote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class HmjController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $hmj = Hmj::where('id', $id)->first();
        $hmj->prodi->jurusan;
        $hmj->misi;
        return response()->json([
            'success' => true,
            'data'    => $hmj,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function data($id)
    {
        $hmj = Hmj::where('id_jurusan', $id)->get();
        foreach ($hmj as $data) {
            $data->jurusan;
            $data->prodi;
        }
        return response()->json([
            'success' => true,
            'data'    => $hmj,
        ]);
    }

    public function vote($id, $id_user)
    {
        $user = User::where('id', $id_user)->first();
        $vote_check = Vote::where([['id_hmj', '=', $id], ['id_user', '=', $user->id]])->first();
        $voter = $user->voter;
        if ($voter->hmj == 0) {
            if ($vote_check == null) {
                $voter->hmj = 1;
                $voter->save();
                $vote = new Vote;
                $vote->id_hmj = Hash::make($id);
                $vote->id_user = $user->id;
                $vote->save();
                $count_vote = 0;
                $votes = Vote::all();
                $hmj = Hmj::all();
                foreach ($votes as $data) {
                    foreach ($hmj as $value) {
                        if (Hash::check($value->id, $data->id_hmj)) {
                            $$count_vote = $count_vote + 1;
                        }
                    }
                }
                $vote->count = $count_vote;
                return response()->json([
                    'success' => true,
                    'vote' => true,
                    'messages' => 'Vote Berhasil',
                    'data'    => $vote,
                ]);
            } else {

                return response()->json([
                    'success' => true,
                    'vote' => false,
                    'messages' => 'Vote Gagal',
                ]);
            }
        } else {

            return response()->json([
                'success' => true,
                'vote' => false,
                'messages' => 'Anda Sudah Memilih',
            ]);
        }
    }

    public function count()
    {
        $votes = Vote::all();
        $hmj = Hmj::all();
        foreach ($hmj as $value) {
            $value->vote = 0;
            foreach ($votes as $data) {
                if (Hash::check($value->id, $data->id_hmj)) {
                    $value->vote = $value->vote + 1;
                }
            }
        }
        return response()->json([
            'success' => true,
            'data'    => $hmj,
        ]);
    }
}
