<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Bem;
use App\Models\User;
use App\Models\Vote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class BemController extends Controller
{
    public function index()
    {
        $bem = Bem::all();
        foreach ($bem as $data) {
            $data->prodi1->jurusan;
            $data->prodi2->jurusan;
        }
        return response()->json([
            'success' => true,
            'data'    => $bem,
        ]);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $bem = Bem::where('id', $id)->first();
        $bem->prodi1->jurusan;
        $bem->prodi2->jurusan;
        $bem->misi;
        return response()->json([
            'success' => true,
            'data'    => $bem,
        ]);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    public function vote($id, $id_user)
    {
        $user = User::where('id', $id_user)->first();
        $vote_check = Vote::where([['id_bem', '=', $id], ['id_user', '=', $user->id]])->first();
        $voter = $user->voter;
        if ($voter->bem == 0) {
            if ($vote_check == null) {
                $voter->bem = 1;
                $voter->save();
                $vote = new Vote;
                $vote->id_bem = Hash::make($id);
                $vote->id_user = $user->id;
                $vote->save();
                $count_vote = 0;
                $votes = Vote::all();
                $bem = Bem::all();
                foreach ($votes as $data) {
                    foreach ($bem as $value) {
                        if (Hash::check($value->id, $data->id_bem)) {
                            $$count_vote = $count_vote + 1;
                        }
                    }
                }
                $vote->count = $count_vote;
                return response()->json([
                    'success' => true,
                    'vote' => true,
                    'messages' => 'Vote Berhasil',
                    'data'    => $vote,
                ]);
            } else {

                return response()->json([
                    'success' => true,
                    'vote' => false,
                    'messages' => 'Vote Gagal',
                ]);
            }
        } else {

            return response()->json([
                'success' => true,
                'vote' => false,
                'messages' => 'Anda Sudah Memilih',
            ]);
        }
    }

    public function count()
    {
        $votes = Vote::all();
        $bem = Bem::all();
        foreach ($bem as $value) {
            $value->vote = 0;
            foreach ($votes as $data) {
                if (Hash::check($value->id, $data->id_bem)) {
                    $value->vote = $value->vote + 1;
                }
            }
        }
        return response()->json([
            'success' => true,
            'data'    => $bem,
        ]);
    }
}
