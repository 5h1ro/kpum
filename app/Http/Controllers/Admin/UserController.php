<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bem;
use App\Models\Dpm;
use App\Models\Hima;
use App\Models\Hmj;
use App\Models\Jurusan;
use App\Models\Prodi;
use App\Models\User;
use App\Models\Vote;
use App\Models\Voter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

class UserController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $voter = User::where('id_role', '3')->paginate(10);
        if ($voter != null) {
            foreach ($voter as $data) {
                if ($data->voter->bem == 1) {
                    $data->voter->bem = "Sudah Memilih";
                    if ($data->voter->dpm == 1) {
                        $data->voter->dpm = "Sudah Memilih";
                        if ($data->voter->hmj == 1) {
                            $data->voter->hmj = "Sudah Memilih";
                            if ($data->voter->hima == 1) {
                                $data->voter->hima = "Sudah Memilih";
                            } else {
                                $data->voter->hima = "Belum Memilih";
                            }
                        } else {
                            $data->voter->hmj = "Belum Memilih";
                            if ($data->voter->hima == 1) {
                                $data->voter->hima = "Sudah Memilih";
                            } else {
                                $data->voter->hima = "Belum Memilih";
                            }
                        }
                    } else {
                        $data->voter->dpm = "Belum Memilih";
                        if ($data->voter->hmj == 1) {
                            $data->voter->hmj = "Sudah Memilih";
                            if ($data->voter->hima == 1) {
                                $data->voter->hima = "Sudah Memilih";
                            } else {
                                $data->voter->hima = "Belum Memilih";
                            }
                        } else {
                            $data->voter->hmj = "Belum Memilih";
                            if ($data->voter->hima == 1) {
                                $data->voter->hima = "Sudah Memilih";
                            } else {
                                $data->voter->hima = "Belum Memilih";
                            }
                        }
                    }
                } else {
                    $data->voter->bem = "Belum Memilih";
                    if ($data->voter->dpm == 1) {
                        $data->voter->dpm = "Sudah Memilih";
                        if ($data->voter->hmj == 1) {
                            $data->voter->hmj = "Sudah Memilih";
                            if ($data->voter->hima == 1) {
                                $data->voter->hima = "Sudah Memilih";
                            } else {
                                $data->voter->hima = "Belum Memilih";
                            }
                        } else {
                            $data->voter->hmj = "Belum Memilih";
                            if ($data->voter->hima == 1) {
                                $data->voter->hima = "Sudah Memilih";
                            } else {
                                $data->voter->hima = "Belum Memilih";
                            }
                        }
                    } else {
                        $data->voter->dpm = "Belum Memilih";
                        if ($data->voter->hmj == 1) {
                            $data->voter->hmj = "Sudah Memilih";
                            if ($data->voter->hima == 1) {
                                $data->voter->hima = "Sudah Memilih";
                            } else {
                                $data->voter->hima = "Belum Memilih";
                            }
                        } else {
                            $data->voter->hmj = "Belum Memilih";
                            if ($data->voter->hima == 1) {
                                $data->voter->hima = "Sudah Memilih";
                            } else {
                                $data->voter->hima = "Belum Memilih";
                            }
                        }
                    }
                }
            }
        }

        $os = ['Linux', 'Mac', 'Windows', 'Android', 'iPhone', 'Unknown'];
        $browser = ['Opera', 'Edge', 'Chrome', 'Safari', 'Firefox', 'Internet Explorer', 'Unknown'];
        $prodi = Prodi::all();
        $jurusan = Jurusan::all();
        return view('admin.user.index', compact('user', 'voter', 'prodi', 'jurusan', 'os', 'browser'));
    }

    public function edit($id, Request $request)
    {
        $voter = Voter::findOrFail($id);
        $user = User::where('id', $voter->id_user)->first();
        $voter->name = $request->name;
        $voter->number = $request->number;
        $voter->id_jurusan = $request->jurusan;
        $voter->id_prodi = $request->prodi;
        $user->os = $request->os;
        $user->browser = $request->browser;
        $voter->save();
        $user->save();
        return redirect()->back();
    }

    public function add(Request $request)
    {
        //get file
        $upload = $request->file('file');
        $filePath = $upload->getRealPath();
        //open and read
        $file = fopen($filePath, 'r');
        $header = fgetcsv($file);
        // dd($header);
        $escapedHeader = [];
        //validate
        foreach ($header as $key => $value) {
            $lheader = strtolower($value);
            array_push($escapedHeader, $lheader);
        }
        while ($columns = fgetcsv($file)) {
            if ($columns[0] == "") {
                continue;
            }
            $data = array_combine($escapedHeader, $columns);

            $user = User::create([
                'npm'               => $data['npm'],
                'email'             => $data['email'],
                'password'          => Hash::make($data['password'])
            ]);
            $user->save();
            $voter = Voter::create([
                'name'             => $data['name'],
                'number'           => $data['no'],
                'id_jurusan'       => $data['id_jurusan'],
                'id_prodi'         => $data['id_prodi'],
                'id_user'          => $user->id,
            ]);
            $voter->save();

            require base_path("vendor/autoload.php");

            $mail = new PHPMailer(true);     // Passing `true` enables exceptions

            try {

                //Server settings
                $mail->SMTPDebug = 2; // Enable verbose debug output
                $mail->isSMTP(); // Set mailer to use SMTP
                $mail->Host = 'tls://smtp.gmail.com'; // Specify main and backup SMTP servers
                $mail->SMTPAuth = true; // Enable SMTP authentication
                $mail->Username = 'dany.zoom01@gmail.com'; // SMTP username
                $mail->Password = 'nurhakiki11200'; // SMTP password
                $mail->SMTPSecure = 'ssl'; // Enable TLS encryption, [ICODE]ssl[/ICODE] also accepted
                $mail->Port = 587; // TCP port to connect to

                //Recipients
                $mail->setFrom('dany.zoom01@gmail.com', 'noReply');
                $mail->addAddress($data['email']);

                // Content
                $mail->isHTML(true); // Set email format to HTML
                $mail->Subject = 'AKUN KPUM PNM 2021';
                $mail->Body = "<h3><b>Selamat datang di Pemira 2021, berikut merupakan akun anda. Simpan baik baik.<b></h3>
            NPM anda <b>" . $data['npm'] . "</b>, dan password anda <b>" . $data['password'] . "</b><br>
            Silahkan registrasi device anda melalui link berikut : <br>
            127.0.0.1:8000/register=" . Crypt::encrypt($user->id);

                $mail->send();
            } catch (Exception $e) {
                echo "Message could not be sent. Mailer Error: $mail->ErrorInfo";
            }
        }

        return redirect()->back();
    }

    public function delete($id)
    {
        $voter = Voter::findOrFail($id);
        if ($voter->bem == 1) {
            $vote = Vote::where([['id_bem', '!=', null], ['id_user', '=', $voter->user->id]])->first();
            if ($vote != null) {
                $bem = Bem::findOrFail($vote->id_bem);
                $bem->vote = $bem->vote - 1;
                $bem->save();
                $vote->delete();
            }
        }

        if ($voter->dpm == 1) {
            $vote = Vote::where([['id_dpm', '!=', null], ['id_user', '=', $voter->user->id]])->first();
            if ($vote != null) {
                $dpm = Dpm::findOrFail($vote->id_dpm);
                $dpm->vote = $dpm->vote - 1;
                $dpm->save();
                $vote->delete();
            }
        }

        if ($voter->hmj == 1) {
            $vote = Vote::where([['id_hmj', '!=', null], ['id_user', '=', $voter->user->id]])->first();
            if ($vote != null) {
                $hmj = Hmj::findOrFail($vote->id_hmj);
                $hmj->vote = $hmj->vote - 1;
                $hmj->save();
                $vote->delete();
            }
        }

        if ($voter->hima == 1) {
            $vote = Vote::where([['id_him', '!=', null], ['id_user', '=', $voter->user->id]])->first();
            if ($vote != null) {
                $hima = Hima::findOrFail($vote->id_him);
                $hima->vote = $hima->vote - 1;
                $hima->save();
                $vote->delete();
            }
        }

        $user = User::findOrFail($voter->id_user);
        $user->delete();

        return redirect()->back();
    }
}
