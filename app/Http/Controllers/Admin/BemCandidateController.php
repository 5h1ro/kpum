<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bem;
use App\Models\BemCandidate;
use App\Models\Jurusan;
use App\Models\MisiBem;
use App\Models\Prodi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BemCandidateController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $calon = BemCandidate::all();
        $prodi = Prodi::all();
        $jurusan = Jurusan::all();
        return view('admin.candidate.bem.index', compact('user', 'calon', 'prodi', 'jurusan'));
    }

    public function detail($id)
    {
        $user = Auth::user();
        $calon = BemCandidate::findOrFail($id);
        $calon->birthday1 = substr($calon->birthday1, 8, 2) . '-' . substr($calon->birthday1, 5, 2) . '-' . substr($calon->birthday1, 0, 4);
        $calon->birthday2 = substr($calon->birthday2, 8, 2) . '-' . substr($calon->birthday2, 5, 2) . '-' . substr($calon->birthday2, 0, 4);
        return view('admin.candidate.bem.detail', compact('user', 'calon'));
    }

    public function acc($id)
    {
        $calon = BemCandidate::findOrFail($id);
        $bem = new Bem;
        $bem->name1 = $calon->fullname1;
        $bem->name2 = $calon->fullname2;
        $bem->nickname1 = $calon->name1;
        $bem->nickname2 = $calon->name2;
        $bem->id_prodi1 = $calon->id_prodi1;
        $bem->id_prodi2 = $calon->id_prodi2;
        $bem->visi = $calon->visi;
        $bem->save();

        foreach ($calon->misi as $old_misi) {
            $misi = new MisiBem;
            $misi->detail = $old_misi->detail;
            $misi->id_bem = $bem->id;
            $misi->save();
        }
        $calon->delete();
        return redirect()->route('admin-bem-candidate');
    }
}
