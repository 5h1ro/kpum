<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bem;
use App\Models\MisiBem;
use App\Models\Prodi;
use App\Models\Report;
use App\Models\Setup;
use App\Models\Vote;
use App\Models\Voter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Barryvdh\DomPDF\Facade as PDF;
use HnhDigital\LaravelNumberConverter\Facade as NumConvert;
use Illuminate\Support\Facades\Hash;

use function PHPUnit\Framework\isNull;

class BemController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $calon = Bem::all();
        $vote = Vote::all();
        foreach ($vote as $value) {
            foreach ($calon as $values) {
                if (Hash::check($values->id, $value->id_bem)) {
                    $value->id_bem = $values->id;
                }
            }
        }
        foreach ($calon as $value) {
            $value->vote = count($vote->where('id_bem', $value->id));
        }
        $prodi = Prodi::all();
        $category = ['BEM', 'DPM', 'HMJ', 'HIMA'];
        return view('admin.calon.bem.index', compact('user', 'calon', 'prodi', 'category'));
    }

    public function add(Request $request)
    {
        $name = Carbon::now()->format('YmdHis');
        $fileName = $name . '.' . $request->image->getClientOriginalExtension();
        $request->image->move(public_path('image/candidate'), $fileName);
        $fileName2 = $name . 'vote.' . $request->image2->getClientOriginalExtension();
        $request->image2->move(public_path('image/candidate'), $fileName2);

        $bem = new Bem;
        $bem->nickname1 = $request->nickname1;
        $bem->nickname2 = $request->nickname2;
        $bem->name1 = $request->name1;
        $bem->name2 = $request->name2;
        $bem->img = asset('image/candidate') . '/' . $fileName;
        $bem->img2 = asset('image/candidate') . '/' . $fileName2;
        $bem->id_prodi1 = $request->id_prodi1;
        $bem->id_prodi2 = $request->id_prodi2;
        $bem->visi = $request->visi;
        $bem->save();
        if ($request->misi1 != null) {
            $misi = new MisiBem;
            $misi->detail = $request->misi1;
            $misi->id_bem = $bem->id;
            $misi->save();
            if ($request->misi2 != null) {
                $misi2 = new MisiBem;
                $misi2->detail = $request->misi2;
                $misi2->id_bem = $bem->id;
                $misi2->save();
                if ($request->misi3 != null) {
                    $misi3 = new MisiBem;
                    $misi3->detail = $request->misi3;
                    $misi3->id_bem = $bem->id;
                    $misi3->save();
                    if ($request->misi4 != null) {
                        $misi4 = new MisiBem;
                        $misi4->detail = $request->misi4;
                        $misi4->id_bem = $bem->id;
                        $misi4->save();
                        if ($request->misi5 != null) {
                            $misi5 = new MisiBem;
                            $misi5->detail = $request->misi5;
                            $misi5->id_bem = $bem->id;
                            $misi5->save();
                            if ($request->misi6 != null) {
                                $misi6 = new MisiBem;
                                $misi6->detail = $request->misi6;
                                $misi6->id_bem = $bem->id;
                                $misi6->save();
                            } else {
                                return redirect('bem-calon');
                            }
                        } else {
                            return redirect('bem-calon');
                        }
                    } else {
                        return redirect('bem-calon');
                    }
                } else {
                    return redirect('bem-calon');
                }
            } else {
                return redirect('bem-calon');
            }
        } else {
            return redirect('bem-calon');
        }
    }

    public function update(Request $request)
    {
        $misi = MisiBem::where('id_bem', $request->id)->get();
        $name = Carbon::now()->format('YmdHis');
        for ($i = 0; $i < count($misi); $i++) {
            $a = "misi" . $i + 1;
            $misi[$i]->detail = $request->$a;
            $misi[$i]->save();
        };
        $bem = Bem::find($request->id);
        $bem->name1 = $request->name1;
        $bem->name2 = $request->name2;
        if ($request->image != null) {
            $fileName = $name . '.' . $request->image->getClientOriginalExtension();
            $request->image->move(public_path('image/candidate'), $fileName);
            $bem->img = asset('image/candidate') . '/' . $fileName;
        }
        if ($request->image2 != null) {
            $fileName2 = $name . 'vote.' . $request->image2->getClientOriginalExtension();
            $request->image2->move(public_path('image/candidate'), $fileName2);
            $bem->img2 = asset('image/candidate') . '/' . $fileName2;
        }
        $bem->id_prodi1 = $request->id_prodi1;
        $bem->id_prodi2 = $request->id_prodi2;
        $bem->visi = $request->visi;
        $bem->save();
        return redirect()->route('bem-calon');
    }

    public function delete($id)
    {
        $bem = Bem::findOrFail($id);
        dd($bem);
    }

    public function create(Request $request)
    {
        $filename = Carbon::now()->format('dmYHis');
        $now = Carbon::now()->isoFormat('D MMMM Y');
        $d = Carbon::now()->isoFormat('dddd');
        $D = $this->penyebut(Carbon::now()->isoFormat('D'));
        $M = strtolower(Carbon::now()->isoFormat('MMMM'));
        $Y = $this->penyebut(Carbon::now()->isoFormat('Y'));
        $path = public_path('reports');
        $report = Report::all();
        $setup = Setup::first();
        $setup->no = $setup->no + 1;
        $setup->save();
        $setup->now = $now;
        $setup->now_text = $d . ', tanggal' . $D . ' bulan ' . $M . ' tahun' . $Y;
        $setup->date = Carbon::now()->isoFormat('D/MM/Y');
        $setup->voter = count(Voter::all());
        if (strlen($setup->no) >= 3 || (int)$setup->no == 3) {
            $setup->no = (int)$setup->no;
        } elseif (strlen($setup->no) == 2 || (int)$setup->no == 2) {
            $setup->no = '0' . (int)$setup->no;
        } elseif (strlen($setup->no) == 1) {
            $setup->no = '00' . (int)$setup->no;
        }
        $setup->no = $setup->no . '/KPUM/' . NumConvert::roman((int)Carbon::now()->isoFormat('M')) . '/' . Carbon::now()->isoFormat('Y');
        $bem = Bem::all();
        $pdf = PDF::loadview('reports.bem.sk_tap', compact('setup', 'bem', 'request'))->setPaper('a4', 'portrait');
        $pdf->save($path . '/BEM/SK_TAP_' . $filename . '.pdf');

        $new_report = new Report;
        $new_report->name = 'SK BEM';
        $new_report->category = 'BEM';
        $new_report->no = $setup->no;
        $new_report->url_sk_tap = asset('reports') . '/BEM/SK_TAP_' . $filename . '.pdf';
        $new_report->save();
        return redirect()->back();
    }

    public function penyebut($nilai)
    {
        $nilai = abs($nilai);
        $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($nilai < 12) {
            $temp = " " . $huruf[$nilai];
        } else if ($nilai < 20) {
            $temp = $this->penyebut($nilai - 10) . " belas";
        } else if ($nilai < 100) {
            $temp = $this->penyebut($nilai / 10) . " puluh" . $this->penyebut($nilai % 10);
        } else if ($nilai < 200) {
            $temp = " seratus" . $this->penyebut($nilai - 100);
        } else if ($nilai < 1000) {
            $temp = $this->penyebut($nilai / 100) . " ratus" . $this->penyebut($nilai % 100);
        } else if ($nilai < 2000) {
            $temp = " seribu" . $this->penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
            $temp = $this->penyebut($nilai / 1000) . " ribu" . $this->penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
            $temp = $this->penyebut($nilai / 1000000) . " juta" . $this->penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
            $temp = $this->penyebut($nilai / 1000000000) . " milyar" . $this->penyebut(fmod($nilai, 1000000000));
        } else if ($nilai < 1000000000000000) {
            $temp = $this->penyebut($nilai / 1000000000000) . " trilyun" . $this->penyebut(fmod($nilai, 1000000000000));
        }
        return $temp;
    }
}
