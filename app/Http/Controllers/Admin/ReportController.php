<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bem;
use App\Models\Dpm;
use App\Models\Hima;
use App\Models\Hmj;
use App\Models\Jurusan;
use App\Models\Prodi;
use App\Models\Report;
use App\Models\Setup;
use App\Models\Vote;
use App\Models\Voter;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use HnhDigital\LaravelNumberConverter\Facade as NumConvert;
use Illuminate\Support\Facades\Hash;

use function PHPUnit\Framework\isNull;

class ReportController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $report = Report::all();
        $prodi = Prodi::all();
        $jurusan = Jurusan::all();
        $category = ['BEM', 'DPM', 'HMJ', 'HIMA'];
        return view('admin.report.index', compact('report', 'user', 'category', 'prodi', 'jurusan'));
    }

    public function delete($id)
    {
        $report = Report::find($id);
        $report->delete();
        return redirect()->back();
    }

    public function add(Request $request)
    {
        $filename = Carbon::now()->format('dmYHis');
        $now = Carbon::now()->isoFormat('D MMMM Y');
        $d = Carbon::now()->isoFormat('dddd');
        $D = $this->penyebut(Carbon::now()->isoFormat('D'));
        $M = strtolower(Carbon::now()->isoFormat('MMMM'));
        $Y = $this->penyebut(Carbon::now()->isoFormat('Y'));
        $path = public_path('reports');
        $setup = Setup::first();
        $setup->no = $setup->no + 1;
        $setup->save();
        $setup->now = $now;
        $setup->now_text = $d . ', tanggal' . $D . ' bulan ' . $M . ' tahun' . $Y;
        $setup->date = Carbon::now()->isoFormat('D/MM/Y');
        $setup->voter = count(Voter::all());
        $reportall = Report::all();
        $setup2 = Setup::first();
        $setup2->no = $setup2->no + 1;
        $setup2->save();
        if (strlen($setup->no) >= 3 || (int)$setup->no == 3) {
            $setup->no = (int)$setup->no;
        } elseif (strlen($setup->no) == 2 || (int)$setup->no == 2) {
            $setup->no = '0' . (int)$setup->no;
        } elseif (strlen($setup->no) == 1) {
            $setup->no = '00' . (int)$setup->no;
        }
        $setup->no = $setup->no . '/KPUM/' . NumConvert::roman((int)Carbon::now()->isoFormat('M')) . '/' . Carbon::now()->isoFormat('Y');

        if (strlen($setup2->no) >= 3 || (int)$setup2->no == 3) {
            $setup2->no = (int)$setup2->no;
        } elseif (strlen($setup2->no) == 2 || (int)$setup2->no == 2) {
            $setup2->no = '0' . (int)$setup2->no;
        } elseif (strlen($setup2->no) == 1) {
            $setup2->no = '00' . (int)$setup2->no;
        }
        $setup2->no = $setup2->no . '/KPUM/' . NumConvert::roman((int)Carbon::now()->isoFormat('M')) . '/' . Carbon::now()->isoFormat('Y');


        if ($request->category == 'BEM') {
            $report = Report::where('name', 'SK BEM')->first();
            if (isset($report)) {
                $bem = Bem::all();
                $bem->total = 0;
                $vote = Vote::all();
                foreach ($vote as $value) {
                    foreach ($bem as $values) {
                        if (Hash::check($values->id, $value->id_bem)) {
                            $value->id_bem = $values->id;
                        }
                    }
                }
                foreach ($bem as $value) {
                    $value->votes = count($vote->where('id_bem', $value->id));
                    $bem->total = $bem->total + $value->votes;
                }
                $winner = $bem->where('votes', $bem->max('votes'))->first();


                $pdf = PDF::loadview('reports.bem.sk_penetapan', compact('setup', 'bem', 'report', 'request', 'setup2', 'winner'))->setPaper('a4', 'portrait');
                $pdf->save($path . '/BEM/SK_PENETAPAN_' . $filename . '.pdf');
                $pdf1 = PDF::loadview('reports.bem.sk_perhitungan', compact('setup', 'bem', 'report', 'request'))->setPaper('a4', 'portrait');
                $pdf1->save($path . '/BEM/SK_PERHITUNGAN_' . $filename . '.pdf');
                $pdf2 = PDF::loadview('reports.bem.ba', compact('setup', 'bem', 'request', 'report'))->setPaper('a4', 'portrait');
                $pdf2->save($path . '/BEM/BA_' . $filename . '.pdf');

                $report->no = $report->no . ' & ' . $setup->no . ' & ' . $setup2->no;
                $report->url_ba = asset('reports') . '/BEM/BA_' . $filename . '.pdf';
                $report->url_sk_penetapan = asset('reports') . '/BEM/SK_PENETAPAN_' . $filename . '.pdf';
                $report->url_sk_perhitungan = asset('reports') . '/BEM/SK_PERHITUNGAN_' . $filename . '.pdf';
                $report->save();
            } else {
                $setupfinal = Setup::first();
                $setupfinal->no = $setupfinal->no - 2;
                $setupfinal->save();
            }
        } elseif ($request->category == 'DPM') {
            $report = Report::where('name', 'SK DPM')->first();
            if (isset($report)) {
                $dpm = Dpm::all();
                $dpm->teknik = collect();
                $dpm->adbis = collect();
                $dpm->kompak = collect();
                foreach ($dpm as $data) {
                    if ($data->prodi->id_jurusan == 1) {
                        $dpm->teknik->push($data);
                    } elseif ($data->prodi->id_jurusan == 2) {
                        $dpm->adbis->push($data);
                    } elseif ($data->prodi->id_jurusan == 3) {
                        $dpm->kompak->push($data);
                    }
                }
                $dpm->total = 0;
                $vote = Vote::all();
                foreach ($vote as $value) {
                    foreach ($dpm as $values) {
                        if (Hash::check($values->id, $value->id_dpm)) {
                            $value->id_dpm = $values->id;
                        }
                    }
                }
                foreach ($dpm->kompak as $value) {
                    $value->votes = count($vote->where('id_dpm', $value->id));
                    $dpm->total = $dpm->total + $value->votes;
                }
                foreach ($dpm->adbis as $value) {
                    $value->votes = count($vote->where('id_dpm', $value->id));
                    $dpm->total = $dpm->total + $value->votes;
                }
                foreach ($dpm->teknik as $value) {
                    $value->votes = count($vote->where('id_dpm', $value->id));
                    $dpm->total = $dpm->total + $value->votes;
                }
                $winner = collect();
                $winner->teknik = collect();
                $winner->adbis = collect();
                $winner->kompak = collect();
                $prodi = Prodi::all();
                foreach ($prodi as $value) {
                    try {
                        if ($value->id <= 6) {
                            $a = $dpm->teknik->where('id_prodi', $value->id)->sortByDesc('votes')->take(2);
                            $winner->teknik->push($a);
                        } elseif ($value->id <= 8) {
                            $a = $dpm->adbis->where('id_prodi', $value->id)->sortByDesc('votes')->take(2);
                            $winner->adbis->push($a);
                        } elseif ($value->id <= 10) {
                            $a = $dpm->kompak->where('id_prodi', $value->id)->sortByDesc('votes')->take(2);
                            $winner->kompak->push($a);
                        }
                    } catch (\Throwable $th) {
                        //throw $th;
                    }
                }
                $pdf = PDF::loadview('reports.dpm.sk_penetapan', compact('setup', 'dpm', 'report', 'request', 'setup2', 'winner'))->setPaper('a4', 'portrait');
                $pdf->save($path . '/DPM/SK_PENETAPAN_' . $filename . '.pdf');
                $pdf1 = PDF::loadview('reports.dpm.sk_perhitungan', compact('setup', 'dpm', 'report', 'request', 'setup2', 'winner'))->setPaper('a4', 'portrait');
                $pdf1->save($path . '/DPM/SK_PERHITUNGAN_' . $filename . '.pdf');
                $pdf2 = PDF::loadview('reports.dpm.ba', compact('setup', 'dpm', 'request', 'report'))->setPaper('a4', 'portrait');
                $pdf2->save($path . '/DPM/BA_' . $filename . '.pdf');

                $report->no = $report->no . ' & ' . $setup->no . ' & ' . $setup2->no;
                $report->url_ba = asset('reports') . '/DPM/BA_' . $filename . '.pdf';
                $report->url_sk_penetapan = asset('reports') . '/DPM/SK_PENETAPAN_' . $filename . '.pdf';
                $report->url_sk_perhitungan = asset('reports') . '/DPM/SK_PERHITUNGAN_' . $filename . '.pdf';
                $report->save();
            } else {
                $setupfinal = Setup::first();
                $setupfinal->no = $setupfinal->no - 2;
                $setupfinal->save();
            }
        } elseif ($request->category == 'HMJ') {
            $jurusan = Jurusan::find($request->id_jurusan);
            $report = Report::where('name', 'SK HMJ ' . strtoupper($jurusan->name))->first();
            if (isset($report)) {
                $hmj = Hmj::where('id_jurusan', $jurusan->id)->get();
                $hmj->total = 0;
                $vote = Vote::all();
                foreach ($vote as $value) {
                    foreach ($hmj as $values) {
                        if (Hash::check($values->id, $value->id_hmj)) {
                            $value->id_hmj = $values->id;
                        }
                    }
                }
                foreach ($hmj as $value) {
                    $value->votes = count($vote->where('id_hmj', $value->id));
                    $hmj->total = $hmj->total + $value->votes;
                }
                $winner = $hmj->where('votes', $hmj->max('votes'))->first();
                $pdf = PDF::loadview('reports.hmj.sk_penetapan', compact('setup', 'hmj', 'report', 'request', 'setup2', 'winner', 'jurusan'))->setPaper('a4', 'portrait');
                $pdf->save($path . '/HMJ/SK_PENETAPAN_' . strtoupper($jurusan->name) . $filename . '.pdf');
                $pdf1 = PDF::loadview('reports.hmj.sk_perhitungan', compact('setup', 'hmj', 'report', 'request', 'setup2', 'winner', 'jurusan'))->setPaper('a4', 'portrait');
                $pdf1->save($path . '/HMJ/SK_PERHITUNGAN_' . strtoupper($jurusan->name) . $filename . '.pdf');
                $pdf2 = PDF::loadview('reports.hmj.ba', compact('setup', 'hmj', 'request', 'report', 'jurusan'))->setPaper('a4', 'portrait');
                $pdf2->save($path . '/HMJ/BA_' . strtoupper($jurusan->name) . $filename . '.pdf');

                $report->no = $report->no . ' & ' . $setup->no . ' & ' . $setup2->no;
                $report->url_ba = asset('reports') . '/HMJ/BA_' . strtoupper($jurusan->name) . $filename . '.pdf';
                $report->url_sk_penetapan = asset('reports') . '/HMJ/SK_PENETAPAN_' . strtoupper($jurusan->name) . $filename . '.pdf';
                $report->url_sk_perhitungan = asset('reports') . '/HMJ/SK_PERHITUNGAN_' . strtoupper($jurusan->name) . $filename . '.pdf';
                $report->save();
            } else {
                $setupfinal = Setup::first();
                $setupfinal->no = $setupfinal->no - 2;
                $setupfinal->save();
            }
        } elseif ($request->category == 'HIMA') {
            $prodi = Prodi::find($request->id_prodi);
            $prodi->name = substr($prodi->name, 3);
            $prodi->himpunan = $this->hima($request->id_prodi);
            $report = Report::where('name', 'SK HIMA ' . strtoupper($prodi->himpunan))->first();
            if (isset($report)) {
                $hima = Hima::where('id_prodi', $prodi->id)->get();
                $hima->total = 0;
                $vote = Vote::all();
                foreach ($vote as $value) {
                    foreach ($hima as $values) {
                        if (Hash::check($values->id, $value->id_hima)) {
                            $value->id_hima = $values->id;
                        }
                    }
                }
                foreach ($hima as $value) {
                    $value->votes = count($vote->where('id_hima', $value->id));
                    $hima->total = $hima->total + $value->votes;
                }
                $winner = $hima->where('votes', $hima->max('votes'))->first();
                $pdf = PDF::loadview('reports.hima.sk_penetapan', compact('setup', 'hima', 'report', 'request', 'setup2', 'winner', 'prodi'))->setPaper('a4', 'portrait');
                $pdf->save($path . '/HIMA/SK_PENETAPAN_' . strtoupper($prodi->name) . $filename . '.pdf');
                $pdf1 = PDF::loadview('reports.hima.sk_perhitungan', compact('setup', 'hima', 'report', 'request', 'setup2', 'winner', 'prodi'))->setPaper('a4', 'portrait');
                $pdf1->save($path . '/HIMA/SK_PERHITUNGAN_' . strtoupper($prodi->name) . $filename . '.pdf');
                $pdf2 = PDF::loadview('reports.hima.ba', compact('setup', 'hima', 'request', 'report', 'prodi'))->setPaper('a4', 'portrait');
                $pdf2->save($path . '/HIMA/BA_' . strtoupper($prodi->name) . $filename . '.pdf');
                $report->no = $report->no . ' & ' . $setup->no . ' & ' . $setup2->no;
                $report->url_ba = asset('reports') . '/HIMA/BA_' . strtoupper($prodi->name) . $filename . '.pdf';
                $report->url_sk_penetapan = asset('reports') . '/HIMA/SK_PENETAPAN_' . strtoupper($prodi->name) . $filename . '.pdf';
                $report->url_sk_perhitungan = asset('reports') . '/HIMA/SK_PERHITUNGAN_' . strtoupper($prodi->name) . $filename . '.pdf';
                $report->save();
            } else {
                $setupfinal = Setup::first();
                $setupfinal->no = $setupfinal->no - 2;
                $setupfinal->save();
            }
        }
        return redirect()->back();

        // return redirect()->back();
        // return $pdf->stream('laporan-pegawai-pdf');
    }

    public function penyebut($nilai)
    {
        $nilai = abs($nilai);
        $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($nilai < 12) {
            $temp = " " . $huruf[$nilai];
        } else if ($nilai < 20) {
            $temp = $this->penyebut($nilai - 10) . " belas";
        } else if ($nilai < 100) {
            $temp = $this->penyebut($nilai / 10) . " puluh" . $this->penyebut($nilai % 10);
        } else if ($nilai < 200) {
            $temp = " seratus" . $this->penyebut($nilai - 100);
        } else if ($nilai < 1000) {
            $temp = $this->penyebut($nilai / 100) . " ratus" . $this->penyebut($nilai % 100);
        } else if ($nilai < 2000) {
            $temp = " seribu" . $this->penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
            $temp = $this->penyebut($nilai / 1000) . " ribu" . $this->penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
            $temp = $this->penyebut($nilai / 1000000) . " juta" . $this->penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
            $temp = $this->penyebut($nilai / 1000000000) . " milyar" . $this->penyebut(fmod($nilai, 1000000000));
        } else if ($nilai < 1000000000000000) {
            $temp = $this->penyebut($nilai / 1000000000000) . " trilyun" . $this->penyebut(fmod($nilai, 1000000000000));
        }
        return $temp;
    }

    public function hima($id)
    {
        $hasil = "";
        switch ($id) {
            case 1:
                $hasil = "Himatif";
                break;

            case 2:
                $hasil = "Himatekom";
                break;

            case 3:
                $hasil = "Himateklis";
                break;

            case 4:
                $hasil = "Himameto";
                break;

            case 5:
                $hasil = "Himatka";
                break;

            case 6:
                $hasil = "Himatpl";
                break;

            case 7:
                $hasil = "Himadbis";
                break;

            case 8:
                $hasil = "Himags";
                break;

            case 9:
                $hasil = "Himaksi";
                break;

            case 10:
                $hasil = "Himka";
                break;

            default:
                $hasil = "";
                break;
        }

        return $hasil;
    }
}
