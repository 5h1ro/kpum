<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bem;
use App\Models\Dpm;
use App\Models\Hima;
use App\Models\Hmj;
use App\Models\Vote;
use App\Models\Voter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $bem = Bem::all();
        $vote = Vote::all();
        $voter = Voter::all();
        $persentase = [];
        foreach ($vote as $value) {
            foreach ($bem as $values) {
                if (Hash::check($values->id, $value->id_bem)) {
                    $value->id_bem = $values->id;
                }
            }
        }
        foreach ($bem as $percent) {
            $count = count($vote->where('id_bem', $percent->id));
            $percent->vote = $count;
            $data = round($percent->vote / count($voter) * 100, 2);
            array_push($persentase, $data);
        }
        $suarasah = 0;
        $suarasahpersen = 0;
        foreach ($bem as $sah) {
            $count = count($vote->where('id_bem', $sah->id));
            $suarasah = $suarasah + $sah->vote;
            $suarasahpersen = $suarasahpersen + round($sah->vote / count($voter) * 100, 2);
        }
        $pemilih = count($voter);
        $pemilihpersen = 100;
        $suaranonsah = $pemilih - $suarasah;
        $suaranonsahpersen = $pemilihpersen - $suarasahpersen;
        return view('admin.home', compact('user', 'bem', 'persentase', 'suarasah', 'suarasahpersen', 'suaranonsah', 'suaranonsahpersen', 'pemilih', 'pemilihpersen'));
    }

    // public function fresh()
    // {
    //     $user = Auth::user();
    //     $bem = Bem::all();
    //     $persentase = [];
    //     foreach ($bem as $percent) {
    //         $data = round($percent->vote / 1432 * 100, 2);
    //         array_push($persentase, $data);
    //     }
    //     $suarasah = 0;
    //     $suarasahpersen = 0;
    //     foreach ($bem as $sah) {
    //         $suarasah = $suarasah + $sah->vote;
    //         $suarasahpersen = $suarasahpersen + round($sah->vote / 1432 * 100, 2);
    //     }
    //     $pemilih = 1432;
    //     $pemilihpersen = 100;
    //     $suaranonsah = $pemilih - $suarasah;
    //     $suaranonsahpersen = $pemilihpersen - $suarasahpersen;
    //     return view('admin.index', compact('user', 'bem', 'persentase', 'suarasah', 'suarasahpersen', 'suaranonsah', 'suaranonsahpersen', 'pemilih', 'pemilihpersen'));
    // }

    public function index_hima($id)
    {
        $user = Auth::user();
        $hima = Hima::where('id_prodi', $id)->get();
        $persentase = [];
        foreach ($hima as $percent) {
            $vote = Vote::where('id_hima', $percent->id)->get();
            $count = count($vote);
            $percent->vote = $count;
            $data = round($percent->vote / 1432 * 100, 2);
            array_push($persentase, $data);
        }
        $suarasah = 0;
        $suarasahpersen = 0;
        foreach ($hima as $sah) {
            $vote = Vote::where('id_hima', $sah->id)->get();
            $count = count($vote);
            $suarasah = $suarasah + $sah->vote;
            $suarasahpersen = $suarasahpersen + round($sah->vote / 1432 * 100, 2);
        }
        $pemilih = 1432;
        $pemilihpersen = 100;
        $suaranonsah = $pemilih - $suarasah;
        $suaranonsahpersen = $pemilihpersen - $suarasahpersen;
        return view('admin.hima.home', compact('user', 'hima', 'persentase', 'suarasah', 'suarasahpersen', 'suaranonsah', 'suaranonsahpersen', 'pemilih', 'pemilihpersen'));
    }

    // public function fresh_hima($id)
    // {
    //     $user = Auth::user();
    //     $hima = Hima::where('id_prodi', $id)->get();
    //     $persentase = [];
    //     foreach ($hima as $percent) {
    //         $data = round($percent->vote / 1432 * 100, 2);
    //         array_push($persentase, $data);
    //     }
    //     $suarasah = 0;
    //     $suarasahpersen = 0;
    //     foreach ($hima as $sah) {
    //         $suarasah = $suarasah + $sah->vote;
    //         $suarasahpersen = $suarasahpersen + round($sah->vote / 1432 * 100, 2);
    //     }
    //     $pemilih = 1432;
    //     $pemilihpersen = 100;
    //     $suaranonsah = $pemilih - $suarasah;
    //     $suaranonsahpersen = $pemilihpersen - $suarasahpersen;
    //     return view('admin.hima.index', compact('user', 'hima', 'persentase', 'suarasah', 'suarasahpersen', 'suaranonsah', 'suaranonsahpersen', 'pemilih', 'pemilihpersen'));
    // }

    public function index_dpm($id)
    {
        $user = Auth::user();
        $dpm = Dpm::where('id_prodi', $id)->get();
        $vote = Vote::all();
        $voter = Voter::where('id_prodi', $id)->get();
        $persentase = [];
        foreach ($vote as $value) {
            foreach ($dpm as $values) {
                if (Hash::check($values->id, $value->id_dpm)) {
                    $value->id_dpm = $values->id;
                }
            }
        }
        foreach ($dpm as $percent) {
            $count = count($vote->where('id_dpm', $percent->id));
            $percent->vote = $count;
            $data = round($percent->vote / count($voter) * 100, 2);
            array_push($persentase, $data);
        }
        $suarasah = 0;
        $suarasahpersen = 0;
        foreach ($dpm as $sah) {
            $count = count($vote->where('id_dpm', $sah->id));
            $suarasah = $suarasah + $sah->vote;
            $suarasahpersen = $suarasahpersen + round($sah->vote / count($voter) * 100, 2);
        }
        $pemilih = count($voter);
        $pemilihpersen = 100;
        $suaranonsah = $pemilih - $suarasah;
        $suaranonsahpersen = $pemilihpersen - $suarasahpersen;
        return view('admin.dpm.home', compact('user', 'dpm', 'persentase', 'suarasah', 'suarasahpersen', 'suaranonsah', 'suaranonsahpersen', 'pemilih', 'pemilihpersen'));
    }

    // public function fresh_dpm($id)
    // {
    //     $user = Auth::user();
    //     $dpm = Dpm::where('id_prodi', $id)->get();
    //     $persentase = [];
    //     foreach ($dpm as $percent) {
    //         $data = round($percent->vote / 1432 * 100, 2);
    //         array_push($persentase, $data);
    //     }
    //     $suarasah = 0;
    //     $suarasahpersen = 0;
    //     foreach ($dpm as $sah) {
    //         $suarasah = $suarasah + $sah->vote;
    //         $suarasahpersen = $suarasahpersen + round($sah->vote / 1432 * 100, 2);
    //     }
    //     $pemilih = 1432;
    //     $pemilihpersen = 100;
    //     $suaranonsah = $pemilih - $suarasah;
    //     $suaranonsahpersen = $pemilihpersen - $suarasahpersen;
    //     return view('admin.dpm.index', compact('user', 'dpm', 'persentase', 'suarasah', 'suarasahpersen', 'suaranonsah', 'suaranonsahpersen', 'pemilih', 'pemilihpersen'));
    // }

    public function index_hmj($id)
    {
        $user = Auth::user();
        $hmj = Hmj::where('id_jurusan', $id)->get();
        $persentase = [];
        foreach ($hmj as $percent) {
            $vote = Vote::where('id_hmj', $percent->id)->get();
            $count = count($vote);
            $percent->vote = $count;
            $data = round($percent->vote / 1432 * 100, 2);
            array_push($persentase, $data);
        }
        $suarasah = 0;
        $suarasahpersen = 0;
        foreach ($hmj as $sah) {
            $vote = Vote::where('id_hmj', $sah->id)->get();
            $count = count($vote);
            $suarasah = $suarasah + $sah->vote;
            $suarasahpersen = $suarasahpersen + round($sah->vote / 1432 * 100, 2);
        }
        $pemilih = 1432;
        $pemilihpersen = 100;
        $suaranonsah = $pemilih - $suarasah;
        $suaranonsahpersen = $pemilihpersen - $suarasahpersen;
        return view('admin.hmj.home', compact('user', 'hmj', 'persentase', 'suarasah', 'suarasahpersen', 'suaranonsah', 'suaranonsahpersen', 'pemilih', 'pemilihpersen'));
    }

    // public function fresh_hmj($id)
    // {
    //     $user = Auth::user();
    //     $hmj = Hmj::where('id_jurusan', $id)->get();
    //     $persentase = [];
    //     foreach ($hmj as $percent) {
    //         $data = round($percent->vote / 1432 * 100, 2);
    //         array_push($persentase, $data);
    //     }
    //     $suarasah = 0;
    //     $suarasahpersen = 0;
    //     foreach ($hmj as $sah) {
    //         $suarasah = $suarasah + $sah->vote;
    //         $suarasahpersen = $suarasahpersen + round($sah->vote / 1432 * 100, 2);
    //     }
    //     $pemilih = 1432;
    //     $pemilihpersen = 100;
    //     $suaranonsah = $pemilih - $suarasah;
    //     $suaranonsahpersen = $pemilihpersen - $suarasahpersen;
    //     return view('admin.hmj.index', compact('user', 'hmj', 'persentase', 'suarasah', 'suarasahpersen', 'suaranonsah', 'suaranonsahpersen', 'pemilih', 'pemilihpersen'));
    // }
}
