<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Dpm;
use App\Models\MisiDpm;
use App\Models\Prodi;
use App\Models\Report;
use App\Models\Setup;
use App\Models\Vote;
use App\Models\Voter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Barryvdh\DomPDF\Facade as PDF;
use HnhDigital\LaravelNumberConverter\Facade as NumConvert;
use Illuminate\Support\Facades\Hash;

class DpmController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $calon = Dpm::all();
        $vote = Vote::all();
        foreach ($vote as $value) {
            foreach ($calon as $values) {
                if (Hash::check($values->id, $value->id_dpm)) {
                    $value->id_dpm = $values->id;
                }
            }
        }
        foreach ($calon as $value) {
            $value->vote = count($vote->where('id_dpm', $value->id));
        }
        $prodi = Prodi::all();
        return view('admin.calon.dpm.index', compact('user', 'calon', 'prodi'));
    }

    public function add(Request $request)
    {
        $name = Carbon::now()->format('YmdHis');
        $fileName = $name . '.' . $request->image->getClientOriginalExtension();
        $request->image->move(public_path('image/candidate'), $fileName);
        $fileName2 = $name . 'vote.' . $request->image2->getClientOriginalExtension();
        $request->image2->move(public_path('image/candidate'), $fileName2);

        $dpm = new Dpm;
        $dpm->nickname = $request->nickname;
        $dpm->name = $request->name;
        $dpm->img = asset('image/candidate') . '/' . $fileName;
        $dpm->img2 = asset('image/candidate') . '/' . $fileName2;
        $dpm->id_prodi = $request->id_prodi;
        $dpm->visi = $request->visi;
        $dpm->save();
        if ($request->misi1 != null) {
            $misi = new MisiDpm;
            $misi->detail = $request->misi1;
            $misi->id_dpm = $dpm->id;
            $misi->save();
            if ($request->misi2 != null) {
                $misi2 = new MisiDpm;
                $misi2->detail = $request->misi2;
                $misi2->id_dpm = $dpm->id;
                $misi2->save();
                if ($request->misi3 != null) {
                    $misi3 = new MisiDpm;
                    $misi3->detail = $request->misi3;
                    $misi3->id_dpm = $dpm->id;
                    $misi3->save();
                    if ($request->misi4 != null) {
                        $misi4 = new MisiDpm;
                        $misi4->detail = $request->misi4;
                        $misi4->id_dpm = $dpm->id;
                        $misi4->save();
                        if ($request->misi5 != null) {
                            $misi5 = new MisiDpm;
                            $misi5->detail = $request->misi5;
                            $misi5->id_dpm = $dpm->id;
                            $misi5->save();
                            if ($request->misi6 != null) {
                                $misi6 = new MisiDpm;
                                $misi6->detail = $request->misi6;
                                $misi6->id_dpm = $dpm->id;
                                $misi6->save();
                            } else {
                                return redirect('dpm-calon');
                            }
                        } else {
                            return redirect('dpm-calon');
                        }
                    } else {
                        return redirect('dpm-calon');
                    }
                } else {
                    return redirect('dpm-calon');
                }
            } else {
                return redirect('dpm-calon');
            }
        } else {
            return redirect('dpm-calon');
        }
    }
    public function update(Request $request)
    {
        $name = Carbon::now()->format('YmdHis');
        $fileName = $name . '.' . $request->image->getClientOriginalExtension();
        $request->image->move(public_path('image/candidate'), $fileName);
        $fileName2 = $name . 'vote.' . $request->image2->getClientOriginalExtension();
        $request->image2->move(public_path('image/candidate'), $fileName2);
        $misi = MisiDpm::where('id_dpm', $request->id)->get();
        for ($i = 0; $i < count($misi); $i++) {
            $a = "misi" . $i + 1;
            $misi[$i]->detail = $request->$a;
            $misi[$i]->save();
        };

        $dpm = Dpm::find($request->id);
        $dpm->name = $request->name;
        $dpm->img = asset('image/candidate') . '/' . $fileName;
        $dpm->img2 = asset('image/candidate') . '/' . $fileName2;
        $dpm->id_prodi = $request->id_prodi;
        $dpm->visi = $request->visi;
        $dpm->save();
        return redirect('dpm-calon');
    }

    public function create(Request $request)
    {
        $filename = Carbon::now()->format('dmYHis');
        $now = Carbon::now()->isoFormat('D MMMM Y');
        $d = Carbon::now()->isoFormat('dddd');
        $D = $this->penyebut(Carbon::now()->isoFormat('D'));
        $M = strtolower(Carbon::now()->isoFormat('MMMM'));
        $Y = $this->penyebut(Carbon::now()->isoFormat('Y'));
        $path = public_path('reports');
        $report = Report::all();
        $setup = Setup::first();
        $setup->no = $setup->no + 1;
        $setup->save();
        $setup->now = $now;
        $setup->now_text = $d . ', tanggal' . $D . ' bulan ' . $M . ' tahun' . $Y;
        $setup->date = Carbon::now()->isoFormat('D/MM/Y');
        $setup->voter = count(Voter::all());
        if (strlen($setup->no) >= 3 || (int)$setup->no == 3) {
            $setup->no = (int)$setup->no;
        } elseif (strlen($setup->no) == 2 || (int)$setup->no == 2) {
            $setup->no = '0' . (int)$setup->no;
        } elseif (strlen($setup->no) == 1) {
            $setup->no = '00' . (int)$setup->no;
        }
        $setup->no = $setup->no . '/KPUM/' . NumConvert::roman((int)Carbon::now()->isoFormat('M')) . '/' . Carbon::now()->isoFormat('Y');

        $dpm = Dpm::all();
        $dpm->teknik = collect();
        $dpm->adbis = collect();
        $dpm->kompak = collect();
        foreach ($dpm as $data) {
            if ($data->prodi->id_jurusan == 1) {
                $dpm->teknik->push($data);
            } elseif ($data->prodi->id_jurusan == 2) {
                $dpm->adbis->push($data);
            } elseif ($data->prodi->id_jurusan == 3) {
                $dpm->kompak->push($data);
            }
        }
        $pdf = PDF::loadview('reports.dpm.sk_tap', compact('setup', 'dpm', 'request'))->setPaper('a4', 'portrait');
        $pdf->save($path . '/DPM/SK_TAP_' . $filename . '.pdf');

        $new_report = new Report;
        $new_report->name = 'SK DPM';
        $new_report->category = 'DPM';
        $new_report->no = $setup->no;
        $new_report->url_sk_tap = asset('reports') . '/DPM/SK_TAP_' . $filename . '.pdf';
        $new_report->save();
        return redirect()->back();
    }

    public function penyebut($nilai)
    {
        $nilai = abs($nilai);
        $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($nilai < 12) {
            $temp = " " . $huruf[$nilai];
        } else if ($nilai < 20) {
            $temp = $this->penyebut($nilai - 10) . " belas";
        } else if ($nilai < 100) {
            $temp = $this->penyebut($nilai / 10) . " puluh" . $this->penyebut($nilai % 10);
        } else if ($nilai < 200) {
            $temp = " seratus" . $this->penyebut($nilai - 100);
        } else if ($nilai < 1000) {
            $temp = $this->penyebut($nilai / 100) . " ratus" . $this->penyebut($nilai % 100);
        } else if ($nilai < 2000) {
            $temp = " seribu" . $this->penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
            $temp = $this->penyebut($nilai / 1000) . " ribu" . $this->penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
            $temp = $this->penyebut($nilai / 1000000) . " juta" . $this->penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
            $temp = $this->penyebut($nilai / 1000000000) . " milyar" . $this->penyebut(fmod($nilai, 1000000000));
        } else if ($nilai < 1000000000000000) {
            $temp = $this->penyebut($nilai / 1000000000000) . " trilyun" . $this->penyebut(fmod($nilai, 1000000000000));
        }
        return $temp;
    }
}
