<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Hima;
use App\Models\MisiHima;
use App\Models\Prodi;
use App\Models\Report;
use App\Models\Setup;
use App\Models\Vote;
use App\Models\Voter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use HnhDigital\LaravelNumberConverter\Facade as NumConvert;
use Barryvdh\DomPDF\Facade as PDF;

class HimaController extends Controller
{

    public function index()
    {
        $user = Auth::user();
        $calon = Hima::all();
        foreach ($calon as $data) {
            $vote = Vote::where('id_hima', $data->id)->get();
            $count = count($vote);
            $data->vote = $count;
        }
        $prodi = Prodi::all();
        return view('admin.calon.hima.index', compact('user', 'calon', 'prodi'));
    }

    public function update(Request $request)
    {
        $misi = MisiHima::where('id_hima', $request->id)->get();
        for ($i = 0; $i < count($misi); $i++) {
            $a = "misi" . $i + 1;
            $misi[$i]->detail = $request->$a;
            $misi[$i]->save();
        };

        $hima = Hima::find($request->id);
        $hima->name = $request->name;
        $hima->id_prodi = $request->id_prodi;
        $hima->visi = $request->visi;
        $hima->save();
        return redirect('hima-calon');
    }

    public function create(Request $request)
    {
        $prodi = Prodi::find($request->id_prodi);
        $prodi->name = substr($prodi->name, 3);
        $prodi->himpunan = $this->hima($request->id_prodi);
        $filename = Carbon::now()->format('dmYHis');
        $now = Carbon::now()->isoFormat('D MMMM Y');
        $d = Carbon::now()->isoFormat('dddd');
        $D = $this->penyebut(Carbon::now()->isoFormat('D'));
        $M = strtolower(Carbon::now()->isoFormat('MMMM'));
        $Y = $this->penyebut(Carbon::now()->isoFormat('Y'));
        $path = public_path('reports');
        $report = Report::all();
        $setup = Setup::first();
        $setup->no = $setup->no + 1;
        $setup->save();
        $setup->now = $now;
        $setup->now_text = $d . ', tanggal' . $D . ' bulan ' . $M . ' tahun' . $Y;
        $setup->date = Carbon::now()->isoFormat('D/MM/Y');
        $setup->voter = count(Voter::all());
        if (strlen($setup->no) >= 3 || (int)$setup->no == 3) {
            $setup->no = (int)$setup->no;
        } elseif (strlen($setup->no) == 2 || (int)$setup->no == 2) {
            $setup->no = '0' . (int)$setup->no;
        } elseif (strlen($setup->no) == 1) {
            $setup->no = '00' . (int)$setup->no;
        }
        $setup->no = $setup->no . '/KPUM/' . NumConvert::roman((int)Carbon::now()->isoFormat('M')) . '/' . Carbon::now()->isoFormat('Y');
        $hima = Hima::where('id_prodi', $request->id_prodi)->get();
        $pdf = PDF::loadview('reports.hima.sk_tap', compact('setup', 'hima', 'request', 'prodi'))->setPaper('a4', 'portrait');
        $pdf->save($path . '/HIMA/SK_TAP_' . strtoupper($prodi->himpunan) . $filename . '.pdf');

        $new_report = new Report;
        $new_report->name = 'SK HIMA ' . strtoupper($prodi->himpunan);
        $new_report->category = 'HIMA';
        $new_report->no = $setup->no;
        $new_report->url_sk_tap = asset('reports') . '/HIMA/SK_TAP_' . strtoupper($prodi->himpunan) . $filename . '.pdf';
        $new_report->save();
        return redirect()->back();
    }

    public function penyebut($nilai)
    {
        $nilai = abs($nilai);
        $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($nilai < 12) {
            $temp = " " . $huruf[$nilai];
        } else if ($nilai < 20) {
            $temp = $this->penyebut($nilai - 10) . " belas";
        } else if ($nilai < 100) {
            $temp = $this->penyebut($nilai / 10) . " puluh" . $this->penyebut($nilai % 10);
        } else if ($nilai < 200) {
            $temp = " seratus" . $this->penyebut($nilai - 100);
        } else if ($nilai < 1000) {
            $temp = $this->penyebut($nilai / 100) . " ratus" . $this->penyebut($nilai % 100);
        } else if ($nilai < 2000) {
            $temp = " seribu" . $this->penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
            $temp = $this->penyebut($nilai / 1000) . " ribu" . $this->penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
            $temp = $this->penyebut($nilai / 1000000) . " juta" . $this->penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
            $temp = $this->penyebut($nilai / 1000000000) . " milyar" . $this->penyebut(fmod($nilai, 1000000000));
        } else if ($nilai < 1000000000000000) {
            $temp = $this->penyebut($nilai / 1000000000000) . " trilyun" . $this->penyebut(fmod($nilai, 1000000000000));
        }
        return $temp;
    }

    public function hima($id)
    {
        $hasil = "";
        switch ($id) {
            case 1:
                $hasil = "Himatif";
                break;

            case 2:
                $hasil = "Himatekom";
                break;

            case 3:
                $hasil = "Himateklis";
                break;

            case 4:
                $hasil = "Himameto";
                break;

            case 5:
                $hasil = "Himatka";
                break;

            case 6:
                $hasil = "Himatpl";
                break;

            case 7:
                $hasil = "Himadbis";
                break;

            case 8:
                $hasil = "Himags";
                break;

            case 9:
                $hasil = "Himaksi";
                break;

            case 10:
                $hasil = "Himka";
                break;

            default:
                $hasil = "";
                break;
        }

        return $hasil;
    }
}
