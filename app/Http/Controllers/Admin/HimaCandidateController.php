<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Hima;
use App\Models\HimaCandidate;
use App\Models\Jurusan;
use App\Models\MisiHima;
use App\Models\Prodi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HimaCandidateController extends Controller
{

    public function index()
    {
        $user = Auth::user();
        $calon = HimaCandidate::all();
        $prodi = Prodi::all();
        $jurusan = Jurusan::all();
        return view('admin.candidate.hima.index', compact('user', 'calon', 'prodi', 'jurusan'));
    }

    public function detail($id)
    {
        $user = Auth::user();
        $calon = HimaCandidate::findOrFail($id);
        $calon->birthday = substr($calon->birthday, 8, 2) . '-' . substr($calon->birthday, 5, 2) . '-' . substr($calon->birthday, 0, 4);
        return view('admin.candidate.hima.detail', compact('user', 'calon'));
    }

    public function acc($id)
    {
        $calon = HimaCandidate::findOrFail($id);
        $hima = new Hima;
        $hima->name = $calon->fullname;
        $hima->nickname = $calon->name;
        $hima->id_prodi = $calon->id_prodi;
        $hima->visi = $calon->visi;
        $hima->save();

        foreach ($calon->misi as $old_misi) {
            $misi = new MisiHima;
            $misi->detail = $old_misi->detail;
            $misi->id_hima = $hima->id;
            $misi->save();
        }
        $calon->delete();
        return redirect()->route('admin-hima-candidate');
    }
}
