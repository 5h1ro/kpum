<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SetupController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $setup = Setup::first();
        return view('admin.setup.index', compact('user', 'setup'));
    }
    public function edit(Request $request)
    {
        $setup = Setup::first();
        $setup->name = $request->name;
        $setup->npm = $request->npm;
        $setup->no = $request->no;
        $setup->current_periode = $request->current_periode;
        $setup->next_periode = $request->next_periode;
        $setup->save();
        return redirect()->back();
    }
}
