<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Dpm;
use App\Models\DpmCandidate;
use App\Models\Jurusan;
use App\Models\MisiDpm;
use App\Models\Prodi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DpmCandidateController extends Controller
{

    public function index()
    {
        $user = Auth::user();
        $calon = DpmCandidate::all();
        $prodi = Prodi::all();
        $jurusan = Jurusan::all();
        return view('admin.candidate.dpm.index', compact('user', 'calon', 'prodi', 'jurusan'));
    }

    public function detail($id)
    {
        $user = Auth::user();
        $calon = DpmCandidate::findOrFail($id);
        $calon->birthday = substr($calon->birthday, 8, 2) . '-' . substr($calon->birthday, 5, 2) . '-' . substr($calon->birthday, 0, 4);
        return view('admin.candidate.dpm.detail', compact('user', 'calon'));
    }

    public function acc($id)
    {
        $calon = DpmCandidate::findOrFail($id);
        $dpm = new Dpm;
        $dpm->name = $calon->fullname;
        $dpm->nickname = $calon->name;
        $dpm->id_prodi = $calon->id_prodi;
        $dpm->visi = $calon->visi;
        $dpm->save();

        foreach ($calon->misi as $old_misi) {
            $misi = new MisiDpm();
            $misi->detail = $old_misi->detail;
            $misi->id_dpm = $dpm->id;
            $misi->save();
        }
        $calon->delete();
        return redirect()->route('admin-dpm-candidate');
    }
}
