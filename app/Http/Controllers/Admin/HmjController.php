<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Hmj;
use App\Models\Jurusan;
use App\Models\MisiHmj;
use App\Models\Prodi;
use App\Models\Report;
use App\Models\Setup;
use App\Models\Vote;
use App\Models\Voter;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use HnhDigital\LaravelNumberConverter\Facade as NumConvert;

class HmjController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $calon = Hmj::all();
        $vote = Vote::all();
        foreach ($vote as $value) {
            foreach ($calon as $values) {
                if (Hash::check($values->id, $value->id_hmj)) {
                    $value->id_hmj = $values->id;
                }
            }
        }
        foreach ($calon as $value) {
            $value->vote = count($vote->where('id_hmj', $value->id));
        }
        $prodi = Prodi::all();
        $jurusan = Jurusan::all();
        return view('admin.calon.hmj.index', compact('user', 'calon', 'prodi', 'jurusan'));
    }

    public function update(Request $request)
    {
        $misi = MisiHmj::where('id_hmj', $request->id)->get();
        for ($i = 0; $i < count($misi); $i++) {
            $a = "misi" . $i + 1;
            $misi[$i]->detail = $request->$a;
            $misi[$i]->save();
        };

        $hmj = Hmj::find($request->id);
        $hmj->name = $request->name;
        $hmj->id_jurusan = $request->id_jurusan;
        $hmj->id_prodi = $request->id_prodi;
        $hmj->visi = $request->visi;
        $hmj->save();
        return redirect('hmj-calon');
    }

    public function create(Request $request)
    {
        $jurusan = Jurusan::find($request->id_jurusan);
        $filename = Carbon::now()->format('dmYHis');
        $now = Carbon::now()->isoFormat('D MMMM Y');
        $d = Carbon::now()->isoFormat('dddd');
        $D = $this->penyebut(Carbon::now()->isoFormat('D'));
        $M = strtolower(Carbon::now()->isoFormat('MMMM'));
        $Y = $this->penyebut(Carbon::now()->isoFormat('Y'));
        $path = public_path('reports');
        $report = Report::all();
        $setup = Setup::first();
        $setup->no = $setup->no + 1;
        $setup->save();
        $setup->now = $now;
        $setup->now_text = $d . ', tanggal' . $D . ' bulan ' . $M . ' tahun' . $Y;
        $setup->date = Carbon::now()->isoFormat('D/MM/Y');
        $setup->voter = count(Voter::all());
        if (strlen($setup->no) >= 3 || (int)$setup->no == 3) {
            $setup->no = (int)$setup->no;
        } elseif (strlen($setup->no) == 2 || (int)$setup->no == 2) {
            $setup->no = '0' . (int)$setup->no;
        } elseif (strlen($setup->no) == 1) {
            $setup->no = '00' . (int)$setup->no;
        }
        $setup->no = $setup->no . '/KPUM/' . NumConvert::roman((int)Carbon::now()->isoFormat('M')) . '/' . Carbon::now()->isoFormat('Y');
        $hmj = Hmj::where('id_jurusan', $request->id_jurusan)->get();
        $pdf = PDF::loadview('reports.hmj.sk_tap', compact('setup', 'hmj', 'request', 'jurusan'))->setPaper('a4', 'portrait');
        $pdf->save($path . '/HMJ/SK_TAP_' . strtoupper($jurusan->name) . $filename . '.pdf');

        $new_report = new Report;
        $new_report->name = 'SK HMJ ' . strtoupper($jurusan->name);
        $new_report->category = 'HMJ';
        $new_report->no = $setup->no;
        $new_report->url_sk_tap = asset('reports') . '/HMJ/SK_TAP_' . strtoupper($jurusan->name) . $filename . '.pdf';
        $new_report->save();
        return redirect()->back();
    }

    public function penyebut($nilai)
    {
        $nilai = abs($nilai);
        $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($nilai < 12) {
            $temp = " " . $huruf[$nilai];
        } else if ($nilai < 20) {
            $temp = $this->penyebut($nilai - 10) . " belas";
        } else if ($nilai < 100) {
            $temp = $this->penyebut($nilai / 10) . " puluh" . $this->penyebut($nilai % 10);
        } else if ($nilai < 200) {
            $temp = " seratus" . $this->penyebut($nilai - 100);
        } else if ($nilai < 1000) {
            $temp = $this->penyebut($nilai / 100) . " ratus" . $this->penyebut($nilai % 100);
        } else if ($nilai < 2000) {
            $temp = " seribu" . $this->penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
            $temp = $this->penyebut($nilai / 1000) . " ribu" . $this->penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
            $temp = $this->penyebut($nilai / 1000000) . " juta" . $this->penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
            $temp = $this->penyebut($nilai / 1000000000) . " milyar" . $this->penyebut(fmod($nilai, 1000000000));
        } else if ($nilai < 1000000000000000) {
            $temp = $this->penyebut($nilai / 1000000000000) . " trilyun" . $this->penyebut(fmod($nilai, 1000000000000));
        }
        return $temp;
    }
}
