<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Hmj;
use App\Models\HmjCandidate;
use App\Models\Jurusan;
use App\Models\MisiHmj;
use App\Models\Prodi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HmjCandidateController extends Controller
{

    public function index()
    {
        $user = Auth::user();
        $calon = HmjCandidate::all();
        $prodi = Prodi::all();
        $jurusan = Jurusan::all();
        return view('admin.candidate.hmj.index', compact('user', 'calon', 'prodi', 'jurusan'));
    }

    public function detail($id)
    {
        $user = Auth::user();
        $calon = HmjCandidate::findOrFail($id);
        $calon->birthday = substr($calon->birthday, 8, 2) . '-' . substr($calon->birthday, 5, 2) . '-' . substr($calon->birthday, 0, 4);
        return view('admin.candidate.hmj.detail', compact('user', 'calon'));
    }

    public function acc($id)
    {
        $calon = HmjCandidate::findOrFail($id);
        $hmj = new Hmj;
        $hmj->name = $calon->fullname;
        $hmj->nickname = $calon->name;
        $hmj->id_jurusan = $calon->id_jurusan;
        $hmj->id_prodi = $calon->id_prodi;
        $hmj->visi = $calon->visi;
        $hmj->save();

        foreach ($calon->misi as $old_misi) {
            $misi = new MisiHmj;
            $misi->detail = $old_misi->detail;
            $misi->id_hmj = $hmj->id;
            $misi->save();
        }
        $calon->delete();
        return redirect()->route('admin-hmj-candidate');
    }
}
