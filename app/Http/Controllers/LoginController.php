<?php

namespace App\Http\Controllers;

use App\Models\Bem;
use App\Models\Jurusan;
use App\Models\Prodi;
use App\Models\User;
use App\Models\Voter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{

    public function logout()
    {
        Auth::guard('web')->logout();
        return redirect('/');
    }

    public function home()
    {
        $bem = Bem::all();
        return view('welcome');
    }

    public function regis()
    {
        $prodi = Prodi::all();
        return view('auth.register', compact('prodi'));
    }

    public function register(Request $request)
    {
        $prodi = Prodi::find($request->id_prodi);

        $user = new User;
        $user->email = $request->email;
        $user->npm = $request->npm;
        $user->password = Hash::make($request->password);
        $user->save();

        $voter = new Voter;
        $voter->name = $request->name;
        $voter->fullname = $request->fullname;
        $voter->number = $request->number;
        $voter->id_prodi = $request->id_prodi;
        $voter->id_jurusan = $prodi->id_jurusan;
        $voter->id_user = $user->id;
        $voter->save();
        return redirect()->route('login');
        // if ($user->npm == $request->npm && Hash::check($request->password, $user->password)) {
        // } else {
        //     return redirect(url('/login'))->withErrors(['msg' => 'Error']);
        // }
    }
}
