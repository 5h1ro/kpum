<?php

namespace App\Http\Controllers\Voter;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Voter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        return view('voter.profile.index', compact('user'));
    }

    public function edit(Request $request)
    {
        $user = Auth::user();
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        return redirect()->route('voter-profile');
    }

    public function edit2(Request $request)
    {
        $user = Auth::user();
        $voter = Voter::findOrFail($user->voter->id);
        $name = Carbon::now()->format('YmdHis');
        if ($request->image != null) {
            $fileName = $name . '.' . $request->image->getClientOriginalExtension();
            $request->image->move(public_path('image/profile'), $fileName);
            $voter->image = asset('image/profile') . '/' . $fileName;
        }

        $voter->name = $request->name;
        $voter->fullname = $request->fullname;
        $voter->number = $request->number;
        $voter->city = $request->city;
        $voter->birthday = $request->birthday;
        $voter->gender = $request->gender;
        $voter->religion = $request->religion;
        $voter->address = $request->address;
        $voter->save();

        return redirect()->route('voter-profile');
    }
}
