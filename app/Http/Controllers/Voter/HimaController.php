<?php

namespace App\Http\Controllers\Voter;

use App\Http\Controllers\Controller;
use App\Models\Hima;
use App\Models\Prodi;
use App\Models\Schedule;
use App\Models\Vote;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class HimaController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $hima = Hima::where('id_prodi', $user->voter->id_prodi)->get();
        $prodi = Prodi::all();
        return view('voter.pemilihan.hima.index', compact('user', 'hima', 'prodi'));
    }

    public function detail($id)
    {
        $user = Auth::user();
        $real_id = Crypt::decrypt($id);
        $hima = Hima::where('id', $real_id)->first();
        $prodi = Prodi::all();
        return view('voter.pemilihan.hima.detail', compact('user', 'hima', 'prodi'));
    }

    public function vote($id)
    {
        $user = Auth::user();
        $schedule = Schedule::where([['id_prodi', '=', $user->voter->id_prodi], ['category', '=', 'HIMA']])->first();
        if (isset($schedule)) {
            if (Carbon::now()->format('YmdHi') >= preg_replace('/[^0-9]/', '', $schedule->start) && Carbon::now()->format('YmdHi') <= preg_replace('/[^0-9]/', '', $schedule->end)) {
                $real_id = Crypt::decrypt($id);
                $hima = Hima::where('id', $real_id)->first();
                $vote_check = Vote::where([['id_hima', '=', $real_id], ['id_user', '=', $user->id]])->first();
                $voter = $user->voter;
                if ($voter->hima == 0) {
                    if ($vote_check == null) {
                        $voter->hima = 1;
                        $voter->save();
                        $vote = new Vote;
                        $vote->id_hima = Hash::make($real_id);
                        $vote->id_user = $user->id;
                        $vote->save();
                        $count_vote = Vote::where('id_hima', $real_id)->get();
                        $hima->vote = count($count_vote);
                    }
                }
                $voter = $user->voter;
                if ($voter->bem == 0) {
                }
                Session::flash('message', "Success");
                return redirect(route('pemilihan'));
            } else {
                return redirect(route('pemilihan'))->withErrors(['msg' => 'Error']);
            }
        } else {
            return redirect(route('pemilihan'))->withErrors(['msg' => 'Error']);
        }
    }
}
