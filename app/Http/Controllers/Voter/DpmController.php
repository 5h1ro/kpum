<?php

namespace App\Http\Controllers\Voter;

use App\Http\Controllers\Controller;
use App\Models\Dpm;
use App\Models\Prodi;
use App\Models\Schedule;
use App\Models\Vote;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class DpmController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $dpm = Dpm::where('id_prodi', $user->voter->id_prodi)->get();
        $prodi = Prodi::all();
        return view('voter.pemilihan.dpm.index', compact('user', 'dpm', 'prodi'));
    }

    public function detail($id)
    {
        $user = Auth::user();
        $real_id = Crypt::decrypt($id);
        $dpm = Dpm::where('id', $real_id)->first();
        $prodi = Prodi::all();
        return view('voter.pemilihan.dpm.detail', compact('user', 'dpm', 'prodi'));
    }

    public function vote($id)
    {
        $user = Auth::user();
        // $dpm = Dpm::where('id', $id)->first();
        // $voter = $user->voter;
        // if ($voter->dpm == 0) {
        //     $voter->dpm = 1;
        //     $voter->save();
        //     $dpm->vote = $voter->dpm + 1;
        //     $dpm->save();
        // }
        // return redirect(route('pemilihan'));

        $schedule = Schedule::where([['id_prodi', '=', $user->voter->id_prodi], ['category', '=', 'DPM']])->first();
        if (isset($schedule)) {
            if (Carbon::now()->format('YmdHi') >= preg_replace('/[^0-9]/', '', $schedule->start) && Carbon::now()->format('YmdHi') <= preg_replace('/[^0-9]/', '', $schedule->end)) {
                $real_id = Crypt::decrypt($id);
                $dpm = Dpm::where('id', $real_id)->first();
                $vote_check = Vote::where([['id_dpm', '=', $real_id], ['id_user', '=', $user->id]])->first();
                $voter = $user->voter;
                if ($voter->dpm == 0) {
                    if ($vote_check == null) {
                        $voter->dpm = 1;
                        $voter->save();
                        $vote = new Vote;
                        $vote->id_dpm = Hash::make($real_id);
                        $vote->id_user = $user->id;
                        $vote->save();
                        $count_vote = Vote::where('id_dpm', $real_id)->get();
                        $dpm->vote = count($count_vote);
                    }
                }
                $voter = $user->voter;
                if ($voter->bem == 0) {
                }
                Session::flash('message', "Success");
                return redirect(route('pemilihan'));
            } else {
                return redirect(route('pemilihan'))->withErrors(['msg' => 'Error']);
            }
        } else {
            return redirect(route('pemilihan'))->withErrors(['msg' => 'Error']);
        }
    }
}
