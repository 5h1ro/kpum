<?php

namespace App\Http\Controllers\Voter;

use App\Http\Controllers\Controller;
use App\Models\Hmj;
use App\Models\Schedule;
use App\Models\Vote;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class HmjController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $hmj = Hmj::where('id_jurusan', $user->voter->id_jurusan)->get();
        return view('voter.pemilihan.hmj.index', compact('user', 'hmj'));
    }

    public function detail($id)
    {
        $user = Auth::user();
        $real_id = Crypt::decrypt($id);
        $hmj = Hmj::where('id', $real_id)->first();
        return view('voter.pemilihan.hmj.detail', compact('user', 'hmj'));
    }

    public function vote($id)
    {
        $user = Auth::user();
        $schedule = Schedule::where([['id_prodi', '=', $user->voter->id_prodi], ['category', '=', 'HMJ']])->first();
        if (isset($schedule)) {
            if (Carbon::now()->format('YmdHi') >= preg_replace('/[^0-9]/', '', $schedule->start) && Carbon::now()->format('YmdHi') <= preg_replace('/[^0-9]/', '', $schedule->end)) {
                $real_id = Crypt::decrypt($id);
                $hmj = Hmj::where('id', $real_id)->first();
                $vote_check = Vote::where([['id_bem', '=', $real_id], ['id_user', '=', $user->id]])->first();
                $voter = $user->voter;
                if ($voter->hmj == 0) {
                    if ($vote_check == null) {
                        $voter->hmj = 1;
                        $voter->save();
                        $vote = new Vote;
                        $vote->id_hmj = Hash::make($real_id);
                        $vote->id_user = $user->id;
                        $vote->save();
                        $count_vote = Vote::where('id_hmj', $real_id)->get();
                        $hmj->vote = count($count_vote);
                    }
                }
                $voter = $user->voter;
                if ($voter->bem == 0) {
                }
                Session::flash('message', "Success");
                return redirect(route('pemilihan'));
            } else {
                return redirect(route('pemilihan'))->withErrors(['msg' => 'Error']);
            }
        } else {
            return redirect(route('pemilihan'))->withErrors(['msg' => 'Error']);
        }
    }
}
