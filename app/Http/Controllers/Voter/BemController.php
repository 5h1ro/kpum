<?php

namespace App\Http\Controllers\Voter;

use App\Http\Controllers\Controller;
use App\Models\Bem;
use App\Models\Prodi;
use App\Models\Schedule;
use App\Models\Vote;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class BemController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $bem = Bem::all();
        $prodi = Prodi::all();
        return view('voter.pemilihan.bem.index', compact('user', 'bem', 'prodi'));
    }

    public function detail($id)
    {
        $user = Auth::user();
        $real_id = Crypt::decrypt($id);
        $bem = Bem::where('id', $real_id)->first();
        $prodi = Prodi::all();
        return view('voter.pemilihan.bem.detail', compact('user', 'bem', 'prodi'));
    }

    public function vote($id)
    {
        $user = Auth::user();
        $schedule = Schedule::where([['id_prodi', '=', $user->voter->id_prodi], ['category', '=', 'BEM']])->first();
        if (isset($schedule)) {
            if (Carbon::now()->format('YmdHi') >= preg_replace('/[^0-9]/', '', $schedule->start) && Carbon::now()->format('YmdHi') <= preg_replace('/[^0-9]/', '', $schedule->end)) {
                $real_id = Crypt::decrypt($id);
                $bem = Bem::where('id', $real_id)->first();
                $vote_check = Vote::where([['id_bem', '=', $real_id], ['id_user', '=', $user->id]])->first();
                $voter = $user->voter;
                if ($voter->bem == 0) {
                    if ($vote_check == null) {
                        $voter->bem = 1;
                        $voter->save();
                        $vote = new Vote;
                        $vote->id_bem = Hash::make($real_id);
                        $vote->id_user = $user->id;
                        $vote->save();
                        $count_vote = Vote::where('id_bem', $real_id)->get();
                        $bem->vote = count($count_vote);
                    }
                }
                Session::flash('message', "Success");
                return redirect(route('pemilihan'));
            } else {
                return redirect(route('pemilihan'))->withErrors(['msg' => 'Error']);
            }
        } else {
            return redirect(route('pemilihan'))->withErrors(['msg' => 'Error']);
        }
    }
}
