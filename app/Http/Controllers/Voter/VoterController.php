<?php

namespace App\Http\Controllers\Voter;

use App\Http\Controllers\Controller;
use App\Models\Bem;
use App\Models\Dpm;
use App\Models\Hima;
use App\Models\Hmj;
use App\Models\Vote;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VoterController extends Controller
{

    public function index()
    {
        $user = Auth::user();
        $bem = Bem::all();
        $persentase = [];
        foreach ($bem as $percent) {
            $vote = Vote::where('id_bem', $percent->id)->get();
            $count = count($vote);
            $data = round($count / 1432 * 100, 2);
            array_push($persentase, $data);
        }
        $suarasah = 0;
        $suarasahpersen = 0;
        foreach ($bem as $sah) {
            $vote = Vote::where('id_bem', $sah->id)->get();
            $count = count($vote);
            $sah->vote = $count;
            $suarasah = $suarasah + $count;
            $suarasahpersen = $suarasahpersen + round($count / 1432 * 100, 2);
        }
        $pemilih = 1432;
        $pemilihpersen = 100;
        $suaranonsah = $pemilih - $suarasah;
        $suaranonsahpersen = $pemilihpersen - $suarasahpersen;
        return view('voter.index', compact('user', 'bem', 'persentase', 'suarasah', 'suarasahpersen', 'suaranonsah', 'suaranonsahpersen', 'pemilih', 'pemilihpersen'));
    }


    public function fresh()
    {
        $user = Auth::user();
        $bem = Bem::all();
        $persentase = [];
        foreach ($bem as $percent) {
            $data = round($percent->vote / 1432 * 100, 2);
            array_push($persentase, $data);
        }
        $suarasah = 0;
        $suarasahpersen = 0;
        foreach ($bem as $sah) {
            $suarasah = $suarasah + $sah->vote;
            $suarasahpersen = $suarasahpersen + round($sah->vote / 1432 * 100, 2);
        }
        $pemilih = 1432;
        $pemilihpersen = 100;
        $suaranonsah = $pemilih - $suarasah;
        $suaranonsahpersen = $pemilihpersen - $suarasahpersen;
        return view('voter.home', compact('user', 'bem', 'persentase', 'suarasah', 'suarasahpersen', 'suaranonsah', 'suaranonsahpersen', 'pemilih', 'pemilihpersen'));
    }

    public function index_hima($id)
    {
        $user = Auth::user();
        $hima = Hima::where('id_prodi', $id)->get();
        $persentase = [];
        foreach ($hima as $percent) {
            $data = round($percent->vote / 1432 * 100, 2);
            array_push($persentase, $data);
        }
        $suarasah = 0;
        $suarasahpersen = 0;
        foreach ($hima as $sah) {
            $suarasah = $suarasah + $sah->vote;
            $suarasahpersen = $suarasahpersen + round($sah->vote / 1432 * 100, 2);
        }
        $pemilih = 1432;
        $pemilihpersen = 100;
        $suaranonsah = $pemilih - $suarasah;
        $suaranonsahpersen = $pemilihpersen - $suarasahpersen;
        return view('voter.hima.home', compact('user', 'hima', 'persentase', 'suarasah', 'suarasahpersen', 'suaranonsah', 'suaranonsahpersen', 'pemilih', 'pemilihpersen'));
    }

    public function fresh_hima($id)
    {
        $user = Auth::user();
        $hima = Hima::where('id_prodi', $id)->get();
        $persentase = [];
        foreach ($hima as $percent) {
            $data = round($percent->vote / 1432 * 100, 2);
            array_push($persentase, $data);
        }
        $suarasah = 0;
        $suarasahpersen = 0;
        foreach ($hima as $sah) {
            $suarasah = $suarasah + $sah->vote;
            $suarasahpersen = $suarasahpersen + round($sah->vote / 1432 * 100, 2);
        }
        $pemilih = 1432;
        $pemilihpersen = 100;
        $suaranonsah = $pemilih - $suarasah;
        $suaranonsahpersen = $pemilihpersen - $suarasahpersen;
        return view('voter.hima.index', compact('user', 'hima', 'persentase', 'suarasah', 'suarasahpersen', 'suaranonsah', 'suaranonsahpersen', 'pemilih', 'pemilihpersen'));
    }

    public function index_dpm($id)
    {
        $user = Auth::user();
        $dpm = Dpm::where('id_prodi', $id)->get();
        $persentase = [];
        foreach ($dpm as $percent) {
            $data = round($percent->vote / 1432 * 100, 2);
            array_push($persentase, $data);
        }
        $suarasah = 0;
        $suarasahpersen = 0;
        foreach ($dpm as $sah) {
            $suarasah = $suarasah + $sah->vote;
            $suarasahpersen = $suarasahpersen + round($sah->vote / 1432 * 100, 2);
        }
        $pemilih = 1432;
        $pemilihpersen = 100;
        $suaranonsah = $pemilih - $suarasah;
        $suaranonsahpersen = $pemilihpersen - $suarasahpersen;
        return view('voter.dpm.home', compact('user', 'dpm', 'persentase', 'suarasah', 'suarasahpersen', 'suaranonsah', 'suaranonsahpersen', 'pemilih', 'pemilihpersen'));
    }

    public function fresh_dpm($id)
    {
        $user = Auth::user();
        $dpm = Dpm::where('id_prodi', $id)->get();
        $persentase = [];
        foreach ($dpm as $percent) {
            $data = round($percent->vote / 1432 * 100, 2);
            array_push($persentase, $data);
        }
        $suarasah = 0;
        $suarasahpersen = 0;
        foreach ($dpm as $sah) {
            $suarasah = $suarasah + $sah->vote;
            $suarasahpersen = $suarasahpersen + round($sah->vote / 1432 * 100, 2);
        }
        $pemilih = 1432;
        $pemilihpersen = 100;
        $suaranonsah = $pemilih - $suarasah;
        $suaranonsahpersen = $pemilihpersen - $suarasahpersen;
        return view('voter.dpm.index', compact('user', 'dpm', 'persentase', 'suarasah', 'suarasahpersen', 'suaranonsah', 'suaranonsahpersen', 'pemilih', 'pemilihpersen'));
    }

    public function index_hmj($id)
    {
        $user = Auth::user();
        $hmj = Hmj::where('id_jurusan', $id)->get();
        $persentase = [];
        foreach ($hmj as $percent) {
            $data = round($percent->vote / 1432 * 100, 2);
            array_push($persentase, $data);
        }
        $suarasah = 0;
        $suarasahpersen = 0;
        foreach ($hmj as $sah) {
            $suarasah = $suarasah + $sah->vote;
            $suarasahpersen = $suarasahpersen + round($sah->vote / 1432 * 100, 2);
        }
        $pemilih = 1432;
        $pemilihpersen = 100;
        $suaranonsah = $pemilih - $suarasah;
        $suaranonsahpersen = $pemilihpersen - $suarasahpersen;
        return view('voter.hmj.home', compact('user', 'hmj', 'persentase', 'suarasah', 'suarasahpersen', 'suaranonsah', 'suaranonsahpersen', 'pemilih', 'pemilihpersen'));
    }

    public function fresh_hmj($id)
    {
        $user = Auth::user();
        $hmj = Hmj::where('id_jurusan', $id)->get();
        $persentase = [];
        foreach ($hmj as $percent) {
            $data = round($percent->vote / 1432 * 100, 2);
            array_push($persentase, $data);
        }
        $suarasah = 0;
        $suarasahpersen = 0;
        foreach ($hmj as $sah) {
            $suarasah = $suarasah + $sah->vote;
            $suarasahpersen = $suarasahpersen + round($sah->vote / 1432 * 100, 2);
        }
        $pemilih = 1432;
        $pemilihpersen = 100;
        $suaranonsah = $pemilih - $suarasah;
        $suaranonsahpersen = $pemilihpersen - $suarasahpersen;
        return view('voter.hmj.index', compact('user', 'hmj', 'persentase', 'suarasah', 'suarasahpersen', 'suaranonsah', 'suaranonsahpersen', 'pemilih', 'pemilihpersen'));
    }
}
