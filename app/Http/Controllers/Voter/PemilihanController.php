<?php

namespace App\Http\Controllers\Voter;

use App\Http\Controllers\Controller;
use App\Models\Schedule;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PemilihanController extends Controller
{
    public function index(Schedule $schedule)
    {
        $user = Auth::user();
        return view('voter.pemilihan.index', compact('user', 'schedule'));
    }
}
