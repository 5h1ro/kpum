<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Models\Jurusan;
use App\Models\Prodi;
use App\Models\Schedule;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ScheduleController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $schedule = Schedule::all();
        $category = ['BEM', 'DPM', 'HMJ', 'HIMA'];
        $prodi = Prodi::all();
        $jurusan = Jurusan::all();
        $year_now = Carbon::now()->format('Y-m-d');
        $time_now = Carbon::now()->format('H:i');
        $now = $year_now . 'T' . $time_now . ':00';
        $nowadd = $year_now . 'T' . $time_now;
        if (isset($schedule->array)) {
            return view('superadmin.schedule.index', compact('user', 'category', 'prodi', 'jurusan', 'now'));
        } else {
            $startedit = [];
            $endedit = [];
            foreach ($schedule as $data) {
                $start = preg_replace('/[^0-9]/', '', $data->start);
                $ys = substr($start, 0, 4);
                $ms = substr($start, 4, 2);
                $ds = substr($start, 6, 2);
                $hs = substr($start, 8, 2);
                $is = substr($start, 10, 2);
                $end = preg_replace('/[^0-9]/', '', $data->end);
                $ye = substr($end, 0, 4);
                $me = substr($end, 4, 2);
                $de = substr($end, 6, 2);
                $he = substr($end, 8, 2);
                $ie = substr($end, 10, 2);

                array_push($startedit, $data->start);
                array_push($endedit, $data->end);
                $data->start = $ds . '-' . $ms . '-' . $ys . ', ' . $hs . ':' . $is . ':00';
                $data->end = $de . '-' . $me . '-' . $ye . ', ' . $he . ':' . $ie . ':00';
            }
            return view('superadmin.schedule.index', compact('user', 'schedule', 'category', 'prodi', 'jurusan', 'now', 'nowadd', 'startedit', 'endedit'));
        }
    }

    public function edit($id, Request $request)
    {
        $schedule = Schedule::where('id', $id)->first();
        $schedule->name = $request->name;
        $schedule->category = $request->category;
        $schedule->start = $request->start;
        $schedule->end = $request->end;
        $schedule->id_jurusan = $request->id_jurusan;
        $schedule->id_prodi = $request->id_prodi;
        $schedule->save();

        return redirect()->route('sa-data-schedule');
    }

    public function add(Request $request)
    {
        $validate = Schedule::where([['category', '=', $request->category], ['id_prodi', '=', $request->id_prodi]])->first();
        if (isset($validate)) {
            return redirect()->route('sa-data-schedule');
        } else {
            $schedule = new Schedule;
            $schedule->name = $request->name;
            $schedule->category = $request->category;
            $schedule->start = $request->start;
            $schedule->end = $request->end;
            $schedule->id_jurusan = $request->id_jurusan;
            $schedule->id_prodi = $request->id_prodi;
            $schedule->save();
            return redirect()->route('sa-data-schedule');
        }
    }

    public function delete($id)
    {
        $schedule = Schedule::where('id', $id)->first();
        $schedule->delete();

        return redirect()->route('sa-data-schedule');
    }
}
