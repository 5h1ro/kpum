<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RoleController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $role = Role::all();
        return view('superadmin.role.index', compact('user', 'role'));
    }

    public function edit(Request $request)
    {
        $role = Role::where('id', $request->id)->first();
        $role->name = $request->name;
        $role->permission = $request->permission;
        $role->save();

        return redirect()->route('sa-role');
    }
}
