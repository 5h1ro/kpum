<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Models\Bem;
use App\Models\MisiBem;
use App\Models\Prodi;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BemController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $calon = Bem::all();
        $prodi = Prodi::all();
        return view('superadmin.calon.bem.index', compact('user', 'calon', 'prodi'));
    }

    public function add(Request $request)
    {
        $name = Carbon::now()->format('YmdHis');
        $fileName = $name . '.' . $request->image->getClientOriginalExtension();
        $request->image->move(public_path('image/candidate'), $fileName);
        $fileName2 = $name . 'vote.' . $request->image2->getClientOriginalExtension();
        $request->image2->move(public_path('image/candidate'), $fileName2);

        $bem = new Bem;
        $bem->nickname1 = $request->nickname1;
        $bem->nickname2 = $request->nickname2;
        $bem->name1 = $request->name1;
        $bem->name2 = $request->name2;
        $bem->img = asset('image/candidate') . '/' . $fileName;
        $bem->img2 = asset('image/candidate') . '/' . $fileName2;
        $bem->id_prodi1 = $request->id_prodi1;
        $bem->id_prodi2 = $request->id_prodi2;
        $bem->visi = $request->visi;
        $bem->save();
        if ($request->misi1 != null) {
            $misi = new MisiBem();
            $misi->detail = $request->misi1;
            $misi->id_bem = $bem->id;
            $misi->save();
            if ($request->misi2 != null) {
                $misi2 = new MisiBem;
                $misi2->detail = $request->misi2;
                $misi2->id_bem = $bem->id;
                $misi2->save();
                if ($request->misi3 != null) {
                    $misi3 = new MisiBem;
                    $misi3->detail = $request->misi3;
                    $misi3->id_bem = $bem->id;
                    $misi3->save();
                    if ($request->misi4 != null) {
                        $misi4 = new MisiBem;
                        $misi4->detail = $request->misi4;
                        $misi4->id_bem = $bem->id;
                        $misi4->save();
                        if ($request->misi5 != null) {
                            $misi5 = new MisiBem;
                            $misi5->detail = $request->misi5;
                            $misi5->id_bem = $bem->id;
                            $misi5->save();
                            if ($request->misi6 != null) {
                                $misi6 = new MisiBem;
                                $misi6->detail = $request->misi6;
                                $misi6->id_bem = $bem->id;
                                $misi6->save();
                            } else {
                                return redirect('sa-bem-calon');
                            }
                        } else {
                            return redirect('sa-bem-calon');
                        }
                    } else {
                        return redirect('sa-bem-calon');
                    }
                } else {
                    return redirect('sa-bem-calon');
                }
            } else {
                return redirect('sa-bem-calon');
            }
        } else {
            return redirect('sa-bem-calon');
        }
    }

    public function update(Request $request)
    {
        $misi = MisiBem::where('id_bem', $request->id)->get();
        $name = Carbon::now()->format('YmdHis');
        for ($i = 0; $i < count($misi); $i++) {
            $a = "misi" . $i + 1;
            $misi[$i]->detail = $request->$a;
            $misi[$i]->save();
        };
        $bem = Bem::find($request->id);
        $bem->name1 = $request->name1;
        $bem->name2 = $request->name2;
        if ($request->image != null) {
            $fileName = $name . '.' . $request->image->getClientOriginalExtension();
            $request->image->move(public_path('image/candidate'), $fileName);
            $bem->img = asset('image/candidate') . '/' . $fileName;
        }
        if ($request->image2 != null) {
            $fileName2 = $name . 'vote.' . $request->image2->getClientOriginalExtension();
            $request->image2->move(public_path('image/candidate'), $fileName2);
            $bem->img2 = asset('image/candidate') . '/' . $fileName2;
        }
        $bem->id_prodi1 = $request->id_prodi1;
        $bem->id_prodi2 = $request->id_prodi2;
        $bem->visi = $request->visi;
        $bem->save();
        return redirect('sa-bem-calon');
    }

    public function delete($id)
    {
        $bem = Bem::findOrFail($id);
        dd($bem);
    }
}
