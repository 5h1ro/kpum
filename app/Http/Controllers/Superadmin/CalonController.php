<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Models\Bem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CalonController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $calon = Bem::all();
        return view('superadmin.calon.index', compact('user', 'calon'));
    }
}
