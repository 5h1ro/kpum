<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Models\Hima;
use App\Models\MisiHima;
use App\Models\Prodi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HimaController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $calon = Hima::all();
        $prodi = Prodi::all();
        return view('superadmin.calon.hima.index', compact('user', 'calon', 'prodi'));
    }

    public function update(Request $request)
    {
        $misi = MisiHima::where('id_hima', $request->id)->get();
        for ($i = 0; $i < count($misi); $i++) {
            $a = "misi" . $i + 1;
            $misi[$i]->detail = $request->$a;
            $misi[$i]->save();
        };

        $hima = Hima::find($request->id);
        $hima->name = $request->name;
        $hima->id_prodi = $request->id_prodi;
        $hima->visi = $request->visi;
        $hima->save();
        return redirect('sa-hima-calon');
    }
}
