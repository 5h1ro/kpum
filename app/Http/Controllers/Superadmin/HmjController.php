<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Models\Hmj;
use App\Models\Jurusan;
use App\Models\MisiHmj;
use App\Models\Prodi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HmjController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $calon = Hmj::all();
        $prodi = Prodi::all();
        $jurusan = Jurusan::all();
        return view('superadmin.calon.hmj.index', compact('user', 'calon', 'prodi', 'jurusan'));
    }

    public function update(Request $request)
    {
        $misi = MisiHmj::where('id_hmj', $request->id)->get();
        for ($i = 0; $i < count($misi); $i++) {
            $a = "misi" . $i + 1;
            $misi[$i]->detail = $request->$a;
            $misi[$i]->save();
        };

        $hmj = Hmj::find($request->id);
        $hmj->name = $request->name;
        $hmj->id_jurusan = $request->id_jurusan;
        $hmj->id_prodi = $request->id_prodi;
        $hmj->visi = $request->visi;
        $hmj->save();
        return redirect('sa-hmj-calon');
    }
}
