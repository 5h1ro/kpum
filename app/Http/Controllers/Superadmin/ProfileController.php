<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        return view('superadmin.profile.index', compact('user'));
    }

    public function edit(Request $request)
    {
        $user = Auth::user();
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        return redirect()->route('sa-profile');
    }
}
