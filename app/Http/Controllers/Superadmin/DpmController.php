<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Models\Dpm;
use App\Models\MisiDpm;
use App\Models\Prodi;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DpmController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $calon = Dpm::all();
        $prodi = Prodi::all();
        return view('superadmin.calon.dpm.index', compact('user', 'calon', 'prodi'));
    }

    public function add(Request $request)
    {
        $name = Carbon::now()->format('YmdHis');
        $fileName = $name . '.' . $request->image->getClientOriginalExtension();
        $request->image->move(public_path('image/candidate'), $fileName);
        $fileName2 = $name . 'vote.' . $request->image2->getClientOriginalExtension();
        $request->image2->move(public_path('image/candidate'), $fileName2);

        $dpm = new Dpm;
        $dpm->nickname = $request->nickname;
        $dpm->name = $request->name;
        $dpm->img = asset('image/candidate') . '/' . $fileName;
        $dpm->img2 = asset('image/candidate') . '/' . $fileName2;
        $dpm->id_prodi = $request->id_prodi;
        $dpm->visi = $request->visi;
        $dpm->save();
        if ($request->misi1 != null) {
            $misi = new MisiDpm();
            $misi->detail = $request->misi1;
            $misi->id_dpm = $dpm->id;
            $misi->save();
            if ($request->misi2 != null) {
                $misi2 = new MisiDpm;
                $misi2->detail = $request->misi2;
                $misi2->id_dpm = $dpm->id;
                $misi2->save();
                if ($request->misi3 != null) {
                    $misi3 = new MisiDpm;
                    $misi3->detail = $request->misi3;
                    $misi3->id_dpm = $dpm->id;
                    $misi3->save();
                    if ($request->misi4 != null) {
                        $misi4 = new MisiDpm;
                        $misi4->detail = $request->misi4;
                        $misi4->id_dpm = $dpm->id;
                        $misi4->save();
                        if ($request->misi5 != null) {
                            $misi5 = new MisiDpm;
                            $misi5->detail = $request->misi5;
                            $misi5->id_dpm = $dpm->id;
                            $misi5->save();
                            if ($request->misi6 != null) {
                                $misi6 = new MisiDpm;
                                $misi6->detail = $request->misi6;
                                $misi6->id_dpm = $dpm->id;
                                $misi6->save();
                            } else {
                                return redirect('sa-dpm-calon');
                            }
                        } else {
                            return redirect('sa-dpm-calon');
                        }
                    } else {
                        return redirect('sa-dpm-calon');
                    }
                } else {
                    return redirect('sa-dpm-calon');
                }
            } else {
                return redirect('sa-dpm-calon');
            }
        } else {
            return redirect('sa-dpm-calon');
        }
    }
    public function update(Request $request)
    {
        $name = Carbon::now()->format('YmdHis');
        $misi = MisiDpm::where('id_dpm', $request->id)->get();
        for ($i = 0; $i < count($misi); $i++) {
            $a = "misi" . $i + 1;
            $misi[$i]->detail = $request->$a;
            $misi[$i]->save();
        };
        $dpm = Dpm::find($request->id);
        $dpm->name = $request->name;
        if ($request->image != null) {
            $fileName = $name . '.' . $request->image->getClientOriginalExtension();
            $request->image->move(public_path('image/candidate'), $fileName);
            $dpm->img = asset('image/candidate') . '/' . $fileName;
        }
        if ($request->image2 != null) {
            $fileName2 = $name . 'vote.' . $request->image2->getClientOriginalExtension();
            $request->image2->move(public_path('image/candidate'), $fileName2);
            $dpm->img2 = asset('image/candidate') . '/' . $fileName2;
        }
        $dpm->id_prodi = $request->id_prodi;
        $dpm->visi = $request->visi;
        $dpm->save();
        return redirect('sa-dpm-calon');
    }
}
