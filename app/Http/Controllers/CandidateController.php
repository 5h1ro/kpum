<?php

namespace App\Http\Controllers;

use App\Models\BemCandidate;
use App\Models\DpmCandidate;
use App\Models\HimaCandidate;
use App\Models\HmjCandidate;
use App\Models\Jurusan;
use App\Models\MisiBemCandidate;
use App\Models\MisiDpmCandidate;
use App\Models\MisiHimaCandidate;
use App\Models\MisiHmjCandidate;
use App\Models\Prodi;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CandidateController extends Controller
{
    public function register_candidate()
    {
        return view('candidate.index');
    }

    public function register_candidate_bem()
    {
        $jurusan = Jurusan::all();
        $prodi = Prodi::all();
        return view('candidate.bem.index', compact('jurusan', 'prodi'));
    }

    public function bem_post(Request $request)
    {
        $name = Carbon::now()->format('YmdHis');
        $bem = new BemCandidate;
        $bem->fullname1 = $request->fullname1;
        $bem->name1 = $request->name1;
        $bem->npm1 = $request->npm1;
        $bem->birthplace1 = $request->birthplace1;
        $bem->birthday1 = $request->birthday1;
        $bem->gender1 = $request->gender1;
        $bem->email1 = $request->email1;
        $bem->id_jurusan1 = $request->id_jurusan1;
        $bem->id_prodi1 = $request->id_prodi1;
        $bem->class1 = $request->class1;
        $bem->fullname2 = $request->fullname2;
        $bem->name2 = $request->name2;
        $bem->npm2 = $request->npm2;
        $bem->birthplace2 = $request->birthplace2;
        $bem->birthday2 = $request->birthday2;
        $bem->gender2 = $request->gender2;
        $bem->email2 = $request->email2;
        $bem->id_jurusan2 = $request->id_jurusan2;
        $bem->id_prodi2 = $request->id_prodi2;
        $bem->class2 = $request->class2;
        $bem->visi = $request->visi;
        if ($request->formulir != null) {
            $fileName = $name . '.' . $request->formulir->getClientOriginalExtension();
            $request->formulir->move(public_path('candidate/formulir'), $fileName);
            $bem->formulir = asset('candidate/formulir') . '/' . $fileName;
        }
        if ($request->persyaratan != null) {
            $fileName = $name . '.' . $request->persyaratan->getClientOriginalExtension();
            $request->persyaratan->move(public_path('candidate/persyaratan'), $fileName);
            $bem->persyaratan = asset('candidate/persyaratan') . '/' . $fileName;
        }
        if ($request->photo1 != null) {
            $fileName = $name . '.' . $request->photo1->getClientOriginalExtension();
            $request->photo1->move(public_path('candidate/photo'), $fileName);
            $bem->photo1 = asset('candidate/photo') . '/' . $fileName;
        }
        if ($request->photo2 != null) {
            $fileName = $name . 'wapres' . '.' . $request->photo2->getClientOriginalExtension();
            $request->photo2->move(public_path('candidate/photo'), $fileName);
            $bem->photo2 = asset('candidate/photo') . '/' . $fileName;
        }
        $bem->save();

        for ($i = 1; $i < 7; $i++) {
            if ($request['misi' . $i] != null) {
                $misi = new MisiBemCandidate;
                $misi->detail = $request['misi' . $i];
                $misi->id_bem_candidate = $bem->id;
                $misi->save();
            }
        }

        return redirect()->route('register-candidate');
    }

    public function register_candidate_dpm()
    {
        $jurusan = Jurusan::all();
        $prodi = Prodi::all();
        return view('candidate.dpm.index', compact('jurusan', 'prodi'));
    }

    public function dpm_post(Request $request)
    {
        $name = Carbon::now()->format('YmdHis');
        $dpm = new DpmCandidate;
        $dpm->fullname = $request->fullname;
        $dpm->name = $request->name;
        $dpm->npm = $request->npm;
        $dpm->birthplace = $request->birthplace;
        $dpm->birthday = $request->birthday;
        $dpm->gender = $request->gender;
        $dpm->email = $request->email;
        $dpm->id_jurusan = $request->id_jurusan;
        $dpm->id_prodi = $request->id_prodi;
        $dpm->class = $request->class;
        $dpm->visi = $request->visi;
        if ($request->formulir != null) {
            $fileName = $name . '.' . $request->formulir->getClientOriginalExtension();
            $request->formulir->move(public_path('candidate/formulir'), $fileName);
            $dpm->formulir = asset('candidate/formulir') . '/' . $fileName;
        }
        if ($request->persyaratan != null) {
            $fileName = $name . '.' . $request->persyaratan->getClientOriginalExtension();
            $request->persyaratan->move(public_path('candidate/persyaratan'), $fileName);
            $dpm->persyaratan = asset('candidate/persyaratan') . '/' . $fileName;
        }
        if ($request->photo != null) {
            $fileName = $name . '.' . $request->photo->getClientOriginalExtension();
            $request->photo->move(public_path('candidate/photo'), $fileName);
            $dpm->photo = asset('candidate/photo') . '/' . $fileName;
        }
        $dpm->save();

        for ($i = 1; $i < 7; $i++) {
            if ($request['misi' . $i] != null) {
                $misi = new MisiDpmCandidate();
                $misi->detail = $request['misi' . $i];
                $misi->id_dpm_candidate = $dpm->id;
                $misi->save();
            }
        }

        return redirect()->route('register-candidate');
    }

    public function register_candidate_hmj()
    {
        $jurusan = Jurusan::all();
        $prodi = Prodi::all();
        return view('candidate.hmj.index', compact('jurusan', 'prodi'));
    }

    public function hmj_post(Request $request)
    {
        $name = Carbon::now()->format('YmdHis');
        $hmj = new HmjCandidate;
        $hmj->fullname = $request->fullname;
        $hmj->name = $request->name;
        $hmj->npm = $request->npm;
        $hmj->birthplace = $request->birthplace;
        $hmj->birthday = $request->birthday;
        $hmj->gender = $request->gender;
        $hmj->email = $request->email;
        $hmj->id_jurusan = $request->id_jurusan;
        $hmj->id_prodi = $request->id_prodi;
        $hmj->class = $request->class;
        $hmj->visi = $request->visi;
        if ($request->formulir != null) {
            $fileName = $name . '.' . $request->formulir->getClientOriginalExtension();
            $request->formulir->move(public_path('candidate/formulir'), $fileName);
            $hmj->formulir = asset('candidate/formulir') . '/' . $fileName;
        }
        if ($request->persyaratan != null) {
            $fileName = $name . '.' . $request->persyaratan->getClientOriginalExtension();
            $request->persyaratan->move(public_path('candidate/persyaratan'), $fileName);
            $hmj->persyaratan = asset('candidate/persyaratan') . '/' . $fileName;
        }
        if ($request->photo != null) {
            $fileName = $name . '.' . $request->photo->getClientOriginalExtension();
            $request->photo->move(public_path('candidate/photo'), $fileName);
            $hmj->photo = asset('candidate/photo') . '/' . $fileName;
        }
        $hmj->save();

        for ($i = 1; $i < 7; $i++) {
            if ($request['misi' . $i] != null) {
                $misi = new MisiHmjCandidate();
                $misi->detail = $request['misi' . $i];
                $misi->id_hmj_candidate = $hmj->id;
                $misi->save();
            }
        }

        return redirect()->route('register-candidate');
    }

    public function register_candidate_hima()
    {
        $jurusan = Jurusan::all();
        $prodi = Prodi::all();
        return view('candidate.hima.index', compact('jurusan', 'prodi'));
    }

    public function hima_post(Request $request)
    {
        $name = Carbon::now()->format('YmdHis');
        $hima = new HimaCandidate();
        $hima->fullname = $request->fullname;
        $hima->name = $request->name;
        $hima->npm = $request->npm;
        $hima->birthplace = $request->birthplace;
        $hima->birthday = $request->birthday;
        $hima->gender = $request->gender;
        $hima->email = $request->email;
        $hima->id_jurusan = $request->id_jurusan;
        $hima->id_prodi = $request->id_prodi;
        $hima->class = $request->class;
        $hima->visi = $request->visi;
        if ($request->formulir != null) {
            $fileName = $name . '.' . $request->formulir->getClientOriginalExtension();
            $request->formulir->move(public_path('candidate/formulir'), $fileName);
            $hima->formulir = asset('candidate/formulir') . '/' . $fileName;
        }
        if ($request->persyaratan != null) {
            $fileName = $name . '.' . $request->persyaratan->getClientOriginalExtension();
            $request->persyaratan->move(public_path('candidate/persyaratan'), $fileName);
            $hima->persyaratan = asset('candidate/persyaratan') . '/' . $fileName;
        }
        if ($request->photo != null) {
            $fileName = $name . '.' . $request->photo->getClientOriginalExtension();
            $request->photo->move(public_path('candidate/photo'), $fileName);
            $hima->photo = asset('candidate/photo') . '/' . $fileName;
        }
        $hima->save();

        for ($i = 1; $i < 7; $i++) {
            if ($request['misi' . $i] != null) {
                $misi = new MisiHimaCandidate;
                $misi->detail = $request['misi' . $i];
                $misi->id_hima_candidate = $hima->id;
                $misi->save();
            }
        }

        return redirect()->route('register-candidate');
    }
}
