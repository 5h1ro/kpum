<?php

namespace Illuminate\Foundation\Auth;

use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\ValidationException;

trait AuthenticatesUsers
{
    use RedirectsUsers, ThrottlesLogins;

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('auth.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (
            method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)
        ) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        // $user = User::where('npm', $request->npm)->first();
        // $user_agent = strtolower($request->header('user-agent'));
        // if ($user !=  null) {
        //     // set browser
        //     if (strpos($user_agent, 'opera') || strpos($user_agent, 'opr/')) {

        //         if ($user->browser == 'Opera') {
        //             // os
        //             if (strpos($user_agent, 'linux')) {
        //                 if ($user->os == 'Linux') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } elseif (strpos($user_agent, 'macintosh')) {
        //                 if ($user->os == 'Mac') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } elseif (strpos($user_agent, 'windows')) {
        //                 if ($user->os == 'Windows') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } elseif (strpos($user_agent, 'android')) {
        //                 if ($user->os == 'Android') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } elseif (strpos($user_agent, 'iphone')) {
        //                 if ($user->os == 'iPhone') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } else {
        //                 if ($user->os == 'Unknown') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             }
        //         } else {
        //             $request->error = 'device';
        //         }
        //     } elseif (strpos($user_agent, 'edge')) {

        //         if ($user->browser == 'Edge') {
        //             // os
        //             if (strpos($user_agent, 'linux')) {
        //                 if ($user->os == 'Linux') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } elseif (strpos($user_agent, 'macintosh')) {
        //                 if ($user->os == 'Mac') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } elseif (strpos($user_agent, 'windows')) {
        //                 if ($user->os == 'Windows') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } elseif (strpos($user_agent, 'android')) {
        //                 if ($user->os == 'Android') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } elseif (strpos($user_agent, 'iphone')) {
        //                 if ($user->os == 'iPhone') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } else {
        //                 if ($user->os == 'Unknown') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             }
        //         } else {
        //             $request->error = 'device';
        //         }
        //     } elseif (strpos($user_agent, 'chrome')) {

        //         if ($user->browser == 'Chrome') {
        //             // os
        //             if (strpos($user_agent, 'linux')) {
        //                 if ($user->os == 'Linux') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } elseif (strpos($user_agent, 'macintosh')) {
        //                 if ($user->os == 'Mac') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } elseif (strpos($user_agent, 'windows')) {
        //                 if ($user->os == 'Windows') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } elseif (strpos($user_agent, 'android')) {
        //                 if ($user->os == 'Android') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } elseif (strpos($user_agent, 'iphone')) {
        //                 if ($user->os == 'iPhone') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } else {
        //                 if ($user->os == 'Unknown') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             }
        //         } else {
        //             $request->error = 'device';
        //         }
        //     } elseif (strpos($user_agent, 'safari')) {

        //         if ($user->browser == 'Safari') {
        //             // os
        //             if (strpos($user_agent, 'linux')) {
        //                 if ($user->os == 'Linux') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } elseif (strpos($user_agent, 'macintosh')) {
        //                 if ($user->os == 'Mac') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } elseif (strpos($user_agent, 'windows')) {
        //                 if ($user->os == 'Windows') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } elseif (strpos($user_agent, 'android')) {
        //                 if ($user->os == 'Android') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } elseif (strpos($user_agent, 'iphone')) {
        //                 if ($user->os == 'iPhone') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } else {
        //                 if ($user->os == 'Unknown') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             }
        //         } else {
        //             $request->error = 'device';
        //         }
        //     } elseif (strpos($user_agent, 'firefox')) {

        //         if ($user->browser == 'Firefox') {
        //             // os
        //             if (strpos($user_agent, 'linux')) {
        //                 if ($user->os == 'Linux') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } elseif (strpos($user_agent, 'macintosh')) {
        //                 if ($user->os == 'Mac') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } elseif (strpos($user_agent, 'windows')) {
        //                 if ($user->os == 'Windows') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } elseif (strpos($user_agent, 'android')) {
        //                 if ($user->os == 'Android') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } elseif (strpos($user_agent, 'iphone')) {
        //                 if ($user->os == 'iPhone') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } else {
        //                 if ($user->os == 'Unknown') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             }
        //         } else {
        //             $request->error = 'device';
        //         }
        //     } elseif (strpos($user_agent, 'msie') || strpos($user_agent, 'trident/7')) {

        //         if ($user->browser == 'Internet Explorer') {
        //             // os
        //             if (strpos($user_agent, 'linux')) {
        //                 if ($user->os == 'Linux') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } elseif (strpos($user_agent, 'macintosh')) {
        //                 if ($user->os == 'Mac') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } elseif (strpos($user_agent, 'windows')) {
        //                 if ($user->os == 'Windows') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } elseif (strpos($user_agent, 'android')) {
        //                 if ($user->os == 'Android') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } elseif (strpos($user_agent, 'iphone')) {
        //                 if ($user->os == 'iPhone') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } else {
        //                 if ($user->os == 'Unknown') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             }
        //         } else {
        //             $request->error = 'device';
        //         }
        //     } else {

        //         if ($user->browser == 'Unknown') {
        //             // os
        //             if (strpos($user_agent, 'linux')) {
        //                 if ($user->os == 'Linux') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } elseif (strpos($user_agent, 'macintosh')) {
        //                 if ($user->os == 'Mac') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } elseif (strpos($user_agent, 'windows')) {
        //                 if ($user->os == 'Windows') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } elseif (strpos($user_agent, 'android')) {
        //                 if ($user->os == 'Android') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } elseif (strpos($user_agent, 'iphone')) {
        //                 if ($user->os == 'iPhone') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             } else {
        //                 if ($user->os == 'Unknown') {
        //                     return $this->guard()->attempt(
        //                         $this->credentials($request),
        //                         $request->filled('remember')
        //                     );
        //                 } else {
        //                     $request->error = 'device';
        //                 }
        //             }
        //         } else {
        //             $request->error = 'device';
        //         }
        //     }
        // } else {
        return $this->guard()->attempt(
            $this->credentials($request),
            $request->filled('remember')
        );
        // }
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password');
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        if ($response = $this->authenticated($request, $this->guard()->user())) {
            return $response;
        }

        return $request->wantsJson()
            ? new JsonResponse([], 204)
            : redirect()->intended($this->redirectPath());
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        //
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        if ($request->error == "device") {
            Session::flash('NotAuth', "error");
            return redirect(route('login'));
        } else {
            return redirect(route('login'))->withErrors(['msg' => 'Error']);
        }

        // throw ValidationException::withMessages([
        //     $this->username() => [trans('auth.failed')],
        // ]);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'npm';
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        if ($response = $this->loggedOut($request)) {
            return $response;
        }

        return $request->wantsJson()
            ? new JsonResponse([], 204)
            : redirect('/');
    }

    /**
     * The user has logged out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    protected function loggedOut(Request $request)
    {
        //
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }
}
