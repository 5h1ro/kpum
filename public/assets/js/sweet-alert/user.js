var SweetAlert_custom = {
    init: function () {
        swal({
            title: "Jangan Coba-Coba Curang",
            text: "Pilihlah Sesuai Jadwal!",
            icon: "error",
            buttons: false,
            dangerMode: true,
        });
    }
};
(function ($) {
    SweetAlert_custom.init()
})(jQuery);

