var SweetAlert_custom = {
    init: function () {
        swal({
            title: "Error",
            text: "Gunakan Di Browser dan Device yang sama saat registrasi",
            icon: "error",
            buttons: false,
            dangerMode: true,
        });
    }
};
(function ($) {
    SweetAlert_custom.init()
})(jQuery);

