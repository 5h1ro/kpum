var SweetAlert_custom = {
    init: function () {
        swal({
            title: "Terimakasih",
            text: "Telah Menggunakan Suara Anda",
            icon: "success",
            buttons: false,
            dangerMode: true,
        });
    }
};
(function ($) {
    SweetAlert_custom.init()
})(jQuery);

