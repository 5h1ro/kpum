var SweetAlert_custom = {
    init: function () {
        swal({
            title: "Gagal",
            text: "Maaf Akun Anda Telah Terdaftar Di Device Lain",
            icon: "error",
            buttons: false,
            dangerMode: true,
        });
    }
};
(function ($) {
    SweetAlert_custom.init()
})(jQuery);

