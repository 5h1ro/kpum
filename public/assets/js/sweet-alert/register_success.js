var SweetAlert_custom = {
    init: function () {
        swal({
            title: "Berhasil",
            text: "Device Telah Didaftarkan",
            icon: "success",
            buttons: false,
            dangerMode: true,
        });
    }
};
(function ($) {
    SweetAlert_custom.init()
})(jQuery);

