function add(id) {
    if (id < 6) {
        let i = id + 1;
        if ($("#misi" + i).length == 0) {
            $('#form-misi').append('<div id="form-kiri-' +
                i + '" class="col-6 col-lg-10 mb-3 mb-2 align-self-center"><label style="color: rgba(0, 0, 0, 0);" for="misi' +
                i + '">Misi</label><textarea class="form-control" id="misi' +
                i + '"name="misi' +
                i + '"required=""></textarea></div><div id="form-tengah-' +
                i + '" class="col-3 col-lg-1 mb-3 mb-2 align-self-center"><label style="color: rgba(0, 0, 0, 0);">Remove</label><button class="btn-lg btn-outline-danger shadow-none" id="btn-remove-' +
                i + '"onclick="remove(' +
                i + ')" name="btn-remove-' +
                i + '" ><i class="fa fa-trash"></i></button></div><div id="form-kanan-' +
                i + '" class="col-3 col-lg-1 mb-3 mb-2 align-self-center"><label style="color: rgba(0, 0, 0, 0);">Add</label><button class="btn-lg btn-outline-success shadow-none" id="btn-add-' +
                i + '"onclick="add(' +
                i + ')" name="btn-add-' +
                i + '"><i class="fa fa-plus"></i></button></div>'
            );
        } else {
            console.log('max');
        }
    } else {
        console.log('max');
    }
}

function remove(id) {
    var misi = document.getElementById("misi" + id);
    misi.remove();
    var remove = document.getElementById("btn-remove-" + id);
    remove.remove();
    var add = document.getElementById("btn-add-" + id);
    add.remove();
    var kiri = document.getElementById("form-kiri-" + id);
    kiri.remove();
    var tengah = document.getElementById("form-tengah-" + id);
    tengah.remove();
    var kanan = document.getElementById("form-kanan-" + id);
    kanan.remove();
}
